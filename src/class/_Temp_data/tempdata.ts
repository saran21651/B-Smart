import { Product_Model, Product_List_Model } from "../Model/Order/Product";

export class TempData {
  promotion_data_array: Array<{
    describe: string;
    price: string;
    sale: string;
    image: string;
  }>;
  order_data_array: Array<{
    title: string;
    image: string;
    describe: string;
    img_collection: Array<{ image: string }>;
    product_time: string;
  }>;
  order_sub_data_array: Array<{ title: string }>;
  order_detail_data_array: Array<{
    title: string;
    price: string;
    image: string;

  }>;
  contact_data_array: Array<{
    name: string;
    lastMessage: string;
    timeStamp: any;
    image: string;
  }>;
  main_menu_data_array: Array<{ image: string }>;

  order_tracking_data_array: Array<{
    image: string;
    status: string;
    status_type: number;
    orderno: string;
    date: string;
    quantity: Number;
    total: Number;
    new: string;
    shipped_to: string;
    tel: string;
    payment: string;
    order: Array<{
      name: string;
      quantity: Number;
      price: Number;
    }>;
  }>;

  public main_menu_data() {
    this.main_menu_data_array = [
      {
        image:
          "http://www.b-smartprint.com/2014/images/prod_view/1428131004_.jpg"
      },
      {
        image:
          "http://www.b-smartprint.com/2014/images/prod_view/1427961009_.jpg"
      },
      {
        image:
          "http://www.b-smartprint.com/2014/images/prod_view/1428287450_p.jpg"
      }
    ];
    return this.main_menu_data_array;
  }

  public promotion_data() {
    this.promotion_data_array = [
      {
        describe: "จัดงานอีเว้นท์ รับงานออแกไนซ์ เต็มรูปแบบ",
        price: "150,000 ฿",
        sale: "145,000 ฿",
        image:
          "https://travel.mthai.com/app/uploads/2016/07/13438873_10155011779359298_3998626191450052055_n.jpg"
      },
      {
        describe: "จัดงานอีเว้นท์ รับงานออแกไนซ์ เต็มรูปแบบ",
        price: "160,000 ฿",
        sale: "140,000 ฿",
        image:
          "https://travel.mthai.com/app/uploads/2016/07/13599890_10155009451414298_6958782316265951121_n.jpg"
      },
      {
        describe: "จัดงานอีเว้นท์ รับงานออแกไนซ์ เต็มรูปแบบ",
        price: "120,000 ฿",
        sale: "115,000 ฿",
        image: "https://zipimg.azureedge.net/blog/2015/12/batch_DSC_0053.jpg"
      }
    ];
    return this.promotion_data_array;
  }

  check_price_data() {}

  public contact_data_online() {
    this.contact_data_array = [
      {
        name: "Malcolm Ray",
        lastMessage: "Hi!!!",
        image:
          "https://vignette.wikia.nocookie.net/thatguywiththeglasses/images/7/75/574658_486659071370446_301954597_n.jpg/revision/latest?cb=20130615072746",
        timeStamp: new Date()
      },
      {
        name: "Mike Stoklasa",
        lastMessage: "Hi!!!",
        image:
          "https://vignette.wikia.nocookie.net/prerec/images/6/68/IMG_0312.PNG/revision/latest?cb=20171105232645",
        timeStamp: new Date()
      },
      {
        name: "Chris Stuckmann",
        lastMessage: "Hello!!!",
        image:
          "https://yt3.ggpht.com/-GEPxvQpBV6I/AAAAAAAAAAI/AAAAAAAAAAA/JftrS7a2wwA/s288-mo-c-c0xffffffff-rj-k-no/photo.jpg",
        timeStamp: new Date()
      }
    ];

    return this.contact_data_array;
  }

  public contact_data_offline() {
    this.contact_data_array = [
      {
        name: "Jay Bauman",
        lastMessage: "Hi!!!",
        image:
          "https://vignette.wikia.nocookie.net/rlmeu/images/c/c2/Tumblr_m62g4s9V861rsabxvo6_r1_1280.png/revision/latest?cb=20120815195055",
        timeStamp: new Date()
      },
      {
        name: "Shinji Mikami",
        lastMessage: "Hi!!!",
        image:
          "https://upload.wikimedia.org/wikipedia/commons/e/ed/Shinji_Mikami_April_2013_3.jpg",
        timeStamp: new Date()
      }
    ];
    return this.contact_data_array;
  }

  chat_data() {}

  public order_data() {
    this.order_data_array = [
      {
        title: "งานอีเว้นท์ และออแกไนซ์",
        image:
          "https://www.chillpainai.com/src/wewakeup/scoop/img_scoop/scoop/kat/event/26-27%20march/12366183_919314711490850_1144134642289604892_o.jpg",
        product_time: "7",
        describe:
          "รับจัดงานออแกไนซ์ เต์มรูปแบบ ทั้งแสง สี เสียง งานนออกบูธ จัดขบวนคาราวาน จัดธีมงาน พร้อมสื่อประชาสัมพันธ์ และกิจกรรมสัมพันธ์",
        img_collection: [
          {
            image: ""
          },
          {
            image: ""
          },
          {
            image: ""
          },
          {
            image: ""
          },
          {
            image: ""
          },
          {
            image: ""
          }
        ]
      },
      {
        title: "งานป้ายโฆษณา",
        image:
          "http://xn--12cbl6c5abi0d6czbxce2rg3hd.com/wp-content/uploads/2014/09/%E0%B8%9B%E0%B9%89%E0%B8%B2%E0%B8%A2%E0%B8%82%E0%B8%AD%E0%B8%99%E0%B9%81%E0%B8%81%E0%B9%88%E0%B8%99.jpg",
        product_time: "7",
        describe: "ป้ายโฆษณาลักษณะใหญ่ โครงสร้างแข็งแรง แน่นหนา มั่นคง",
        img_collection: [
          {
            image: ""
          },
          {
            image: ""
          },
          {
            image: ""
          },
          {
            image: ""
          },
          {
            image: ""
          },
          {
            image: ""
          }
        ]
      },
      {
        title: "งานโครงสร้าง",
        image:
          "http://www.smeleader.com/wp-content/uploads/2014/03/%E0%B9%84%E0%B8%81%E0%B9%88%E0%B8%A2%E0%B9%88%E0%B8%B2%E0%B8%87%E0%B8%AB%E0%B9%89%E0%B8%B2%E0%B8%94%E0%B8%B2%E0%B8%A7-%E0%B9%81%E0%B8%9F%E0%B8%A3%E0%B8%99%E0%B9%84%E0%B8%8A%E0%B8%AA%E0%B9%8C%E0%B9%81%E0%B8%84%E0%B9%88%E0%B8%AB%E0%B8%A5%E0%B8%B1%E0%B8%81%E0%B8%AB%E0%B8%A1%E0%B8%B7%E0%B9%88%E0%B8%99-%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B9%83%E0%B8%84%E0%B8%A3%E0%B8%81%E0%B9%87%E0%B9%80%E0%B8%9B%E0%B9%87%E0%B8%99%E0%B9%80%E0%B8%88%E0%B9%89%E0%B8%B2%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%84%E0%B8%94%E0%B9%89-9.jpg",
        product_time: "7",
        describe:
          "งานเหล็ก งานป้ายขนาดใหญ่ ทั้งเหล็กถัก เหล็กกล่อง ตามแบบที่ลูกค้าต้องการ",
        img_collection: [
          {
            image: ""
          },
          {
            image: ""
          },
          {
            image: ""
          },
          {
            image: ""
          },
          {
            image: ""
          },
          {
            image: ""
          }
        ]
      },
      {
        title: "งานสื่อสิ่งพิมพ์",
        image:
          "http://2.bp.blogspot.com/-xdwrlU2_aRc/VNDyrNUa2KI/AAAAAAAAAYM/a4gey9KI2lY/s1600/IMG_8728.gif",
        product_time: "7",
        describe: "ตัวอักษรสามารถผลิตได้ตาม Font ที่ลูกค้าต้องการ",
        img_collection: [
          {
            image: ""
          },
          {
            image: ""
          },
          {
            image: ""
          },
          {
            image: ""
          },
          {
            image: ""
          },
          {
            image: ""
          }
        ]
      }
    ];
    return this.order_data_array;
  }

  public order_sub_data() {
    this.order_sub_data_array = [
      {
        title: "ตรายาง"
      },
      {
        title: "นามบัตร"
      },
      {
        title: "บัตรสมาชิก"
      },
      {
        title: "บัตรพนักงาน"
      },
      {
        title: "ใบปลิว แผ่นผับ โปสเตอร์"
      },
      {
        title: "ลาเบล ฉลากติดสินค้า"
      },
      {
        title: "การ์ด"
      },
      {
        title: "หนังสือ วารสาร"
      },
      {
        title: "เข็มกลัด"
      },
      {
        title: "สายคล้องคอ"
      }
    ];
    return this.order_sub_data_array;
  }

  public order_detail_data() {
    this.order_detail_data_array = [
      {
        title: "ตรายางแป้นธรรมดา",
        price: "100 ฿",
        image: "http://www.b-smartprint.com/7trayang/images/stampnormal.jpg"
      },
      {
        title: "ตรายางออฟเช็ต",
        price: "200 ฿",
        image:
          "http://www.b-smartprint.com/7trayang/images/type-prod/stamp-offset.png"
      },
      {
        title: "ตรายางหมึกในตัว",
        price: "100 ฿",
        image:
          "http://www.b-smartprint.com/7trayang/images/type-prod/stamp-int.png"
      },
      {
        title: "ตรายางสำเร็จรูป",
        price: "100 ฿",
        image:
          "http://www.b-smartprint.com/7trayang/images/type-prod/stamp-sumret.png"
      },
      {
        title: "ด้ามไม้รางรถไฟ",
        price: "100 ฿",
        image:
          "http://www.b-smartprint.com/7trayang/images/type-prod/stamp-mai.png"
      }
    ];
    return this.order_detail_data_array;
  }

  public order_tracking_data() {
    let temp = [
      [
        {
          name: "ตรายางแป้นธรรมดา",
          quantity: 3,
          price: 100
        },
        {
          name: "ตรายางออฟเช็ต",
          quantity: 3,
          price: 200
        }
      ],
      [
        {
          name: "ตรายางหมึกในตัว",
          quantity: 3,
          price: 100
        }
      ]
    ];

    // Status
    // 0 - Cancel
    // 1 - Waiting for shipment
    // 2 - Payment Success

    this.order_tracking_data_array = [
      {
        image: "http://www.b-smartprint.com/7trayang/images/stampnormal.jpg",
        status: "Waiting for shipment",
        status_type: 1,
        orderno: "BSM0004",
        date: "12 June 2018",
        quantity: temp[0][0].quantity + temp[0][1].quantity,
        total:
          temp[0][0].price * temp[0][0].quantity +
          temp[0][1].price * temp[0][1].quantity,
        new: "NEW",
        shipped_to: "99/9 หมู่ 9 ตำบลท่าโพธิ์ อ.เมือง จ.พิษณุโลก 65000",
        tel: "088-8889999",
        payment: "Online Banking (KBANK)",
        order: temp[0]
      },
      {
        image:
          "http://www.b-smartprint.com/7trayang/images/type-prod/stamp-int.png",
        status: "Payment Accepted",
        status_type: 2,
        orderno: "BSM0003",
        date: "12 June 2018",
        quantity: temp[1][0].quantity,
        total: temp[1][0].price * temp[1][0].quantity,
        new: "OLD",
        shipped_to: "99/9 หมู่ 9 ตำบลท่าโพธิ์ อ.เมือง จ.พิษณุโลก 65000",
        tel: "088-8889999",
        payment: "Online Banking (KBANK)",
        order: temp[1]
      },
      {
        image: "http://www.b-smartprint.com/7trayang/images/stampnormal.jpg",
        status: "Cancel",
        status_type: 0,
        orderno: "BSM0004",
        date: "12 June 2018",
        quantity: temp[0][0].quantity + temp[0][1].quantity,
        total:
          temp[0][0].price * temp[0][0].quantity +
          temp[0][1].price * temp[0][1].quantity,
        new: "NEW",
        shipped_to: "99/9 หมู่ 9 ตำบลท่าโพธิ์ อ.เมือง จ.พิษณุโลก 65000",
        tel: "088-8889999",
        payment: "Online Banking (KBANK)",
        order: temp[0]
      }
    ];

    return this.order_tracking_data_array;
  }

  public user_data() {
    return {
      name: "Auter Til",
      member: "Member Since 2018",
      image:
        "https://i.kinja-img.com/gawker-media/image/upload/c_scale,f_auto,fl_progressive,q_80,w_800/eibgv7kctah62iddzywm.jpg"
    };
  }

  public address_book() {
    let items: Array<{
      name: string;
      address: string;
      province: string;
      zipcode: Number;
      tel: string;
    }>;
    return (items = [
      {
        name: "Nicole Morgan",
        address: "99/9 ต.ท่าโพธฺ์",
        province: "พิษณุโลก",
        zipcode: 65000,
        tel: "088-88889999"
      },
      {
        name: "Nicole Morgan",
        address: "99/9 ต.ท่าโพธฺ์",
        province: "พิษณุโลก",
        zipcode: 65000,
        tel: "088-88889999"
      }
    ]);
  }

  public address_credit_card() {
    let items: Array<{
      card_type: string;
      card_number: string;
      card_expiration: string;
    }>;
    return (items = [
      {
        card_type: "Visa",
        card_expiration: "09 / 2019",
        card_number: "XXXX-XXXX-XXXX-XXXX"
      },
      {
        card_type: "Visa",
        card_expiration: "09 / 2019",
        card_number: "XXXX-XXXX-XXXX-XXXX"
      }
    ]);
  }

  public cart() {
    let items: Array<{ name: string; image: string; price: number }>;

    return (items = [
      {
        name: "กล่องบรรจุภัณฑ์",
        price: 25000,
        image: "http://www.b-smartprint.com/7trayang/images/stampnormal.jpg"
      },
      {
        name: "สายคล้องคอ",
        price: 250,
        image: "http://www.b-smartprint.com/7trayang/images/stampnormal.jpg"
      }
    ]);
  }

  public order_instock () {
    /*
    let item: Product_Model[] = [];
    let item2: Product_Model = new Product_Model();
    item2 = {
      product_id: 1234,
      category_id: 1,
      product_name: 'ตรายางแป้นธรรมดา',
      product_image: 'http://www.b-smartprint.com/7trayang/images/stampnormal.jpg',
      product_describe: '---------------------',
      unit_price: 100,
      unit_in_stock: 4,
      product_time: 7,
    };
    item.push(item2);
    item2 = {
      product_id: 1235,
      category_id: 1,
      product_name: 'ตรายางออฟเช็ต',
      product_image: 'http://www.b-smartprint.com/7trayang/images/type-prod/stamp-offset.png',
      product_describe: '---------------------',
      unit_price: 200,
      unit_in_stock: 5,
      product_time: 7,
    };
    item.push(item2);
    item2 = {
      product_id: 1235,
      category_id: 1,
      product_name: 'ตรายางหมึกในตัว',
      product_image: 'http://www.b-smartprint.com/7trayang/images/type-prod/stamp-int.png',
      product_describe: '---------------------',
      unit_price: 100,
      unit_in_stock: 5,
      product_time: 7,
    };
    item.push(item2);
    item2 = {
      product_id: 1235,
      category_id: 1,
      product_name: 'ตรายางออฟเช็ต',
      product_image: 'http://www.b-smartprint.com/7trayang/images/type-prod/stamp-sumret.png',
      product_describe: '---------------------',
      unit_price: 200,
      unit_in_stock: 2,
      product_time: 7,
    };
    item.push(item2);
    item2 = {
      product_id: 1235,
      category_id: 1,
      product_name: 'ด้ามไม้รางรถไฟ',
      product_image: 'http://www.b-smartprint.com/7trayang/images/type-prod/stamp-mai.png',
      product_describe: '---------------------',
      unit_price: 200,
      unit_in_stock: 6,
      product_time: 7,
    };
    item.push(item2);
    return item;
    */
  }
}
