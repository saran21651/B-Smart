export class ArrayChecker {

    constructor () {}

    public IsArray(data): boolean {
        return Object.prototype.toString.call(data) === "[object Array]";
    }
}