export class ImageUtil {

  public datauri_to_blob(base64image) {
    try {
      let binary = atob(base64image.split(',')[1]);
      let arr = [];
      for (let i = 0; i < binary.length; i++) {
        arr.push(binary.charCodeAt(i));
      }
      return new Blob([new Uint8Array(arr)], {
        type: "image/jpeg"
      });
    } catch (error) {
      console.log(error);
      return;
    }
  }
}
