export class Datetime {

  public static getTimeInMilliseconds () {
    return new Date().getTime().toString();
  }

  // return Thu Aug 23 2018 10:51:18 GMT+0
  public static getConvertMil_to_InString (time) {
    return new Date(parseInt(time)).toString();
  }

  // return 12 june 2018
  public static getDate_Order_History (time) {
    let full_time = this.getConvertMil_to_InString(time);
    let full_time_arr = full_time.split(' ');
    return full_time_arr[2] + ' ' + full_time_arr[1] + ' ' + full_time_arr[3];
  }

  // TODO - Create fucntion for convert timestamp
  // Ex. July 12, 9.33 am;
  // Mon Jul 30 2018 20:27:03 GMT+0700 (Indochina Time)
  public static get_chat_timestamp (timeStamp) : string {
    let full_date = new Date(Number(timeStamp)).toString();
    let full_time = new Date(Number(timeStamp)).toLocaleTimeString();
    let full_date_arr = full_date.split(' ');
    let full_time_arr = full_time.split(' ');
    let time_arr = full_time_arr[0].split(':');
    let return_date = "";
    if (full_time_arr[1] != undefined) {
      return_date = full_date_arr[1] +  ' ' + full_date_arr[2] + ', ' + time_arr[0] + '.' + time_arr[1] + ' ' + full_time_arr[1];
    } else {
      return_date = full_date_arr[1] +  ' ' + full_date_arr[2] + ', ' + time_arr[0] + '.' + time_arr[1];
    }
    return return_date;
  }

  // Convert date to timestamp
  // Ex: // Fulldate
  // Thu Sep 20 2018 10:22:00 GMT+0700 (Indochina Time)
  // date : Thu Sep 20 2018
  // time : 10:22:00 GMT+0700 (Indochina Time)
  public static get_timestamp_from_full_date (date: string, time: string) {

    let date_arr = String(date).split(" ");
    let time_arr = String(time).split(" ");
    let partial_date = date_arr[0] + " " + date_arr[1] + " " + date_arr[2] + " " + date_arr[3];
    let partial_time = "";
    /*
    if (time_arr.length == 7) {
      partial_time = time_arr[4] + " " + time_arr[5] + " " + time_arr[6]// + " " + time_arr[7];
    }
    if (time_arr.length == 6) {
      partial_time = time_arr[4] + " " + time_arr[5]// + " " + time_arr[6];
    }
    */
    partial_time = time_arr[4] + " " + time_arr[5];
    let full_date = partial_date + " " + partial_time;
    let d = new Date(full_date);
    return d.getTime();
  }

  public static get_partial_datetime_from_picker (datetime, flag:number) {
    /*
        0 : Date
        1 : Time
        Thu Sep 20 2018 10:22:00 GMT+0700 (Indochina Time)
    */
    let d = String(datetime).split(" ");
    if (flag == 0) {
      return d[1] + " " + d[2] + " " + d[3];
    }
    if (flag == 1) {
      return d[4];
    }
  }

  public static get_current_date (flag: number) {
    /*
        0 : Date
        1 : Time
        Thu Sep 20 2018 10:22:00 GMT+0700 (Indochina Time)
    */
    let date = new Date().toString();
    let d = date.split(" ");
    console.log(date);
    if (flag == 0) {
      return d[1] + " " + d[2] + " " + d[3];
    }
    if (flag == 1) {
      return d[4];
    }
    if (flag == 2) {
      return date;
    }
  }
  /*
  function myFunction() {
    var s = "2018-09-20T10:22:00";
      var b = s.split(/\D/);
      var d = new Date(b[0], b[1]-1, b[2], b[3], b[4], b[5]);
      var dd = d.getTime();
      var c = new Date().getTime();
      document.getElementById("demo").innerHTML = d; //  Thu Sep 20 2018 10:22:00 GMT+0700 (Indochina Time)
      document.getElementById("demo2").innerHTML = dd; // 1537413720000
      document.getElementById("demo3").innerHTML = c; //  1537413764449
  }
  */
}
