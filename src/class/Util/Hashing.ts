import { Md5 } from 'ts-md5/dist/md5';

export class Hashing {

    constructor () {}

    public MD5 (words: string) : string {
        let result = "";

        result = Md5.hashStr(words).toString();
        return result;
    }
}