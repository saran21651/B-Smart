import { UserInformationModel } from './../Model/UserData/UserInformation';
import { Datetime } from './../Util/Datetime';
import { Storage } from '@ionic/storage';
export class User_Storage {

    constructor (public storage: Storage) {
        //this.Test();
    }

    public Test () {
        this.storage.get('Test').then((val) => {
            console.log('Test Message: ', val);
        });
        this.storage.set('Test1', '654321');
    }

    public async Test_Return () {
        /*
        return new Promise(resolve => {
            this.storage.ready().then(() => {
                this.storage.get('Test1').then((value) => {
                    resolve(value);
                });
            });
        });
        */
       return await this.storage.get('Test1');
    }

    public Clear () {
      this.storage.clear();
    }

    public async Set_User_Information (item: any) {
      this.storage.set('user_pname', item['pname']);
      this.storage.set('user_fname', item['fname']);
      this.storage.set('user_lname', item['lname']);
      this.storage.set('user_image', item['image']);
      this.storage.set('user_uid', item['uid']);
      this.storage.set('user_email', item['email']);
      this.storage.set('user_join_date', Datetime.getConvertMil_to_InString(item['join_date']));
    }

    public async Get_User_Information () {
      let item = UserInformationModel.prototype;
      item = {
        pname: await this.storage.get('user_pname'),
        fname: await this.storage.get('user_fname'),
        lname: await this.storage.get('user_lname'),
        image: await this.storage.get('user_image'),
        email: await this.storage.get('user_email'),
        join_date: await this.storage.get('user_join_date'),
        uid: await this.storage.get('user_uid'),
        online: await this.storage.get('user_online'),
        line: await this.storage.get('user_line'),
        tel: await this.storage.get('user_tel')
      }
      return await item;
    }
}
