import { AlertController } from "ionic-angular";
export class AlertControl {

  constructor(
    private alertCtrl: AlertController
  ) {}

  Error(message: string) {
    this.alertCtrl.create({
      subTitle: message,
      buttons: ["OK"]
    }).present();
  }
}
