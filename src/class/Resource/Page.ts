export const Page = {
  Authentication: {
    Login: {
      Page: "LoginPage"
    },
    Register: {
      Page: "RegisterPage"
    }
  },
  Menu: {
    Page: "MainMenuPage",
    Check: {
      Page: "MainMenuCheckPage"
    },
    Chat: {
      Page: "MainMenuChatPage",
      Contact: {
        Page: "MainMenuContactPage"
      },
      Image: {
        Page: "MainMenuChatImagePage"
      },
      Preorder_List : {
        Page: "MainMenuChatPreorderListPage"
      },
      Modal: {
        Info: "MainMenuChatInfoPage",
        Call: "MainMenuChatCallPage",
        Select: "MainMenuChatSelectPage",
        Order_Info: "MainMenuChatOrderInfoPage",
        PreOrder: "MainMenuChatPreorderPage"
      }
    },
    Option: {
      Page: "MainMenuOptionPage"
    },
    Order: {
      Page: "MainMenuOrderTabsPage",
      Detail : {
        Page : "MainMenuOrderDetailPage",
        Modal: "MainMenuOrderDetailModalPage"
      },
      Tabs : {
        PreOrder: "MainMenuOrderPreorderPage",
        InStock: "MainMenuOrderInstockPage",
        InStockAccordion: "MainMenuOrderInstockAccordionPage",
        Modal: "MainMenuOrderModalPage"
      }
    },
    Promotion: {
      Page: "MainMenuPromotionPage",
      Detail: {
        Page: "MainMenuPromotionDetailPage"
      }
    },
    OrderTracking: {
      Page: "MainMenuOrderTrackingPage",
      Detail: {
        Page: "MainMenuOrderTrackingDetailPage",
      },
      DetailPreorder: {
        Page: "MainMenuOrderTrackingDetailPreorderPage",
      },
      Accept: {
        Page: "MainMenuOrderTrackingAcceptPage"
      },
      Comment : {
        Page: "MainMenuOrderTrackingCommentModalPage"
      }
    },
    Purchase: {
      Page: "MainMenuPurchasePage",
      Bank: {
        Page: "MainMenuPurchaseBankPage"
      },
      CreditCard: {
        Page: "MainMenuPurchaseCreditCardPage"
      },
      PaymentInform: {
        Page: "MainMenuPurchasePaymentinformPage"
      }
    },
    Profile: {
      Page: "MainMenuProfilePage",
      Edit: {
        Page: "MainMenuProfileEditPage"
      },
      CreditCard: {
        Page: "MainMenuProfileCreditCardPage",
        Add : {
          Page: "MainMenuProfileCreditCardAddPage"
        },
        Edit : {
          Page: "MainMenuProfileCreditCardEditPage"
        },

      },
      Address: {
        Page: "MainMenuProfileAddressBookPage",
        Add: {
          Page: "MainMenuProfileAddressBookAddPage"
        },
        Edit: {
          Page: "MainMenuProfileAddressBookEditPage"
        },
        Modal: {
          Delete: "MainMenuProfileAddressBookDeletePage"
        }
      },
      Order: {
        Page: "MainMenuProfileOrderHistoryPage",
        Detail: {
          Page: "MmpOrderHistoryDetailPage"
        }
      },
    },
    Cart : {
      Page : 'MainMenuCartListPage',
      Checkout : {
        Page: 'MainMenuCartCheckoutPage'
      },
      Checkout_Preorder : {
        Page: 'MainMenuCartCheckoutPreorderPage'
      }
    },
    News: {
      Page: "MainMenuNewsPage",
      Detail : {
        Page: "MainMenuNewsDetailPage"
      }
    }
  },
  Debug: {
    Page: "DebugPage"
  },
  Blank : {
    Page : "BlankPage"
  }
};
