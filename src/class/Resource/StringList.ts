/*
export class GlobalString {

  public static SIDEMENU_NAME = "menu-avatar";
}
*/

export const GlobalString = {
  SIDEMENU : {
    MENU_AVATAR: "menu-avatar"
  }
}
// Message Chat
export const MessageChat = {
  MessageType: {
    TEXT: 'text',
    IMAGE: 'image',
    FILE: 'file'
  },
  ChatType: {
    CUSTOMER_TO_SALE: 'customer_to_sale',
    STAFF_TO_STAFF: 'staff_to_staff',
    CUSTOMER_TO_SALE_PREORDER: 'customer_to_sale_preorder'
  },
  Seen: {
    READ: 'read',
    NOT_READ: 'not_read'
  }
}

export const BankNameList = [
  "ธนาคารกรุงเทพ",
  "ธนาคารกรุงไทย",
  "ธนาคารกรุงศรีอยุธยา",
  "ธนาคารกสิกรไทย",
  "ธนาคารเกียรตินาคิน",
  "ธนาคารซีไอเอ็มบี",
  "ธนาคารทหารไทย",
  "ธนาคารทิสโก้",
  "ธนาคารธนชาติ",
  "ธนาคารยูโอบี",
  "ธนาคารแลนด์ แอนด์ เฮ้าส์",
  "ธนาคารไทยพาณิชย์",
  "ธนาคารสแตนดาร์ดชาร์เตอร์ด",
  "ธนาคารไอซีบีซี"
]