import { Address_Model } from './../Address/address';
export class Order_Model {
  public address : string;
  public delivery_type : string;
  public order_id : string;
  public order_list : Array<Order_list>;
  public order_status : string;
  public order_status_text : string;
  public search : {
    order_status: string;
  };
  public timestamp : {
    confirm_order_time: string,
    delivered_time: string,
    delivering_time: string,
    order_time: string,
    purchase_time: string
  };
  public uid : string;
  public user_information;
}

export class Order_list {
  public amount: string;
  public category_id : string;
  public product_description: string;
  public product_id: string;
  public product_image: string;
  public product_name: string;
  public product_price : {
    currency: string;
    price: string;
  };
  public product_type : string;
  public product_unit : string;
  public sum_price: string;
}

export class Order_History_Model {
  address: Address_Model = new Address_Model();
  delivery_type: string;
  order_id: string;
  order_list: Array<Order_list> = [];
  order_status: string;
  order_status_text: string;
  payment: any;
  search: any;
  timestamp: any;
  uid: string;
  user_information: any;
}
