export class Product_Model {
  /*
  product_id: number;
  category_id: number;
  product_name: string;
  product_image: string;
  product_describe: string;
  product_time: number;
  unit_price: number;
  unit_in_stock: number;
  */
  category_id: string;
  options: any;
  product_name: string;
  product_image: string;
  product_description: string;
  product_time: number;
  product_unit: string;
  product_stock: number;
  product_id: string;
  product_type: string;
  timestamp: string;
  price : {
    product_price: number,
    design_price: number,
    promotion_price: number,
    total_price: number
  }
}

export class Product_List_Model {
  product: Product_Model;
}
