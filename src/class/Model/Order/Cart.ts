import { Address_Model } from './../Address/address';
import { Product_Model } from "./Product";

export class Cart_Model {
  quantity: number;
  product: Product_Model;
  // address: Address_Model = new Address_Model();
}

export class Cart_Additional_Model {
  address: Address_Model = new Address_Model();
  delivery_type: string;
  payment: {
    method_type: string,
    detail: any;
  };
  price: {
    product_price: number,
    shipping_price: number,
    promotion_price: number,
    total_price: number
  } = {
    product_price: 0,
    shipping_price: 0,
    promotion_price: 0,
    total_price: 0,
  };
}
