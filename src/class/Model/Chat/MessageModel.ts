export class MessageModel {
    public message: string;
    public sender_id: string;
    public receiver_id: string;
    public image: string;
    public timeStamp: string;
    public read: string;
    public name: string;
    public file: string;
    public message_type: string;
    // public type: string;
}
