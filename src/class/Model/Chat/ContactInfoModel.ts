export class ContactInfoModel {
  public uid: string;
  public pname: string;
  public fname: string;
  public lname: string;
  public timeStamp: string;
  public image: string;
  public fullname: string;

  public contact_type: string;
  public employee_company: string;
  public employee_department: string;
  public employee_id: string;
  public employee_position: string;
  public employee_type: string;
  public phone: string;

  public getFullname() {
    return this.pname + " " + this.fname + " " + this.lname;
  }
}
