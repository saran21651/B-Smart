export class UserInformationModel {

  public fname: string;
  public lname: string;
  public pname: string;
  public image: string;
  public join_date: string;
  public uid: string;
  public email: string;
  public online: string;
  public line: string;
  public tel: string;
}
