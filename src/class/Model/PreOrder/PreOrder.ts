

export class PreOrder_Model {

    public Product_Id;
    public Product_Category;
    public Product_Name;
    public Product_Description;
    public Product_Image;
    public Amount;
    public Product_Price;
    public Employee_ID;
    public Customer_ID;
}