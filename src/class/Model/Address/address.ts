export class Address_Model {
  public fname: string;
  public lname: string;
  public address: string;
  public city: string;
  public province: string;
  public zipcode: string;
  public telephone: string;
}
