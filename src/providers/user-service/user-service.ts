import { Datetime } from "./../../class/Util/Datetime";
import { Storage } from "@ionic/storage";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { UserInformationModel } from "../../class/Model/UserData/UserInformation";
import { resolveDefinition } from "../../../node_modules/@angular/core/src/view/util";

@Injectable()
export class UserServiceProvider {
  constructor(private http: HttpClient, private storage: Storage) {
    console.log("Hello UserServiceProvider Provider");
  }

  public set_user_information(item: any) {
    return new Promise<any>((resolve, reject) => {
      this.storage.set("user_pname", item["pname"]);
      this.storage.set("user_fname", item["fname"]);
      this.storage.set("user_lname", item["lname"]);
      this.storage.set("user_image", item["image"]);
      this.storage.set("user_uid", item["uid"]);
      this.storage.set("user_email", item["email"]);
      this.storage.set("user_join_date",Datetime.getConvertMil_to_InString(item["join_date"]));
      this.storage.set("user_tel", item["tel"]);
      resolve();
    });
  }

  public get_user_information() {
    return new Promise<any>(async (resolve, reject) => {
      let item = UserInformationModel.prototype;
      item = {
        pname: await this.storage.get("user_pname"),
        fname: await this.storage.get("user_fname"),
        lname: await this.storage.get("user_lname"),
        image: await this.storage.get("user_image"),
        email: await this.storage.get("user_email"),
        join_date: await this.storage.get("user_join_date"),
        uid: await this.storage.get("user_uid"),
        online: await this.storage.get("user_online"),
        line: await this.storage.get("user_line"),
        tel: await this.storage.get("user_tel")
      };
      resolve(item)
    });
  }

  public get_user_id () {
    return new Promise<any>(async (resolve, reject) => {
      let uid = await this.storage.get("user_uid");
      resolve(uid);
    });
  }

  public check_if_user_logged () {
    return new Promise<any>((resolve, reject) => {
      this.storage.get("user_uid").then((res) => {
 
        if (res !== null || undefined) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  }

  public clear_user_information () {
    return new Promise<any>((resolve, reject) => {
      this.storage.clear().then(() => resolve, () => reject);
    });
  }

  // Remenber order id
  public set_order_id (id) {
    this.storage.set('user_order_id', id);
  }

  public get_order_id () {
    return new Promise<any>(async (resolve, reject) => {
      let id = await this.storage.get('user_order_id');
      resolve(id);
    });
  }

  public clear_order_id () {
    this.storage.remove('user_order_id');
  }

  public set_order_key (key) {
    this.storage.set('user_order_key', key);
  }

  public get_order_key () {
    return new Promise<any>(async (resolve, reject) => {
      let id = await this.storage.get('user_order_key');
      resolve(id);
    });
  }

  public clear_order_key () {
    this.storage.remove('user_order_key');
  }

  public set_preorder_id (id) {
    this.storage.set('user_preorder_id', id);
  }

  public get_preorder_id () {
    return new Promise<any>(async (resolve, reject) => {
      let id = await this.storage.get('user_preorder_id');
      resolve(id);
    });
  }

  public clear_preorder_id () {
    this.storage.remove('user_preorder_id');
  }

  public set_preorder_key (key) {
    this.storage.set('user_preorder_key', key);
  }

  public get_preorder_key () {
    return new Promise<any>(async (resolve, reject) => {
      let id = await this.storage.get('user_preorder_key');
      resolve(id);
    });
  }

  public clear_preorder_key () {
    this.storage.remove('user_preorder_key');
  }


}
