import { FirebaseProvider } from "./../firebase/firebase";
import {
  ToastController,
  AlertController,
  LoadingController
} from "ionic-angular";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Market } from "@ionic-native/market";
import { AuthServiceProvider } from "../auth-service/auth-service";

@Injectable()
export class ControllerProvider {
  constructor(
    private toastController: ToastController,
    private alertController: AlertController,
    private loadController: LoadingController,
    private firebaseprovider: FirebaseProvider,
    private auth: AuthServiceProvider,
    private market: Market
  ) {
    console.log("Hello ControllerProvider Provider");
  }

  private loadingObj;

  // Top Bottom Middle
  public Show_Toast(msg: string, position: string) {
    let toast = this.toastController.create({
      message: msg,
      duration: 2000,
      position: position
    });
    toast.present();
  }

  public Show_Toast_Debug(msg: string, position: string) {
    let toast = this.toastController.create({
      message: msg,
      duration: 50000,
      position: position
    });
    toast.present();
  }
  // Flags mode
  //
  public async Show_Alert_Confirm(mode_flag, msg?, data?) {
    return await new Promise<any>(async resolve => {
      let wait = await this.alert_mode(msg, mode_flag, data);
  
      resolve(wait);
    });
  }

  public Show_Alert_Confirm_Debug (obj) {
    /*
      chat_type
      chatroom
      contact_id
      order_info
      user_id
    */
    let str = obj['chat_type'] + ' ' + obj['chatroom'];
  }

  public Show_Alert_Success(mode_flag: Number, title, subTitle) {
    return new Promise<any>(resolve => {
      let param;
      switch (mode_flag) {
        case 1:
          param = {
            title: title,
            subTitle: subTitle,
            buttons: [
              {
                text: "Dismiss",
                handler: () => {
                  resolve();
                }
              }
            ]
          };
          break;
        case 2:
          param = {
            title: title,
            subTitle: subTitle,
            buttons: [
              {
                text: "ตกลง",
                handler: () => {
                  this.auth.get_app_metadata().then((res) => {
                    this.market.open(res['package_name']);
                    resolve();
                  });
                }
              }
            ]
          };
        default:
      }
      let alert = this.alertController.create(param);
      alert.present();
    });
  }

  private alert_mode(msg, mode_flag, data) {
    return new Promise<any>(resolve => {
      let mode;
      switch (mode_flag) {
        // Delete address
        case 1:
          mode = {
            title: "คุณต้องการลบที่อยู่ใช่หรือไม่?",
            buttons: [
              {
                text: "ลบ",
                handler: async () => {
                  this.firebaseprovider
                    .Delete_Address_Book(data["key"])
                    .then(res => {
                      this.Show_Toast("ทำการลบที่อยู่ สำเร็จ", "bottom");
                      resolve();
                    });
                }
              },
              {
                text: "ยกเลิก",
                handler: async () => {
                  resolve();
                }
              }
            ]
          };
          break;
        case 2:
          mode = {
            title: "คุณต้องการยกเลิกการสั่งซื้อใช่หรือไม่?",
            buttons: [
              {
                text: "ใช่",
                handler: async () => {
                  this.firebaseprovider.Cancel_Order(data).then(res => {
                    resolve(true);
                  });
                }
              },
              {
                text: "ไม่ใช่",
                handler: async () => {
                  resolve();
                }
              }
            ]
          };
          break;
          case 3: // Preorder
          mode = {
            title: "คุณต้องการยกเลิกการสั่งซื้อใช่หรือไม่?",
            buttons: [
              {
                text: "ใช่",
                handler: async () => {
                  this.firebaseprovider.Cancel_PreOrder(data).then(res => {
                    resolve(true);
                  });
                }
              },
              {
                text: "ไม่ใช่",
                handler: async () => {
                  resolve();
                }
              }
            ]
          };
          break;
        default:
          console.log("error");
      }
      let alert = this.alertController.create(mode);
      alert.present();
    });
  }

  public Show_Loading() {
    this.loadingObj = this.loadController.create({
      spinner: "hide",
      content: `
      <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
      `,
      cssClass: "transparent"
    });
    this.loadingObj.present();
  }

  public Hide_Loading() {
    this.loadingObj.dismiss();
  }

  public Show_Not_Complete_Section () {
    const alert = this.alertController.create({
      message: 'ยังไม่พร้อมเปิดใช้บริการ'
    });
    alert.present();
  }
}
