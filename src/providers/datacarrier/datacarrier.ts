import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class DatacarrierProvider {

  private data_order: any;

  constructor(public http: HttpClient) {
    console.log('Hello DatacarrierProvider Provider');
  }

  public Set_Order_Tabs_Data (data) {
    this.Clear();
    this.data_order = data;
  }

  public Get_Order_Tabs_Data () {
    let temp = this.data_order;
    this.Clear();
    return temp;
  }

  private Clear () {
    // this.data_order = "";
    delete this.data_order;
  }

}
