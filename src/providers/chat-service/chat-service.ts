import { ImageUtil } from "./../../class/Util/ImageUtil";
import { ControllerProvider } from "./../controller/controller";
import { MessageModel } from "./../../class/Model/Chat/MessageModel";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { MessageChat } from "./../../class/Resource/StringList";
import { OnesignalProvider } from "./../onesignal/onesignal";
import { UserServiceProvider } from "./../user-service/user-service";
import { Datetime } from "./../../class/Util/Datetime";
import { ContactInfoModel } from "./../../class/Model/Chat/ContactInfoModel";
import { AngularFireDatabase } from "angularfire2/database";
import { Injectable, NgZone } from "@angular/core";
import { Events } from "ionic-angular";
import { Chat_metadataModel } from "../../class/Model/Chat/Chat_metadataModel";
import { UserInformationModel } from "../../class/Model/UserData/UserInformation";
import { AngularFireStorage } from "angularfire2/storage";

@Injectable()
export class ChatServiceProvider {
  // Firebase on event
  private fbe_contact_list: any;
  private fbe_message_list: any;
  private fbe_set_seen: any;
  private fbe_contact_list_department: any;
  private fbe_contact_list_customer: any;
  private fbe_contact_list_customer_preorder: any;
  private fbe_contact_list_preorder: any;
  private fbe_preorder_change: any;
  //
  public EVENT = {
    CONTACT_LIST: "chat:contact_list",
    CONTACT_LIST_DEPARTMENT: "chat:contact_list_department",
    CONTACT_LIST_CUSTOMER: "chat:contact_list_customer",
    CONTACT_LIST_CUSTOMER_PREORDER: "chat:contact_list_customer_preorder",
    CONTACT_LIST_PREORDER: "chat:contact_list_preorder",
    MESSAGE_LIST: "chat:message_list",
    SET_SEEN: "chat:set_seen",
    PREORDER: "chat:preorder"
  };

  public CONTACT_TYPE = {
    EMPLOYEE: "con:employee",
    CUSTOMER: "con:customer"
  };

  // private properties
  private user_info: any = UserInformationModel.prototype;
  private chatroom_info: {
    chatroom: string;
    user_id: string;
    contact_id: string;
    contact_onesignal_id: string;
    image: string;
    fname: string;
    // optional_data: any;
  };
  private chat_type: any;
  private messages = [];
  private limit_load_chat = 10;
  private current_chat_load = 0;
  private key;
  private content;
  //
  constructor(
    private firebase: AngularFireDatabase,
    private firestorage: AngularFireStorage,
    private userservice: UserServiceProvider,
    private onesignalservice: OnesignalProvider,
    private controller: ControllerProvider,
    private zone: NgZone,
    private events: Events,
    private photoviewer: PhotoViewer,
    private camera: Camera
  ) {
    console.log("Hello ChatServiceProvider Provider");
    this.zone = new NgZone({ enableLongStackTrace: false });
  }

  private scrollToBottom() {}

  public Get_Message() {
    return this.messages;
  }

  public Get_Chatroom_Info() {
    return this.chatroom_info;
  }

  // Load Available Contact List
  /*
     Return event with OBJECT
     {
       online: data,
       offline: data
     }
  */
  public Load_Contact_List() {
    this.Turn_Off_Firebase_Event(this.EVENT.CONTACT_LIST);
    this.fbe_contact_list = this.firebase.database.ref("user_information");
    this.fbe_contact_list.on("value", snap => {
      let contacts_online = [];
      let contacts_offline = [];
      if (snap.exists()) {
        this.zone.run(async () => {
          contacts_online = [];
          contacts_offline = [];
          let uid = await this.userservice.get_user_id();
          snap.forEach(child => {
            let contact_info = new ContactInfoModel();

            contact_info.pname = child.child("pname").val();
            contact_info.fname = child.child("fname").val();
            contact_info.lname = child.child("lname").val();
            contact_info.uid = child.child("auth/uid").val();
            contact_info.image = child.child("image").val();
            contact_info.phone = child.child("phone").val();
            contact_info.fullname = contact_info.getFullname();
            contact_info.contact_type = this.CONTACT_TYPE.CUSTOMER;

            if (contact_info.uid != uid) {
              if (child.child("online").val() === "online") {
                contacts_online.push(contact_info);
              }

              if (child.child("online").val() === "offline") {
                contacts_offline.push(contact_info);
              }
            }
          });
          let result = {
            online: contacts_online,
            offline: contacts_offline
          };
          this.events.publish(this.EVENT.CONTACT_LIST, result);
        });
      } else {
      }
    });
    return this.Subscribe_Event();
  }

  public Load_Contact_List_Department() {
    this.Turn_Off_Firebase_Event(this.EVENT.CONTACT_LIST_DEPARTMENT);
    this.fbe_contact_list_department = this.firebase.database.ref(
      "employee_information"
    );
    this.fbe_contact_list_department.on("value", snap => {
      let contact_department = [];
      let contact_list = [];
      if (snap.exists()) {
        this.zone.run(async () => {
          let uid = await this.userservice.get_user_id();
          snap.forEach(child => {
            let contact_info = new ContactInfoModel();
            contact_info.pname = child.child("pname").val();
            contact_info.fname = child.child("fname").val();
            contact_info.lname = child.child("lname").val();
            contact_info.uid = child.child("uid").val();
            contact_info.image = child.child("image").val();
            contact_info.fullname = contact_info.getFullname();
            contact_info.contact_type = this.CONTACT_TYPE.EMPLOYEE;
            contact_info.employee_company = child
              .child("employee_company")
              .val();
            contact_info.employee_department = child
              .child("employee_department")
              .val();
            contact_info.employee_id = child.child("employee_id").val();
            contact_info.employee_position = child
              .child("employee_position")
              .val();
            contact_info.employee_type = child.child("employee_type").val();
            contact_info.phone = child.child("phone").val();
            let exists_department = contact_department.find(
              res => res == contact_info.employee_department
            );

            if (exists_department == undefined) {
              contact_department.push(contact_info.employee_department);
            }
            if (uid != contact_info.uid) {
              contact_list.push(contact_info);
            }
          });
          let result: any = [];
          contact_department.forEach(department => {
            let contact_info = [];
            contact_list.forEach(contact => {
              if (department == contact["employee_department"]) {
                contact_info.push(contact);
              }
            });
            // result[department] = {};
            // result[department] = contact_info;
            result.push({
              contact_list: contact_info,
              department: department
            });
          });
          this.events.publish(this.EVENT.CONTACT_LIST_DEPARTMENT, result);
        });
      } else {
      }
    });
    return this.Subscribe_Event();
  }

  public Load_Contact_List_Customer() {
    this.Turn_Off_Firebase_Event(this.EVENT.CONTACT_LIST_CUSTOMER);
    
    this.fbe_contact_list_customer = this.firebase.database.ref(
      "chat_guest"
    );
    
    this.fbe_contact_list_customer.on("value", async snap => {
      let contact_list = [];
      if (snap.exists()) {
        this.zone.run(async () => {
          let uid = await this.userservice.get_user_id();
          snap.forEach(child => {
            let contact_info: any = child.val();
            //contact_info["chat1"] = child.child("chat1").val();
            //contact_info["chat2"] = child.child("chat2").val();
            //contact_info["chat_type"] = child.child("chat_type").val();
            //contact_info["chatroom"] = child.child("chatroom").val();
            contact_info["uid"] = uid;
            if (
              contact_info["chat_type"] == MessageChat.ChatType.CUSTOMER_TO_SALE
            ) {
              if (
                contact_info["chat1"]["uid"] == uid ||
                contact_info["chat2"]["uid"] == uid
              ) {
                if (contact_info["chat1"]["uid"] == uid) {
                  contact_info["display"] = contact_info["chat2"];
                }
                if (contact_info["chat2"]["uid"] == uid) {
                  contact_info["display"] = contact_info["chat1"];
                }
                contact_info["display"]["fullname"] =
                  contact_info["display"]["pname"] +
                  " " +
                  contact_info["display"]["fname"] +
                  " " +
                  contact_info["display"]["lname"];
                contact_list.push(contact_info);
              }
            }
          });
          this.events.publish(this.EVENT.CONTACT_LIST_CUSTOMER, contact_list);
        });
      } else {
      }
    });

    
    return this.Subscribe_Event();
  }

  public Load_Contact_List_Customer_Preorder () {
    this.Turn_Off_Firebase_Event(this.EVENT.CONTACT_LIST_CUSTOMER_PREORDER);
    this.fbe_contact_list_customer_preorder = this.firebase.database.ref(
      "chat_preorder"
    );
    this.fbe_contact_list_customer_preorder.on("value", async snap => {
      let contact_list = [];
      if (snap.exists()) {
        this.zone.run(async () => {
          let uid = await this.userservice.get_user_id();
          snap.forEach(child => {
            let contact_info = child.val();
            contact_info["uid"] = uid;
            if (contact_info["chat2"]["uid"] == uid) {
              contact_info["display"] = contact_info["chat1"];
              contact_info["display"]["fullname"] =
              contact_info["display"]["pname"] +
              " " +
              contact_info["display"]["fname"] +
              " " +
              contact_info["display"]["lname"];
              contact_list.push(contact_info);
            }
          });
        });
        console.log(contact_list);
        this.events.publish(this.EVENT.CONTACT_LIST_CUSTOMER_PREORDER, contact_list);
      } else {
      }
    });
    return this.Subscribe_Event();
  }

  public Load_Contact;

  private Turn_Off_Firebase_Event(event) {
    switch (event) {
      case this.EVENT.CONTACT_LIST:
        if (this.fbe_contact_list != undefined) {
          this.fbe_contact_list.off();
        }
        break;
      case this.EVENT.CONTACT_LIST_DEPARTMENT:
        if (this.fbe_contact_list_department != undefined) {
          this.fbe_contact_list_department.off();
        }
        break;
      case this.EVENT.MESSAGE_LIST:
        if (this.fbe_message_list != undefined) {
          this.fbe_message_list.off();
        }
        break;
      case this.EVENT.SET_SEEN:
        if (this.fbe_set_seen != undefined) {
          this.fbe_set_seen.off();
        }
        break;
      case this.EVENT.CONTACT_LIST_CUSTOMER:
        if (this.fbe_contact_list_customer != undefined) {
          this.fbe_contact_list_customer.off();
        }
        break;
      case this.EVENT.CONTACT_LIST_PREORDER:
        if (this.fbe_contact_list_preorder != undefined) {
          this.fbe_contact_list_preorder.off();
        }  
        break;
    }
  }

  public Subscribe_Event() {
    return this.events;
  }

  public Unsubscribe_Event(event) {
    this.events.unsubscribe(event);
    this.Turn_Off_Firebase_Event(event);
  }

  public Observe_Messages() {
    return new Promise<any>(resolve => {
      this.events.subscribe(this.EVENT.MESSAGE_LIST, msg => {
        resolve();
      });
    });
  }

  public Remove_Observe_Message() {
    this.events.unsubscribe(this.EVENT.MESSAGE_LIST);
  }

  public Create_Chatroom(data, order?) {
    console.log(data);
    return new Promise<any>(async resolve => {
      let get_user_data = await this.userservice.get_user_information();

      let user1 = get_user_data["uid"]; // User
      let user2 = data["uid"]; // Contact
      let check = await this.Check_Chatroom_Exist(user1, user2);
      let chatroom = "";

      if (check.split("/")[0] === "true") {
        chatroom = check.split("/")[1] === "1" ? user1 + user2 : user2 + user1;
        let item = {
          data: {
            chatroom: chatroom,
            user_id: get_user_data["uid"],
            contact_id: data["uid"],
            chat_type: data["chat_type"],
            additional_data: order || ""
          }
        };
        if (order != undefined) {
          let wait = await this.Add_OrderId_To_Chatroom(chatroom, order);
        }
        
        resolve(item);
        return;
      }

      chatroom = user1 + user2;

      let user_data = {
        pname: get_user_data["pname"],
        fname: get_user_data["fname"],
        lname: get_user_data["lname"],
        image: get_user_data["image"],
        uid: get_user_data["uid"]
      };

      let contact_data = {
        pname: data["pname"],
        fname: data["fname"],
        lname: data["lname"],
        image: data["image"],
        uid: data["uid"]
      };

      let chat_metadata = new Chat_metadataModel();
      chat_metadata = {
        chat1: user_data,
        chat2: contact_data,
        chatroom: chatroom,
        chat_type: data["chat_type"],
        timeStamp: Datetime.getTimeInMilliseconds()
      };

      if (order != undefined) {
        chat_metadata['additional_data'] = order;
      }

      this.firebase.database
        .ref("chat_guest")
        .push()
        .set(chat_metadata)
        .then(res => {
          let item = {
            data: {
              chatroom: chatroom,
              user_id: get_user_data["uid"],
              contact_id: data["uid"],
              chat_type: data["chat_type"],
              additional_data: order || ""
            }
          };

          resolve(item);
        });
    });
  }

  private Add_OrderId_To_Chatroom (chatroom, order_id) {
    return new Promise<any>((resolve) => {
      this.firebase.database
        .ref("chat_guest")
        .orderByChild('chatroom')
        .equalTo(chatroom)
        .once("value", snap => {
          if (snap.exists()) {
            let key = Object.keys(snap.val())[0];
            this.firebase.database.ref("chat_guest/" + key)
            .update({additional_data: order_id})
            .then(res => {
              resolve();
            });
          } else {
            resolve();
          }
        });
    });
  }

  public Create_ChatRoom_Preorder(data, order_item) {
    return new Promise<any>(async resolve => {
      let get_user_data = await this.userservice.get_user_information();

      let customer = get_user_data["uid"];
      let sale = data["uid"];
      let order_id = data["order_id"];
      let check = await this.Check_Chatroom_Exist_Preorder(
        customer,
        sale,
        order_id
      );
      let chatroom = "";

      if (check) {
        chatroom = customer + sale + order_id;
        let item = {
          data: {
            chatroom: chatroom,
            user_id: customer,
            contact_id: sale,
            chat_type: data["chat_type"]
          }
        };

        let meta_data = {
          Product_Id: order_item["product_id"],
          Product_Category : order_item["category_id"],
          Product_Name: order_item["product_name"],
          Product_Description: "",
          Product_Image: order_item["product_image"],
          Amount: "",
          Product_Price: order_item["product_price"]["price"],
          Employee_ID: sale,
          Customer_ID: data["uid"]
        }

        this.firebase.database
        .ref("chat_preorder")
        .orderByChild("chatroom")
        .equalTo(item['data']['chatroom'])
        .once("value", snap => {
          if (snap.exists()) {
            snap.forEach(value => {
              let key = Object.keys(snap.val())[0];
              console.log(key);
              this.firebase.database
                .ref("chat_preorder/" + key + "/additional_data").update(meta_data).then(val => {
                resolve(item);
              });
            });
          } else {
            resolve();
          }
        })

        resolve(item);
        return;
      }

      chatroom = customer + sale + order_id;

      let customer_data = {
        pname: get_user_data["pname"],
        fname: get_user_data["fname"],
        lname: get_user_data["lname"],
        image: get_user_data["image"],
        uid: get_user_data["uid"]
      };

      let sale_metadata;

      let wait = await this.firebase.database
        .ref("employee_information")
        .orderByChild("uid")
        .equalTo(sale)
        .once("value", snap => {
          if (snap.exists()) {
            snap.forEach(value => {
              sale_metadata = value.val();
            });
          } else {
            sale_metadata = "";
          }
        });

      let sale_data = {
        pname: sale_metadata["pname"],
        fname: sale_metadata["fname"],
        lname: sale_metadata["lname"],
        image: sale_metadata["image"],
        uid: data["uid"]
      };

      let chat_metadata = {
        chat1: customer_data,
        chat2: sale_data,
        chatroom: chatroom,
        chat_type: data["chat_type"],
        timeStamp: Datetime.getTimeInMilliseconds(),
        additional_data: {
          Product_Id: order_item["product_id"],
          Product_Category : order_item["category_id"],
          Product_Name: order_item["product_name"],
          Product_Description: "",
          Product_Image: order_item["product_image"],
          Amount: "",
          Product_Price: order_item["product_price"]["price"],
          Employee_ID: sale,
          Customer_ID: data["uid"]
        }
      };
      this.firebase.database
        .ref("chat_preorder")
        .push()
        .set(chat_metadata)
        .then(snap => {
          let item = {
            data: {
              chatroom: chatroom,
              user_id: customer,
              contact_id: sale,
              chat_type: data["chat_type"]
            }
          };

          resolve(item);
        });
    });
  }

  private async Check_Chatroom_Exist(user1, user2): Promise<string> {
    let exists;
    let fire = this.firebase.database.ref("chat_guest");
    await fire
      .orderByChild("chatroom")
      .equalTo(user1 + user2)
      .once("value", snap => {
        if (snap.exists()) {
          exists = "true/1";
        } else {
          exists = "false";
        }
      });

    if (exists === "true/1") {
      return exists;
    }

    this.firebase.database.ref("chat_guest");
    await fire
      .orderByChild("chatroom")
      .equalTo(user2 + user1)
      .once("value", snap => {
        if (snap.exists()) {
          exists = "true/2";
        } else {
          exists = "false";
        }
      });

    return exists;
  }

  public Check_Chatroom_Exist_Preorder(user1: string, user2: string, order_id) {
    return new Promise<any>(async resolve => {
      let exists;
      let fire = await this.firebase.database
        .ref("chat_preorder")
        .orderByChild("chatroom")
        .equalTo(user1 + user2 + order_id)
        .once("value", snap => {
          if (snap.exists()) {
            exists = true;
          } else {
            exists = false;
          }
        });

      resolve(exists);
    });
  }

  public Setup_Chat(data, chat_type, limit_load_chat?: number) {
    return new Promise<any>(async resolve => {
      this.user_info = await this.userservice.get_user_information();
      this.chat_type = chat_type;
      this.limit_load_chat = limit_load_chat || this.limit_load_chat;
      let contact_onesignal_id = await this.onesignalservice.get_contact_player_id(
        data["contact_id"],
        this.chat_type
      );
      /*
      let opt_data = data['optional_data']; // For order in pre-order chattype
      if (opt_data == undefined) {
        opt_data = "";
      }
      */
      this.chatroom_info = {
        chatroom: data["chatroom"],
        user_id: data["user_id"],
        contact_id: data["contact_id"],
        contact_onesignal_id: contact_onesignal_id,
        image: this.user_info["image"],
        fname: this.user_info["fname"]
        //optional_data: opt_data
      };
      this.messages = [];
      let wait = await this.Initial_Chat().then(() => {
        resolve();
      });
    });
  }

  private Initial_Chat() {
    return new Promise<any>(resolve => {
      this.firebase.database
        .ref("chatroom")
        .child(this.chatroom_info["chatroom"])
        .limitToLast(this.limit_load_chat)
        .once("value", snap => {
          if (snap.exists()) {
            snap.forEach(child => {
              let message = child.val();
              message["key"] = child.key;
              message["timeStamp"] = Datetime.get_chat_timestamp(
                child.child("timeStamp").val()
              );
              this.messages.push(message);
              if (
                message["read"] == MessageChat.Seen.NOT_READ &&
                this.chatroom_info["user_id"] != message["sender_id"]
              ) {
                this.Set_Seen_In_Firebase(message["key"]);
              }

              this.key = child.key;
            });
            this.Load_Chat_Event();
            // this.scrollToBottom();
            resolve();
          } else {
            this.Load_Chat_Event();
            resolve();
          }
          // this.Load_Chat_Event();
        });
    });
  }

  private Set_Seen_In_Firebase(key: string) {
    this.firebase.database
      .ref("chatroom")
      .child(this.chatroom_info["chatroom"])
      .child(key)
      .child("read")
      .set(MessageChat.Seen.READ);
  }

  private Load_Chat_Event() {
    this.Turn_Off_Firebase_Event(this.EVENT.MESSAGE_LIST);
    this.Turn_Off_Firebase_Event(this.EVENT.SET_SEEN);
    this.fbe_message_list = this.firebase.database
      .ref("chatroom")
      .child(this.chatroom_info["chatroom"])
      .limitToLast(1);

    this.fbe_message_list.on("child_added", snap => {
      this.zone.run(() => {
        if (this.key != snap.key) {
          let message = snap.val();
          message["key"] = snap.key;
          message["timeStamp"] = Datetime.get_chat_timestamp(
            snap.child("timeStamp").val()
          );
          this.messages.push(message);
          if (
            message["read"] == MessageChat.Seen.NOT_READ &&
            this.chatroom_info["user_id"] != message["sender_id"]
          ) {
            this.Set_Seen_In_Firebase(message["key"]);
          }
          this.key = snap.key;

          this.events.publish(this.EVENT.MESSAGE_LIST, {
            messages: this.messages
          });

          // this.scrollToBottom();
        }
      });
    });

    this.fbe_set_seen = this.firebase.database
      .ref("chatroom")
      .child(this.chatroom_info["chatroom"])
      .limitToLast(1);

    this.fbe_set_seen.on("child_changed", snap => {
      if (snap.exists()) {
        // TODO: Check if working

        this.zone.run(() => {
          this.messages[this.messages.length - 1]["read"] = "read";
        });
      }
    });
  }

  public Load_Chat_Event_On_Refresh() {
    return new Promise<any>(resolve => {
      this.current_chat_load = this.messages.length + this.limit_load_chat;
      this.firebase.database
        .ref("chatroom")
        .child(this.chatroom_info["chatroom"])
        .limitToLast(this.current_chat_load)
        .once("value", async snap => {
          if (snap.exists()) {
            let number_chat = this.messages.length;
            let number_chat_load = snap.numChildren();

            if (number_chat_load > number_chat) {
              this.messages = [];
              // let count = number_chat_load - number_chat;

              snap.forEach(child => {
                let msg = child.val();
                msg["key"] = child.key;
                msg["timeStamp"] = Datetime.get_chat_timestamp(
                  child.child("timeStamp").val()
                );
                this.messages.push(msg);

                if (
                  msg["read"] == MessageChat.Seen.NOT_READ &&
                  this.chatroom_info["user_id"] != msg["sender_id"]
                ) {
                  this.Set_Seen_In_Firebase(msg["key"]);
                }
                resolve();

                return false;
              });
            } else {
              resolve();
            }
          } else {
            resolve();
          }
        });
    });
  }

  public Send_Text(message) {
    return new Promise<any>(async resolve => {
      if (message && message !== "") {
        let msg = MessageModel.prototype;
        msg = {
          message: message,
          sender_id: this.chatroom_info["user_id"],
          receiver_id: this.chatroom_info["contact_id"],
          image: this.chatroom_info["image"],
          timeStamp: Datetime.getTimeInMilliseconds(),
          read: MessageChat.Seen.NOT_READ,
          name: this.chatroom_info["fname"],
          file: "-",
          message_type: MessageChat.MessageType.TEXT
          // type: this.chat_type
        };
        let wait = await this.firebase.database
          .ref("chatroom")
          .child(this.chatroom_info["chatroom"])
          .push()
          .set(msg)
          .then(res => {
            this.Send_Push_Notification(msg);
            // this.scrollToBottom();
          })
          .then(() => {
            resolve();
          });
      }
    });

    // this.chatBox = "";
  }

  public Send_File(event) {
    return new Promise<any>(async resolve => {
      let file = event.target.files[0];
      if (file == undefined) {
        resolve();
      }
      let file_path =
        "files/" + this.chatroom_info["chatroom"] + "/" + file.name;
      await this.firestorage.storage.ref(file_path).put(file);
      let fileUrl = await this.firestorage.storage
        .ref(file_path)
        .getDownloadURL();
      let msg = MessageModel.prototype;
      msg = {
        message: fileUrl,
        sender_id: this.chatroom_info["user_id"],
        receiver_id: this.chatroom_info["contact_id"],
        image: this.chatroom_info["image"],
        timeStamp: Datetime.getTimeInMilliseconds(),
        read: MessageChat.Seen.NOT_READ,
        name: this.chatroom_info["fname"],
        file: file.name,
        message_type: MessageChat.MessageType.FILE
        // type: this.chat_type
      };

      this.firebase.database
        .ref("chatroom")
        .child(this.chatroom_info["chatroom"])
        .push()
        .set(msg)
        .then(
          res => {
            this.Send_Push_Notification(msg);
            this.controller.Show_Toast("สำเร็จ", "bottom");
            // this.alertBox("สำเร็จ");
            // this.scrollToBottom();
            resolve();
          },
          err => {
            this.controller.Show_Toast("ล้มเหลว", "bottom");
            // this.alertBox("ล้มเหลว");
            resolve();
          }
        );
    });
  }

  public Send_Image_Storage(event) {
    return new Promise<any>(resolve => {
      if (event == undefined) {
        resolve();
      }
      let img: any;
      let file_path: any;
      img = event.target.files[0];
      if (img["type"].substring(0, 5) != "image") {
        this.controller.Show_Toast("รูปแบบไฟล์ไม่ถูกต้อง", "bottom");
        resolve();
      }
      file_path =
        "image/" +
        this.chatroom_info["chatroom"] +
        "/" +
        Datetime.getTimeInMilliseconds() +
        "/" +
        img.name;
      this.Send_Image(file_path, img).then(() => {
        resolve();
      });
    });
  }

  public Send_Image_Camera(event) {
    return new Promise<any>(resolve => {
      if (event == undefined) {
        resolve();
      }
      let img: any;
      let file_path: any;
      img = event;
      file_path =
        "image/" +
        this.chatroom_info["chatroom"] +
        "/" +
        Datetime.getTimeInMilliseconds() +
        ".jpg";
      this.Send_Image(file_path, img).then(() => {
        resolve();
      });
    });
  }

  private Send_Image(file_path, img) {
    return new Promise<any>(async resolve => {
      await this.firestorage.storage.ref(file_path).put(img);
      let fileUrl = await this.firestorage.storage
        .ref(file_path)
        .getDownloadURL();
      let msg = MessageModel.prototype;
      msg = {
        message: fileUrl,
        sender_id: this.chatroom_info["user_id"],
        receiver_id: this.chatroom_info["contact_id"],
        image: this.chatroom_info["image"],
        timeStamp: Datetime.getTimeInMilliseconds(),
        read: MessageChat.Seen.NOT_READ,
        name: this.chatroom_info["fname"],
        file: "-",
        message_type: MessageChat.MessageType.IMAGE
        //type: this.chat_type
      };
      this.firebase.database
        .ref("chatroom")
        .child(this.chatroom_info["chatroom"])
        .push()
        .set(msg)
        .then(
          es => {
            this.Send_Push_Notification(msg);
            this.controller.Show_Toast("สำเร็จ", "bottom");
            // this.alertBox("สำเร็จ");
            // this.scrollToBottom();
            resolve();
          },
          err => {
            this.controller.Show_Toast("ล้มเหลว", "bottom");
            // this.alertBox("ล้มเหลว");
            resolve();
          }
        );
    });
  }

  private Send_Push_Notification(msg: MessageModel) {
    console.log(this.chatroom_info);
    let push_content: any;
    switch (msg["message_type"]) {
      case MessageChat.MessageType.TEXT:
        push_content = msg["message"];
        break;
      case MessageChat.MessageType.IMAGE:
        push_content = "Image";
        break;
      case MessageChat.MessageType.FILE:
        push_content = "Attach";
        break;
    }
    this.onesignalservice.send_chat_notification({
      msg: msg,
      content: push_content,
      onesignal_id: this.chatroom_info["contact_onesignal_id"],
      chatroom: this.chatroom_info,
      chat_type: this.chat_type
    });
  }

  public Call_Camera() {
    return new Promise<any>(resolve => {
      const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        targetHeight: 800,
        targetWidth: 600,
        sourceType: 1
      };
      this.camera.getPicture(options).then(
        async res => {
          let base64image = "data:image/jpeg;base64," + res;
          let img = ImageUtil.prototype.datauri_to_blob(base64image);
          let wait = await this.Send_Image_Camera(img).then(() => {
            resolve();
          });
        },
        err => {
          console.log(err);
        }
      );
    });
  }

  public Call_Browser(data) {
    window.open(data, "_system");
  }

  public Call_Image_Page(data) {
    this.photoviewer.show(data["message"], "", { share: false });
  }

  public Load_Order_Information() {
    return new Promise<any>(async (resolve, reject) => {
      let oid = await this.userservice.get_order_key();
      console.log(oid);
      if (oid != null) {
        this.firebase.database
          .ref("order_information")
          .orderByChild("order_id")
          .equalTo(oid)
          .once("value", snap => {
            if (snap.exists()) {
              let result = [];
              snap.forEach(value => {
                let product = value.val();
                result.push(product);
              });
              console.log(result);
              resolve(result);
            } else {
              resolve("");
            }
          });
      } else {
        resolve(null);
      }
    });
  }

  public async Get_PreOrder_Chat(chatroom) {
    return new Promise<any>(resolve => {
      this.firebase.database
        .ref("chat_preorder")
        .orderByChild("chatroom")
        .equalTo(chatroom)
        .once("value", snap => {
          if (snap.exists()) {
            let arr = [];
            snap.forEach(value => {
              arr.push(value.val());
            });
            resolve(arr);
          } else {
            resolve();
          }
        });
    });
  }

  public async Set_PreOrder_Chat(data, chatroom) {
    return new Promise<any>(resolve => {
      this.firebase.database
        .ref("chat_preorder")
        .orderByChild("chatroom")
        .equalTo(chatroom)
        .once("value", async snap => {
          if (snap.exists()) {
            let img = await this.Upload_PreOrder_Image(data["Product_Image"]);
            let key = Object.keys(snap.val())[0];
            data["Product_Image"] = String(img);
            this.firebase.database
              .ref("chat_preorder/" + key)
              .update({ preorder_info: data })
              .then(() => {
                resolve();
              }, error => {
                console.log(error);
              });
            
          } else {
            resolve();
          }
        });
    });
  }

  public async Update_PreOrder_Chat(data, chatroom) {
    return new Promise<any>(resolve => {
      this.firebase.database
        .ref("chat_preorder")
        .orderByChild("chatroom")
        .equalTo(chatroom)
        .once("value", snap => {
          if (snap.exists()) {
            let key = Object.keys(snap.val())[0];
            let update = data;
            this.firebase.database
              .ref("chat_preorder/" + key)
              .update(update)
              .then(() => {
                resolve();
              });
          } else {
            resolve();
          }
        });
    });
  }

  public Upload_PreOrder_Image(data) {
    return new Promise<string>(async resolve => {
      let uid = await this.userservice.get_user_id();
      let file_path =
        "preorder/" + uid + "/" + Datetime.getTimeInMilliseconds();
      if (data['type'] == undefined) {
        resolve(data)
      }
      else if (data == (null || undefined || "")) {
        resolve("");
      } else {
        await this.firestorage.ref(file_path).put(data);
        let url = await this.firestorage.storage
          .ref(file_path)
          .getDownloadURL();
        resolve(url);
      }
    });
  }

  public Get_PreOrder_Chat_OnEvent(chatroom) {
    
    this.Turn_Off_Firebase_Event(this.EVENT.PREORDER);
    this.fbe_preorder_change = this.firebase.database.ref("chat_preorder")
      .orderByChild("chatroom")
      .equalTo(chatroom);
    this.fbe_preorder_change.on("child_changed", snap => {
      if (snap.exists()) {
        this.zone.run(() => {
          let tmp = snap.child("additional_data").val();
          this.events.publish(this.EVENT.PREORDER, tmp);
        });
      
      } else {

      }
    });
    return this.Subscribe_Event();
  }

  public Get_Additional_Data (order_id) {
    return new Promise<any>(resolve => {
      this.firebase.database
        .ref("order_information")
        .orderByChild('order_id')
        .equalTo(order_id)
        .once("value", snap => {
          if (snap.exists()) {
            let tmp;
            snap.forEach(value => {
              tmp = value.val();
            });
            resolve(tmp);
          } else {
            resolve("");
          }
        });
      /*
      console.log('fu');
      this.firebase
        .database
        .ref("chat_guest")
        .orderByChild("chatroom")
        .equalTo(chatroom)
        .once("value", snap => {
          console.log(snap.exists());
          if (snap.exists()) {
            console.log(chatroom);
            snap.forEach((value) => {
              let oid = value.child("additional_data").val();
              console.log(oid);
              if (oid == undefined) {
                console.log(chatroom);
                resolve("");
              } else {  
                console.log(chatroom);
                this.firebase.database
                .ref("order_information")
                .orderByChild("order_id")
                .equalTo(oid)
                .once("value", snap => {
                  if (snap.exists()) {
                    let tmp;
                    snap.forEach(value => {
                      tmp = value.val();
                    });
                    resolve(tmp);
                  } else {
                    console.log(chatroom);
                    resolve("");
                  }
                });
              }
              
            });
          } else {
            resolve("");
          }
        });
        */
    });
  }

  public Load_Contact_List_Preorder_By_Category_Id (category_id) {
    this.Turn_Off_Firebase_Event(this.EVENT.CONTACT_LIST_CUSTOMER);
    this.fbe_contact_list_preorder = this.firebase.database.ref("category_type")
      .orderByChild("category_id")
      .equalTo(category_id);

    this.fbe_contact_list_preorder.on("value", async snap => {
      let contact_list = [];
      if (snap.exists()) {
        this.zone.run(async () => {
          snap.forEach(value => {
            value.child("manage").forEach(async (user) => {
              let user_data = await new Promise<any>(resolve => {
                this.firebase.database
                .ref("employee_information")
                .orderByChild("uid")
                .equalTo(user.child("uid").val())
                .once("value", snap => {
    
                  if (snap.exists()) {
                    let temp;
                    snap.forEach((value) => {
                      let tmp = value.val();
                      tmp['fullname'] = value.child("pname").val() + ' ' + value.child("fname").val() + ' ' + value.child("lname").val();
                      temp = tmp;
                    });
                    resolve(temp);
                  }
                });
              });
              console.log(user_data);
              contact_list.push(user_data);
            }) 
          });
          this.events.publish(this.EVENT.CONTACT_LIST_PREORDER, contact_list);
        });
      } else {

      }
    });

    return this.Subscribe_Event();
  }

  public Fetch_All_Contact () {
    return new Promise<any>(resolve => {
      this.firebase.database.ref("employee_information")
        .once("value", snap => {
          if (snap.exists()) {
            let tmp = [];
            snap.forEach (value => {
              tmp.push(value.child("uid").val());
            });
            this.Fetch_All_Contact_From_Preorder(tmp).then(res => {
              resolve(res);
            }); 
          } else {
            resolve("");
          }
        });
    });
  }

  private Fetch_All_Contact_From_Preorder (data) {
    return new Promise<any>(resolve => {
      this.firebase.database.ref("chat_preorder")
        .once("value", snap => {
          if (snap.exists()) {
            let tmp = [];
            snap.forEach(value => {
              tmp.push(value.child("chat2/uid").val());
            });
            let id = this.Find_Least_Chat(data, tmp);
            resolve(id[0]);
          } else {
            let id = this.Fisher_Yates_Algorithm(data);
            resolve(id[0]);
          }
        });
    });
  }

  private Find_Least_Chat (all_contact, all_chat) {
    let contact_index_arr = [];

    for (let i = 0; i < all_contact.length; i++) {
      contact_index_arr.push(0);
    }

    for (let i = 0; i < all_contact.length; i++) {
      for (let j = 0; j < all_chat.length; j++) {
        console.log(all_contact[i] == all_chat[j]);
        if (all_contact[i] == all_chat[j]) {
          contact_index_arr[i]++;
        }
      }
    }

    let min_value = Math.min(...contact_index_arr);

    console.log(contact_index_arr);
    console.log(min_value);

    let id = [];
    for (let i = 0; i < contact_index_arr.length; i++) {
      if (contact_index_arr[i] == min_value) {
        id.push(all_contact[i]);
      }
    }

    let result = this.Fisher_Yates_Algorithm(id);

    return result;
  }

  private Fisher_Yates_Algorithm (data) {
    var currIndex = data.length;
    var randIndex;
    var temp;

    while (currIndex !== 0) {
      randIndex = Math.floor(Math.random() * currIndex);
      currIndex -= 1;

      temp = data[currIndex];
      data[currIndex] = data[randIndex];
      data[randIndex] = temp;
    }

    return data;
  }
}
