import { ModalController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ModalProvider {

  constructor(
    public http: HttpClient,
    public modalCtrl: ModalController
  ) {
    console.log('Hello ModalProvider Provider');
  }

  Open_Modal (page, css, data) {
    this.modalCtrl
    .create(page, { data: data }, { cssClass: css })
    .present();
  }

}
