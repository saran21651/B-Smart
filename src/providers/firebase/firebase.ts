import { Cart_Model } from "./../../class/Model/Order/Cart";
import { Datetime } from "./../../class/Util/Datetime";
import {
  Order_Model,
  Order_list,
  Order_History_Model
} from "./../../class/Model/Order/Order";
import { UserServiceProvider } from "./../user-service/user-service";
import { Address_Model } from "./../../class/Model/Address/address";
import { Product_Model } from "./../../class/Model/Order/Product";
import { AngularFireDatabase } from "angularfire2/database";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Platform } from "ionic-angular/platform/platform";
import { AuthServiceProvider } from "../auth-service/auth-service";
import { AngularFireStorage } from "angularfire2/storage";

@Injectable()
export class FirebaseProvider {
  private uid;
  private cart_list = [];
  constructor(
    private http: HttpClient,
    private firebase: AngularFireDatabase,
    private userservice: UserServiceProvider,
    private platform: Platform,
    private authservice: AuthServiceProvider,
    private firestorage: AngularFireStorage
  ) {
    console.log("Hello FirebaseProvider Provider");
  }

  // Profile
  public async Update_Profile(data) {
  
    return new Promise<any>((resolve, reject) => {
      this.firebase.database
        .ref("user_information/")
        .orderByChild("auth/uid")
        .equalTo(data["uid"])
        .once("value", snap => {
          if (snap.exists()) {
            let key = Object.keys(snap.val())[0];
            let update_item = {
              fname: data["fname"],
              lname: data["lname"],
              pname: data["pname"],
              email: data["email"],
              image: data["image"],
              tel: data["tel"]
            };
            this.firebase.database
              .ref("user_information/" + key)
              .update(update_item)
              .then(() => {
                resolve();
              });
          } else {
            reject();
          }
        });
    });
  }

  // Order
  // Type 0
  public Cancel_Order (data) {
    return new Promise<any>(resolve => {
     
      let item = {
        order_status: 0,
        cancel_status: {
          user_cancel: true,
          store_cancel: data['cancel_status']['store_cancel']
        }
      };
      
      this.firebase.database
        .ref("order_information")
        .orderByChild("order_id")
        .equalTo(data["order_id"])
        .limitToFirst(1)
        .once("value", snap => {
          if (snap.exists()) {
            snap.forEach(value => {
              let key = value.key;
              this.firebase.database
                .ref("order_information/" + key)
                .update(item)
                .then(
                  () => {
                    resolve(true);
                  },
                  err => {
                    resolve(false);
                  }
                );
            });
          } else {
            resolve(false);
          }
        });
    });
  }
  // Type 1
  public async Order_Pre_Purchase(data) {
    let order_list_arr = this.convert_order_list_app_to_order_list_firebase(
      data
    );
    console.log(data);
    console.log(order_list_arr);
    let uid = await this.userservice.get_user_id();
    // let okey = await this.userservice.get_order_key();
    let okey = await this.Get_Order_Key_From_User_Information();
  
    if (okey != "") {
      this.firebase.database
        .ref("order_information/" + okey + "/order_list")
        .set(order_list_arr); 
    } else {
      let item = {
        destination: "",
        delivery_type: "",
        order_id: "",
        order_list: order_list_arr,
        order_status: 1,
        order_status_text: "",
        search: {
          order_status: ""
        },
        timestamp: "",
        uid: uid,
        user_information: await this.userservice.get_user_information(),
        cancel_status : {
          user_cancel: false,
          store_cancel: false
        }
      };

      this.firebase.database
        .ref("order_information")
        .push(item)
        .then(res => {
          // KEYYYYYYYYYYYYY
          // this.userservice.set_order_key(res["key"]);
          this.Set_Order_Key_To_User_Information(res["key"]);
          // this.wait_order_id_from_cloud_function(res["key"]);
        });
    }
  }

  // Type 2
  public async Order_Purchase(data) {
  
    let okey = await this.Get_Order_Key_From_User_Information();
    let item = {
      destination: data["address"],
      delivery_type: data["delivery_type"],
      payment: data["payment"],
      order_status: 2,
      price: data["price"]
      // sum_total: data["sum_total"]
    };
    if (okey != "") {
      this.firebase.database
        .ref("order_information/" + okey)
        .update(item)
        .then(() => {
          this.userservice.clear_order_id();
          this.userservice.clear_order_key();
          this.Clear_Order_Key_From_User_Information();
        });
    }
  }

  public Preorder_Purchase (data) {
    return new Promise<any>((resolve) => {
      let okey = data['push_key'];
      let item = {
        destination: data["address"],
        delivery_type: data["delivery_type"],
        payment: data["payment"],
        order_status: 2,
        price: data["price"]
      };

      this.firebase.database
        .ref("preorder_information/" + okey)
        .update(item)
        .then(() => {
          resolve();
        });
    });
  }

  // Type 3
  public Order_Payment_Inform(data) {
    return new Promise<any>(resolve => {
   
      let item = {
        order_status: 3
      };
      let item2 = {
        inform: {
          datetime_transfer: data["timestamp"],
          account: data["account"],
          evident_pic: data["evident_pic"]
        }
      };
      this.firebase.database
        .ref("order_information")
        .orderByChild("order_id")
        .equalTo(data["order_id"])
        .limitToFirst(1)
        .once("value", snap => {
          if (snap.exists()) {
          
            snap.forEach(value => {
              let key = value.key;
          
              this.firebase.database
                .ref("order_information/" + key + "/payment")
                .update(item2)
                .then(
                  () => {
                    this.firebase.database
                      .ref("order_information/" + key)
                      .update(item)
                      .then(
                        () => {
                          resolve(true);
                        },
                        err => {
                          resolve(false);
                        }
                      );
                  },
                  err => {
                    resolve(false);
                  }
                );
            });
          } else {
            resolve(false);
          }
        });
    });
  }
  // Type 5
  public Order_Wait(data) {
    return new Promise<any>(resolve => {
     
      let item = {
        order_status: 6, // 5
        comment: {
          star: data["star"],
          comment: data["comment"]
        }
      };
      this.firebase.database
        .ref("order_information")
        .orderByChild("order_id")
        .equalTo(data["order_id"])
        .limitToFirst(1)
        .once("value", snap => {
          if (snap.exists()) {
            snap.forEach(value => {
              let key = value.key;
            
              this.firebase.database
                .ref("order_information/" + key)
                .update(item)
                .then(
                  () => {
                    resolve(true);
                  },
                  err => {
                    resolve(false);
                  }
                );
            });
          } else {
            resolve(false);
          }
        });
    });
  }

  public async Order_Get_List() {
    // remove oid
    this.userservice.clear_order_id();
    this.userservice.clear_order_key();
    let uid = await this.userservice.get_user_id();
    return new Promise<any>(resolve => {
      this.firebase.database
        .ref("order_information")
        .orderByChild("uid")
        .equalTo(uid)
        .limitToLast(1)
        .once("value", async snap => {
          if (snap.exists()) {
            snap.forEach(value => {
              let product_info = [];
              if (Number(value.child("order_status").val()) == 1) {
                let okey = Object.keys(snap.val())[0];
                let oid = value.child("order_id").val();
                this.userservice.set_order_id(oid);
                this.userservice.set_order_key(okey);
                this.cart_list.splice(0, this.cart_list.length);
                let item = value.child("order_list").val();
          
                if (item != null) {
                  for (let i = 0; i < item.length; i++) {
            
                    item[i]['options'] = item[i]['options'] || [];
                    product_info.push({
                      product: item[i]
                    });
                  }
                }
              }
              resolve(product_info);
            });
          } else {
            resolve();
          }
        });
    });
  }

  public async PreOrder_Get_List () {
    this.userservice.clear_preorder_id();
    this.userservice.clear_preorder_key();
    let uid = await this.userservice.get_user_id();
    return new Promise<any>(resolve => {
      this.firebase.database
        .ref("preorder_information")
        .orderByChild("user_information/uid")
        .equalTo(uid)
        .once("value", snap => {
          if (snap.exists()) {
            let product_info = [];
            snap.forEach(value => {
              
              if (Number(value.child("order_status").val()) == 1) {
                let okey = Object.keys(snap.val())[0];
                let oid = value.child("order_id").val();
                this.userservice.set_preorder_id(oid);
                this.userservice.set_preorder_key(okey);
                // this.cart_list.
                let item = value.val();
    
                if (item != null) {
                  product_info.push({
                    product: item
                  });
                }
              }
            });
            resolve(product_info);
          } else {
            resolve();
          }
        });
        
    });
  }

  public Find_product_and_add_to_cart(product_id, amount, product_info?) {
    // Fetch order and add to cart when start app
    return new Promise<any>(resolve => {
      this.firebase.database
        .ref("products_information")
        .orderByChild("product_id")
        .equalTo(product_id)
        .once("value", snap => {
          if (snap.exists()) {
            let result = [];
            snap.forEach(value => {
              let product = value.val();
              if (product_info != null) {
                product["product_price"] = product_info;
              }
              result.push({
                product: product,
                quantity: amount
              });
            });
            resolve(result);
          } else {
            resolve();
          }
        });
    });
  }
  
  public Get_cart_list() {
    return this.cart_list;
  }

  private convert_order_list_app_to_order_list_firebase(order_data) {
    let order_list: Array<Order_list> = [];
    order_data.forEach(data => {
      // let order: Order_list = new Order_list();
      let order: any;
      order = {
        amount: data["quantity"] || data["product"]["amount"],
        category_id: data["product"]["category_id"],
        design_pattern: data["product"]["design_pattern"],
        options: data["product"]["options"] || [],
        product_id: data["product"]["product_id"],
        // product_code: data["product"]["product_code"],
        product_description: data["product"]["product_description"],
        product_image: data["product"]["product_image"],
        product_name: data["product"]["product_name"],
        product_type: data["product"]["product_type"],
        product_unit: data["product"]["product_unit"],
        price: data["product"]["price"]
      };
      order_list.push(order);
    });
    return order_list;
  }

  private wait_order_id_from_cloud_function(key) {
    let fire_event = this.firebase.database.ref(
      "order_information/" + key + "/order_id"
    );

    fire_event.on("child_changed", snap => {
      this.userservice.set_order_id(snap.child("order_id").val());
      let oid = snap.child("order_id").val();
      this.userservice.get_user_id().then(res => {
        this.firebase.database
          .ref("user_information")
          .orderByChild("auth/uid")
          .equalTo(res)
          .once("value", snap => {
            if (snap.exists()) {
              let key = Object.keys(snap.val())[0];
              this.firebase.database
                .ref("user_information/" + key + "/")
                .update({ order_id: oid });
            }
          });
      });
      fire_event.off();
    });
  }

  private async Set_Order_Key_To_User_Information(okey) {
    let uid = await this.userservice.get_user_id();
    this.firebase.database
      .ref("user_information")
      .orderByChild("auth/uid")
      .equalTo(uid)
      .once("value", snap => {
 
        if (snap.exists()) {
          let key = Object.keys(snap.val())[0];
          this.firebase.database
            .ref("user_information/" + key + "/")
            .update({ order_id: okey });
        }
      });
  }

  private async Set_Order_Key_To_User_Information_Preorder (okey) {
    let uid = await this.userservice.get_user_id();
    this.firebase.database
      .ref("user_information")
      .orderByChild("auth/uid")
      .equalTo(uid)
      .once("value", snap => {
        if (snap.exists()) {
          let key = Object.keys(snap.val())[0];
          this.firebase.database
            .ref("user_information/" + key + "/preorder_id")
            .push({order_id: okey});

        }
      }); 
  }

  private async Clear_Order_Key_From_User_Information() {
    let uid = await this.userservice.get_user_id();
    this.firebase.database
      .ref("user_information")
      .orderByChild("auth/uid")
      .equalTo(uid)
      .once("value", snap => {
        if (snap.exists()) {
          let key = Object.keys(snap.val())[0];
          this.firebase.database
            .ref("user_information/" + key + "/" + "order_id")
            .remove();
        }
      });
  }

  private async Clear_Order_Key_From_User_Information_Preorder () {
    let uid = await this.userservice.get_user_id();
    this.firebase.database
      .ref("user_information")
      .orderByChild("auth/uid")
      .equalTo(uid)
      .once("value", snap => {
        if (snap.exists()) {
          let key = Object.keys(snap.val())[0];
          this.firebase.database
            .ref("user_information/" + key + "/" + "preorder_id")
            .remove();
        }
      });
  }

  private Get_Order_Key_From_User_Information() {
    return new Promise<any>(async resolve => {
      let uid = await this.userservice.get_user_id();
      this.firebase.database
        .ref("user_information")
        .orderByChild("auth/uid")
        .equalTo(uid)
        .once("value", snap => {
          if (snap.exists()) {
            snap.forEach(child => {
          
              if (child.child("order_id").val() != undefined) {
                resolve(child.child("order_id").val());
              } else {
                resolve("");
              }
            });
          } else {
            resolve("");
          }
        });
    });
  }

  private Get_Order_Key_From_User_Information_PreOrder (uid) {
    return new Promise<any>(async resolve => {
      this.firebase.database
        .ref("user_information")
        .orderByChild("auth/uid")
        .equalTo(uid)
        .once("value", snap => {
          if (snap.exists()) {
            snap.forEach(child => {
              if (child.child("preorder_id").val() != undefined) {
                resolve(child.child("preorder_id").val());
              } else {
                resolve("");
              }
            })
          } else {
            resolve("");
          }
        });
    });
  }

  // Product
  public Get_Categories() {
    return new Promise<any>(resolve => {
      this.firebase.database.ref("category_type").once("value", snap => {
        if (snap.exists()) {
          let arr = [];
          snap.forEach(value => {
            let item = value.val();
            arr.push(item);
          });
          resolve(arr);
        } else {
          resolve([]);
        }
      });
    });
  }

  public Get_Order_Instock_By_Category(category) {
    return new Promise<any>(resolve => {
      this.firebase.database
        .ref("products_information")
        .orderByChild("category_id")
        .equalTo(category)
        .once("value", snap => {
          if (snap.exists()) {
            let arr = [];
            snap.forEach(value => {
              let item = value.val();
              arr.push(item);
            });
            resolve(arr);
          } else {
            resolve([]);
          }
        });
    });
  }

  public Get_Order_Instock(number) {
    return new Promise<any>((resolve, reject) => {
      this.firebase.database
        .ref("products_information")
        .limitToFirst(number)
        .once("value", snap => {
          if (snap.exists()) {
            let arr = [];
         
            snap.forEach(value => {
              
              let item: Product_Model = new Product_Model();
              item["category_id"] = value.child("category_id").val();
              item["options"] = value.child("options").val();
              item["product_description"] = value
                .child("product_description")
                .val();
              item["product_id"] = value.child("product_id").val();
              item["product_image"] = value.child("product_image").val();
              item["product_name"] = value.child("product_name").val();
              item["product_price"] = value.child("product_price").val();
              item["product_stock"] = value.child("product_stock").val();
              item["product  _unit"] = value.child("product_unit").val();
              item["product_type"] = value.child("product_type").val();
              item["timestamp"] = value.child("timestamp").val();
            
              arr.push(item);
            });

            resolve(arr.reverse());
          } else {
            resolve("");
          }
        });
    });
  }

  public Check_Order_Load_Remain (old_number, new_number) : Boolean {
    if (old_number == new_number) {
      return false;
    } else {
      return true;
    }
  }

  // Test 
  public Get_Order_Instock_Revamp (old_number, new_number, category?) {
    return new Promise<any>((resolve, reject) => {
      let firebase;

      if (category == null) {
        firebase = this.firebase.database.ref("products_information").limitToLast(new_number);
      } else {
        firebase = this.firebase.database.ref("products_information").orderByChild("category_id").equalTo(category).limitToLast(new_number);
      }

      firebase.once("value", snap => {
          if (snap.exists()) {
            let arr = [];
    
            snap.forEach(value => {
              if (String(value.child("product_type").val()) == "1") {
                let item = value.val();
                arr.push(item);
              } 
            });
     
            arr = arr.reverse();
            
            if (old_number != new_number) {
              for (let i = 0; i < new_number; i++) {
                if ((i < old_number)) {
                  arr.splice(arr[i], 1);
                } 
              }
            }
  
            resolve(arr);
          } else {
            resolve("");
          }
        });
    });
  }

  public Get_Order_Preorder_Revamp (old_number, new_number, category?) {
    return new Promise<any>((resolve, reject) => {
      let firebase;

      if (category == null) {
        firebase = this.firebase.database.ref("products_information").limitToLast(new_number);
      } else {
        firebase = this.firebase.database.ref("products_information").orderByChild("category_id").equalTo(category).limitToLast(new_number);
      }

      firebase.once("value", snap => {
          if (snap.exists()) {
            let arr = [];
       
            snap.forEach(value => {
              if (String(value.child("product_type").val()) == "2") {
                let item = value.val();
                arr.push(item);
              } 
            });
     
            arr = arr.reverse();
            
            if (old_number != new_number) {
              for (let i = 0; i < new_number; i++) {
                if ((i < old_number)) {
                  arr.splice(arr[i], 1);
                } 
              }
            }
  
            resolve(arr);
          } else {
            resolve("");
          }
        });
    });
  }

  public Get_Order_Instock_HomePage() {
    return new Promise<any>((resolve, reject) => {
      this.firebase.database
        .ref("products_information")
        // .limitToLast(12)
        .once("value", snap => {
          if (snap.exists()) {
            let arr = [];
            snap.forEach(value => {

              if (String(value.child("product_type").val()) == "1") {
                let item =value.val();
                arr.push(item);
              }
              
            });
            resolve(arr.reverse());
          } else {
            resolve("");
          }
        });
    });
  }

  public Get_Preorder () {
    return new Promise<any>((resolve) => {

      this.firebase.database.ref("products_information")
        .once("value", snap => {
          if (snap.exists()) {
            let arr = [];
            snap.forEach(value => {

              if (String(value.child("product_type").val()) == "2") {
                let item = value.val();
                arr.push(item);
              } 
            });

            resolve(arr);
          } else {
            resolve("");
          }
        });
    });
  }

  public Get_Category_Options (category_option) {
    return new Promise<any>((resolve, reject) => {
      this.firebase.database
        .ref("category_option")
        .orderByChild("category_id")
        .equalTo(category_option)
        .once("value", snap => {
          if (snap.exists()) {
            let arr = [];
            snap.forEach(value => {
              let item = value.val();
              arr.push(item);
            });
            resolve(arr);
          } else {
            resolve([]);
          }
        });
    });
  }

  public Get_Options_and_Categorie (category_option, option_id) {
    return new Promise<any>((resolve, reject) => {
      this.firebase.database
        .ref("category_option")
        .orderByChild("category_id")
        .equalTo(category_option)
        .once("value", snap => {
          if (snap.exists()) {
            let arr = [];
            snap.forEach(value => {
              let item = value.val();
              if (value.child("option_id").val() == option_id) {
                arr.push(item);
              } 
              
            });
            resolve(arr);
          } else {
            resolve([]);
          }
        });
    });
  }

  public Get_Order_Instock_By_Option_Id (option) {
    return new Promise<any>((resolve, reject) => {
      this.firebase.database
        .ref("products_information")
        .orderByChild("option_id")
        .equalTo(option)
        .once("value", snap => {
          if (snap.exists()) {
            let arr = [];
            snap.forEach(value => {
              let item = value.val();
              arr.push(item);
            });
            resolve(arr);
          } else {
            resolve([])
          }
        });
    });
  }

  public Get_Option_By_Option_ID (option) {
    return new Promise<any>((resolve, reject) => {
      this.firebase.database
        .ref("option_filter")
        .orderByChild("option_filter_id")
        .equalTo(option)
        .once("value", snap => {
          if (snap.exists()) {
            let arr = [];
            snap.forEach(value => {
              let item = value.val();
              arr.push(item);
            });
            resolve(arr);
          } else {
            resolve([])
          }
        });
    });
  }

  // Address
  public async Get_Address_Book() {
    this.uid = await this.userservice.get_user_id();
    return new Promise<any>((resolve, reject) => {
      this.firebase.database
        .ref("user_information")
        .orderByChild("auth/uid")
        .equalTo(this.uid)
        .once("value", snap => {
          if (snap.exists()) {
            snap.forEach(value => {
              let item = value.child("address").val();
              let arr = [];
              if (item != undefined) {
                Object.keys(item).forEach(element => {
                  item[element]["key"] = element;
                  arr.push(item[element]);
                });
              }
              resolve(arr);
            });
          } else {
            reject();
          }
        });
    });
  }

  public async Add_Address_Book(data: Address_Model) {
    this.uid = await this.userservice.get_user_id();
    return new Promise<any>((resolve, reject) => {
      this.firebase.database
        .ref("user_information")
        .orderByChild("auth/uid")
        .equalTo(this.uid)
        .once("value", snap => {
          if (snap.exists()) {
            let key = Object.keys(snap.val())[0];
            this.firebase.database
              .ref("user_information/" + key + "/address")
              .push()
              .set(data)
              .then(() => {
                resolve();
              });
          } else {
            reject();
          }
        });
    });
  }

  public async Edit_Address_Book(data: Address_Model) {
    this.uid = await this.userservice.get_user_id();
    return new Promise<any>((resolve, reject) => {
      this.firebase.database
        .ref("user_information")
        .orderByChild("auth/uid")
        .equalTo(this.uid)
        .once("value", snap => {
          if (snap.exists()) {
            let key = Object.keys(snap.val())[0];
            let address_key = data["key"];

            delete data["key"];
            this.firebase.database
              .ref("user_information/" + key + "/address/" + address_key)
              .update(data)
              .then(() => {
                resolve();
              });
          } else {
            reject();
          }
        });
    });
  }

  public async Delete_Address_Book(address_key) {
    this.uid = await this.userservice.get_user_id();
    return new Promise<any>((resolve, reject) => {
      this.firebase.database
        .ref("user_information")
        .orderByChild("auth/uid")
        .equalTo(this.uid)
        .once("value", snap => {
          if (snap.exists()) {
            let key = Object.keys(snap.val())[0];
            this.firebase.database
              .ref("user_information/" + key + "/address/" + address_key)
              .remove()
              .then(() => {
                resolve();
              });
          } else {
            reject();
          }
        });
    });
  }

  // Order History
  public async Get_Order_History() {
    let uid = await this.userservice.get_user_id();
    return new Promise<any>((resolve, reject) => {
      this.firebase.database
        .ref("order_information")
        .orderByChild("uid")
        .equalTo(uid)
        .once("value", snap => {
          if (snap.exists()) {

            let item = [];
            snap.forEach(value => {
              if (String(value.child("order_status").val()) != "1") {
                let temp: Order_History_Model = value.val();
                temp["timestamp"] = this.Convert_Timestamp(temp["timestamp"]);
                item.push(temp);
              }
            });
            resolve(item);
          } else {
            resolve();
          }
        });
    });
  }

  public async Get_Preorder_History () {
    let uid = await this.userservice.get_user_id();
    return new Promise<any>((resolve, reject) => {
      this.firebase.database
        .ref("preorder_information")
        .orderByChild("user_information/uid")
        .equalTo(uid)
        .once("value", snap => {
          if (snap.exists()) {
            let item = [];
            snap.forEach(value => {
              if (String(value.child("order_status").val()) != "0") {
                let temp = value.val();
                temp["timestamp"] = this.Convert_Timestamp(temp["timestamp"]);
                item.push(temp);
              }
            });
            resolve(item);
          } else {
            resolve();
          }
        });
    });
  }

  // Order Tracking
  public async Get_Order_Tracking() {
    let uid = await this.userservice.get_user_id();
    return new Promise<any>((resolve, reject) => {
      this.firebase.database
        .ref("order_information")
        .orderByChild("uid")
        .equalTo(uid)
        .once("value", snap => {
          if (snap.exists()) {
            let item = [];
            snap.forEach(value => {
              if (
                Number(value.child("order_status").val()) != 0 &&
                Number(value.child("order_status").val()) != 1 &&
                Number(value.child("order_status").val()) != 6
              ) {
                let temp: Order_History_Model = value.val();
                temp["timestamp"] = this.Convert_Timestamp(temp["timestamp"]);
                item.push(temp);
              }
            });
            resolve(item);
          } else {
            resolve();
          }
        });
    });
  }

  public async Get_PreOrder_Tracking () {
    let uid = await this.userservice.get_user_id();
    return new Promise<any>((resolve) => {
      this.firebase.database
        .ref("preorder_information")
        .orderByChild("user_information/uid")
        .equalTo(uid)
        .once("value", snap => {
       
          let item = [];
          if (snap.exists()) {
            snap.forEach(value => {
              if (
                Number(value.child("order_status").val()) != 0 &&
                Number(value.child("order_status").val()) != 1 &&
                Number(value.child("order_status").val()) != 6
              ) {
                let tmp = value.val();
                tmp["timestamp"] = this.Convert_Timestamp(tmp["timestamp"]);
                item.push(tmp);
              }
            });
            resolve(item);
          } else {
            resolve();
          }
        });
    });
  }

  // Shipping Charge
  public Get_Shipping_Charge () {
    return new Promise<any>((resolve) => {
      this.firebase.database
        .ref('shipping_charge')
        .once('value', (snap) => {
          if (snap.exists()) {
            let arr = [];
            snap.forEach(value => {
              arr.push(value.val());
            });
            resolve(arr);
          } else {
            resolve([]);
          }
        });
    });
  }

  // Bank
  public Get_Bank () {
    return new Promise<any>((resolve) => {
      this.firebase.database
        .ref('bank')
        .once('value', (snap) => {
          if (snap.exists()) {
            let arr = [];
            snap.forEach(value => {
              arr.push(value.val());
            });
            resolve(arr);
          } else {
            resolve([]);
          }
        });
    });
  }

  // Promotion and News
  public Get_News_Or_Promotion(flag: number) {
    let choice = "";
    if (flag == 0) {
      choice = "news";
    } else {
      choice = "promotion";
    }
    return new Promise<any>(resolve => {
      this.firebase.database
        .ref("alert")
        .orderByChild("type")
        .equalTo(choice)
        .once("value", snap => {
          if (snap.exists()) {
            let arr = [];
            snap.forEach(value => {
              arr.push(value.val());
            });
            resolve(arr);
          } else {
            resolve([]);
          }
        });
    });
  }

  public Get_Promotion_HomePage() {
    return new Promise<any>(resolve => {
      this.firebase.database
        .ref("alert")
        .orderByChild("type")
        .equalTo("promotion")
        .limitToFirst(1)
        .once("value", snap => {
      
          if (snap.exists()) {
            let arr = [];
            snap.forEach(value => {
              arr.push(value.val());
            });
            resolve(arr);
          } else {
            resolve([]);
          }
        });
    });
  }

  public Get_Contact_Info_Staff (id) {
    return new Promise<any>((resolve) => {
      this.firebase.database.ref("employee_information")
        .orderByChild("uid")
        .equalTo(id)
        .once('value', (snap) => {
          if (snap.exists()) {
            snap.forEach(value => {
              resolve(value.val())
            });
          } else {
            resolve();
          }
        });
    });
  }

  public Get_Version () {
    /*
      return 
      {
        android: .. 
        ios: ..
      }
    */
    return new Promise<any>((resolve) => {
      this.firebase.database.ref("config/version/customer")
        .once('value', (snap) => {
          if (snap.exists()) {
  
            this.authservice.get_app_metadata().then((data) => {
              let version = "";
              if (this.platform.is('ios')) {
                version = snap.child('ios').val();
              }
              if (this.platform.is('android')) {
                version = snap.child('android').val();
              }
              if (version == data["version_number"] || data['version_number'] == "test" || version == "0.0.0") {
                resolve(false);
              } else {
                resolve(true);
              }
            });
          } else {
            resolve(false);
          }
        });
    });
  }

  public Get_PreOrder_Data () { 
    return new Promise<any>(async (resolve) => {
      let uid = await this.userservice.get_user_id();
      this.firebase.database
        .ref("preorder_information")
        .orderByChild("uid")
        .equalTo(uid)
        .once("value", snap => {
          if (snap.exists()) {
            let arr = [];
            snap.forEach((value) => {
              let tmp = value.val();
              arr.push(tmp);
            });
            resolve(arr);
          } else {
            resolve([]);
          }
        });
    });
  }

  public Set_PreOrder_Data (data, chatroom) {
   
    return new Promise<any>(async (resolve) => {

      data['user_information'] = await this.userservice.get_user_information();
      data['employee_information'] = await this.Get_Employee_Information(data['employee_information']);
      data['timestamp'] = {
        order_time: Datetime.getTimeInMilliseconds()
      }
      let uid = await this.userservice.get_user_id();
      let okey = await this.Get_Order_Key_From_User_Information_PreOrder(uid);

      this.firebase.database
        .ref("preorder_information")
        .push(data)
        .then((res) => {
          const push_key = res.key;
          // this.Set_Order_Key_To_User_Information_Preorder(res["key"]);
          this.firebase.database
            .ref('chat_preorder')
            .orderByChild('chatroom')
            .equalTo(chatroom)
            .once('value', snap => {
              if (snap.exists()) {
                let key = Object.keys(snap.val())[0];
                this.firebase.database.ref('chat_preorder/' + key).update({
                  additional_data: {
                    Amount: '',
                    Customer_ID: '',
                    Employee_ID: '',
                    Product_Category: '',
                    Product_Description: '',
                    Product_Image: '',
                    Product_Name: '',
                    Product_Price: ''
                  }
                }).then(() => {
                  resolve(push_key);
                }).catch(() => {
                  resolve(push_key);
                });
              } else {
                resolve(push_key);
              }
            });
          // resolve();
        });
    });
  }

  public Remove_Preorder (index) {
    this.userservice.get_user_id().then(id => {
      this.firebase.database
      .ref("preorder_information")
      .orderByChild("user_information/uid")
      .equalTo(id)
      .once("value", (snap) => {
        if (snap.exists()) {
          let i = 0;
          snap.forEach(value => {
         
            if (i == index) {
              this.firebase.database.ref("preorder_information/" + value.key).remove();
            }
            i++;
          });
        }
      });
    });
  }

  public Get_Product_By_Order_Id (product_id) {
    return new Promise<any>(resolve => {
      this.firebase.database
        .ref("products_information")
        .orderByChild("product_id")
        .equalTo(product_id)
        .limitToFirst(1)
        .once("value", snap => {
      
          if (snap.exists()) {
            snap.forEach(value => {
              console.log(value);
              resolve(value.val());
            });
          } else {
            resolve();
          }
        });
    });
  }

  public Remove_Preorder_From_Tracking (data) {
    return new Promise<any>(resolve => {

      let item = {
        order_status: 0,
      };
      this.firebase.database
        .ref("preorder_information")
        .orderByChild("order_id")
        .equalTo(data["order_id"])
        .limitToFirst(1)
        .once("value", snap => {
          if (snap.exists()) {
            snap.forEach(value => {
              let key = value.key;
              this.firebase.database
                .ref("order_information/" + key)
                .update(item)
                .then(
                  () => {
                    resolve(true);
                  },
                  err => {
                    resolve(false);
                  }
                );
            });
          } else {
            resolve(false);
          }
        });
    });
  }

  public Cancel_PreOrder (data) {
    return new Promise<any>(resolve => {

      let item = {
        order_status: 0,
        cancel_status: {
          user_cancel: true,
          store_cancel: data['cancel_status']['store_cancel']
        }
      };
      
      this.firebase.database
        .ref("preorder_information")
        .orderByChild("order_id")
        .equalTo(data["order_id"])
        .limitToFirst(1)
        .once("value", snap => {
          if (snap.exists()) {
            snap.forEach(value => {
              let key = value.key;
              this.firebase.database
                .ref("preorder_information/" + key)
                .update(item)
                .then(
                  () => {
                    resolve(true);
                  },
                  err => {
                    resolve(false);
                  }
                );
            });
          } else {
            resolve(false);
          }
        });
    });
  }

  public Get_Employee_Information (uid) {

    return new Promise<any>((resolve) => {
      this.firebase.database
        .ref('employee_information')
        .orderByChild('uid')
        .equalTo(uid)
        .once('value', snap => {
          if (snap.exists()) {
            let tmp;
            snap.forEach(value => {
              tmp = {
                uid: uid,
                email: value.child('email').val(),
                image: value.child('image').val(),
                fname: value.child('fname').val(),
                lname: value.child('lname').val(),
                pname: value.child('pname').val()
              }
            });
         
            resolve(tmp);
          } else {
            resolve("");
          }
        });
    });
  }

  public Find_Preorder_By_Key (order_key) {
    return new Promise<any>((resolve) => {
      this.firebase.database
        .ref("preorder_information/" + order_key)
        .once("value", snap => {
       
          if (snap.exists()) {
            resolve(snap.val());
          } else {
            resolve("");
          }
        });
    });
  }

  private Calculate_Sum_Total(list) {
    let sum = 0;
  
    list.forEach(element => {
      sum = sum + Number(element["product_price"]["price"]);
    });
    return sum;
  }

  private Convert_Timestamp(item) {
    let time = {
      confirm_order_time: "",
      delivered_time: "",
      delivering_time: "",
      order_time: "",
      purchase_time: ""
    };
    if (item["confirm_order_time"] != undefined) {
      time.confirm_order_time = Datetime.getDate_Order_History(
        Number(item["confirm_order_time"])
      );
    }
    if (item["delivered_time"] != undefined) {
      time.delivered_time = Datetime.getDate_Order_History(
        Number(item["delivered_time"])
      );
    }
    if (item["delivering_time"] != undefined) {
      time.delivering_time = Datetime.getDate_Order_History(
        Number(item["delivering_time"])
      );
    }
    if (item["order_time"] != undefined) {
      time.order_time = Datetime.getDate_Order_History(
        Number(item["order_time"])
      );
    }
    if (item["purchase_time"] != undefined) {
      time.purchase_time = Datetime.getDate_Order_History(
        Number(item["purchase_time"])
      );
    }
    return time;
  }

  IsArray(data): boolean {
    return Object.prototype.toString.call(data) === "[object Array]";
  }
}
