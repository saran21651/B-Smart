import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireStorage } from "angularfire2/storage";
import { AngularFireAuth } from "angularfire2/auth";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { UserInformationModel } from "../../class/Model/UserData/UserInformation";
import { UserServiceProvider } from "../user-service/user-service";
import * as firebaseauth from "firebase/app";
import { OnesignalProvider } from "../onesignal/onesignal";
import { AppVersion } from "@ionic-native/app-version";
import { isCordovaAvailable } from "../../class/Util/CordovaCheck";
// import { Facebook } from "@ionic-native/facebook";

@Injectable()
export class AuthServiceProvider {
  constructor(
    private http: HttpClient,
    private fireauth: AngularFireAuth,
    private firestore: AngularFireStorage,
    private firebase: AngularFireDatabase,
    // private facebook: Facebook,
    private userservice: UserServiceProvider,
    private onesignalservice: OnesignalProvider,
    private app: AppVersion
  ) {
    console.log("Hello AuthServiceProvider Provider");
  }

  login(email, password) {
    return new Promise<any>((resolve, reject) => {
      this.fireauth.auth.signInWithEmailAndPassword(email, password).then(
        res => {
          this.firebase.database
            .ref("user_information")
            .orderByChild("auth/uid")
            .equalTo(this.fireauth.auth.currentUser.uid)
            .once("value")
            .then(
              async (snap) => {
                if (snap.exists()) {
                  snap.forEach((child) => {
                    let item = UserInformationModel.prototype;
                    item = {
                      pname: child.child("pname").val(),
                      fname: child.child("fname").val(),
                      lname: child.child("lname").val(),
                      email: child.child("email").val(),
                      join_date: child.child("join_date").val(),
                      uid: this.fireauth.auth.currentUser.uid,
                      image: child.child("image").val(),
                      online: "online",
                      line: child.child("line").val(),
                      tel: child.child("tel").val()
                    };
                    this.onesignalservice.set_player_id_to_user(child.key).then(async () => {
                      await this.onesignalservice.subscribe_chat_notification();
                      await this.onesignalservice.subscribe_news_and_promotion_notification();
                      await this.userservice.set_user_information(item).then(
                        res => {
                          resolve(res);
                        },
                        err => {
                          reject(err);
                        }
                      );
                    });
                    /*
                    this.onesignalservice.subscribe_chat_notification();
                    this.onesignalservice.subscribe_news_and_promotion_notification();
                    this.userservice.set_user_information(item).then(
                      res => {
                        resolve(res);
                      },
                      err => {
                        reject(err);
                      }
                    );
                    */
                    return false;
                  });
                } else {
                  reject({message: "This user does not exist!"});
                }
              },
              err => {
                reject();
              }
            );
        },
        err => {
          reject(err);
        }
      );
    });
  }

  logout() {
    return new Promise<any>((resolve, reject) => {
      if (this.fireauth.auth.currentUser) {
        this.fireauth.auth.signOut();
        this.userservice.clear_user_information();
        this.onesignalservice.unsubscribe_chat_notification();
        this.onesignalservice.unsubscribe_news_and_promotion_notification();
        resolve();
      } else {
        reject();
      }
    });
  }

  register(email, password) {
    return new Promise<any>((resolve, reject) => {
      firebaseauth
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(
          res => {
            if (res["code"] == "auth/email-already-in-use") {
              console.log(res["message"]);
            }

            let item = {
              customer_id: Math.floor(Math.random() * 100000) + 10000,
              email: res["user"]["email"],
              pname: "",
              fname: "",
              lname: "",
              auth: {
                uid: res["user"]["uid"]
              },
              image: "",
              online: "online",
              join_date: res["user"]["metadata"]["a"],
              line: ""
            };
            this.firebase.database.ref("user_information").push(item);
            resolve(res);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  check_firsttime_login(uid) {
    return new Promise<any>(resolve => {
      this.firebase.database
        .ref("user_information")
        .orderByChild("auth/uid")
        .equalTo(uid)
        .once("value", snap => {
          if (snap.exists()) {
            snap.forEach(value => {
              let fname = value.child("fname").val();
              let lname = value.child("lname").val();
              let pname = value.child("pname").val();
              if (fname == "" && lname == "" && pname == "") {
                resolve(true);
              } else {
                resolve(false);
              }
            });
          } else {
            resolve("");
          }
        });
    });
  }

  facebook_login() {
    return new Promise<any>(resolve => {
      /*
      this.facebook
        .login(["public_profile", "user_photos", "email"])
        .then(res => {
          if (res.status == "connected") {
            // GET user ID and Token
            var fb_id = res.authResponse.userID;
            var fb_token = res.authResponse.accessToken;

            // Get user infos from the API
            this.facebook.api("/me?fields=name,gender,email", []).then(user => {
              var name = user.name;
              var email = user.email;
              resolve(true);
            });
          } else {
            resolve(false);
          }
        });
        */
    });
  }

  public get_app_metadata() {
    return new Promise<any>(async resolve => {
      if (isCordovaAvailable()) {
        const appName = await this.app.getAppName();
        const packageName = await this.app.getPackageName();
        const versionNumber = await this.app.getVersionNumber();
        const versionCode = await this.app.getVersionCode();

        let item = {
          app_name: appName,
          package_name: packageName,
          version_number: versionNumber,
          version_code: versionCode
        };
        resolve(item);
      } else {
        let item = {
          app_name: "appName",
          package_name: "packageName",
          version_number: "test",
          version_code: "versionCode"
        };
        resolve(item);
      }
    });
  }
}
