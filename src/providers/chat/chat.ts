import { UserServiceProvider } from './../user-service/user-service';
import { Datetime } from './../../class/Util/Datetime';
import { Chat_metadataModel } from './../../class/Model/Chat/Chat_metadataModel';
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "angularfire2/database";

@Injectable()
export class ChatProvider {
  constructor(
    private http: HttpClient,
    private firebase: AngularFireDatabase,
    private userservice: UserServiceProvider
  ) {
    console.log("Hello ChatProvider Provider");
  }

  /*

  public Create_Chatroom(data) {
    return new Promise<any>(async (resolve, reject) => {
      let get_user_data = await this.userservice.get_user_information();

      let user1 = get_user_data["uid"];
      let user2 = data["uid"];
      let check = await this.checkChatroomExist(user1, user2);
      let chatroom = "";

      if (check.split("/")[0] === "true") {
        chatroom = check.split("/")[1] === "1" ? user1 + user2 : user2 + user1;
        let item = {
          data: {
            chatroom: chatroom,
            user_id: get_user_data["uid"],
            contact_id: data["uid"]
          }
        }
     
        resolve(item);
        //return;
      }

      chatroom = user1 + user2;

      let user_data = {
        pname: get_user_data["pname"],
        fname: get_user_data["fname"],
        lname: get_user_data["lname"],
        image: get_user_data["image"],
        uid: get_user_data["uid"]
      };

      let contact_data = {
        pname: data["pname"],
        fname: data["fname"],
        lname: data["lname"],
        image: data["image"],
        uid: data["uid"]
      };

      let chat_metadata = Chat_metadataModel.prototype;
      chat_metadata = {
        chat1: user_data,
        chat2: contact_data,
        chatroom: chatroom,
        timeStamp: Datetime.getTimeInMilliseconds()
      };

      this.firebase.database
        .ref("chat_customer")
        .push()
        .set(chat_metadata)
        .then(res => {
          let item = {
            item: {
              chatroom: chatroom,
              user_id: get_user_data["uid"],
              contact_id: data["uid"]
            }
          }
          resolve(item);
   
        });
    });
  }

  private async checkChatroomExist(user1, user2): Promise<string> {
 
    let exists;
    let fire = this.firebase.database.ref("chat_customer");
    await fire
      .orderByChild("chatroom")
      .equalTo(user1 + user2)
      .once("value", snap => {
        if (snap.exists()) {
          exists = "true/1";
        } else {
          exists = "false";
        }
      });

    if (exists === "true/1") {
      return exists;
    }

    this.firebase.database.ref("chat_customer");
    await fire
      .orderByChild("chatroom")
      .equalTo(user2 + user1)
      .once("value", snap => {
        if (snap.exists()) {
          exists = "true/2";
        } else {
          exists = "false";
        }
      });

    return exists;
  }
  */
}
