import { UserServiceProvider } from "./../user-service/user-service";
import { FirebaseProvider } from "./../firebase/firebase";
import {
  Cart_Model,
  Cart_Additional_Model
} from "./../../class/Model/Order/Cart";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Product_Model } from "../../class/Model/Order/Product";
import { Address_Model } from "../../class/Model/Address/address";

@Injectable()
export class CartserviceProvider {
  private cart_count: number;
  private cart_item_list: Cart_Model[] = [];
  private cart_item_list_preorder = [];
  private address: Address_Model = new Address_Model();
  private additional_data = new Cart_Additional_Model();

  private address_preorder : Address_Model = new Address_Model();
  private additional_data_preorder = new Cart_Additional_Model();

  public CART_STATE = {
    INSTOCK: "cart::instock",
    PREORDER: "cart::preorder"
  };

  public cart_state = this.CART_STATE.INSTOCK;

  constructor(
    private firebaseprovider: FirebaseProvider,
    private userservice: UserServiceProvider
  ) {
    console.log("Hello CartserviceProvider Provider");
    this.cart_count = 0;
  }

  Initial_Cart () {
    return new Promise<any>(async (resolve) => {
      this.Clear();
      this.Clear_Preorder();
      let wait1 = await this.firebaseprovider.Order_Get_List().then(async (value) => {
       
        if (value != undefined) {
          for (let i = 0; i < value.length; i++) {
            this.Add_To_Cart_From_Main_Menu(value[i]['product'], value[i]['amount']);
          } 
        }
      });
      let wait2 = await this.firebaseprovider.PreOrder_Get_List().then(async (value) => {
   
        if (value != undefined) {
          for (let i = 0; i < value.length; i++) {
            this.Add_To_Cart_From_Main_Menu_PreOrder(value[i]["product"]);
          }
        }
      });
      resolve();
    });
  }

  Set_Cart_State (flag) {
    if (flag == 0) {
      this.cart_state = this.CART_STATE.INSTOCK;
    } 
    if (flag == 1) {
      this.cart_state = this.CART_STATE.PREORDER;
    } 
  }

  Get_Cart_State () {
    return this.cart_state;
  }

  Clear() {
    this.cart_item_list.splice(0, this.cart_item_list.length);
  }

  Clear_Preorder () {
    this.cart_item_list_preorder.splice(0, this.cart_item_list_preorder.length);
  }

  List(): Cart_Model[] {
    return this.cart_item_list;
  }

  List_Preorder () {
    return this.cart_item_list_preorder;
  }
 
  Cart_Count(): number {
    //let tmp = this.cart_item_list_preorder.length;
    //this.cart_count = this.cart_item_list.length + tmp;
    this.cart_count = this.cart_item_list.length;
    return this.cart_count;
  }

  Add_To_Cart_From_Main_Menu(item, quantity: number) {
    let cart_item = new Cart_Model();
    cart_item["product"] = item;
    cart_item["quantity"] = quantity;
    this.cart_item_list.push(cart_item);
  }

  Add_To_Cart(item: Product_Model, quantity: number, flag?: Number): void {
    /*
    let item_add = this.cart_item_list.find(
      //res => res["product"]["product_id"] == item["product_id"] && res["product"]["product_price"]['price'] == item['product_price']['price'] && res["product"]["product_price"]['size'] == item['product_price']['size']
      res => res["product"]["product_id"] == item["product_id"]
    );
    */
    if (flag != 1) {
      item["price"] = this.Calculate_Each_Product_Total(item["price"]);
    }
    let item_add = this.Find_Product_Id_Contain_In_Cart(item);
    if (item_add.length != 0) {
      let bool = this.Check_If_Product_Exist_In_Cart(item, item_add);
      if (bool["bool"] == true) {
        // this.cart_item_list[bool['index']]['product']['amount'] =  Number(this.cart_item_list[bool['index']]['product']['amount']) + Number(quantity);
        let cart_item = new Cart_Model();
        cart_item["product"] = item;
        cart_item["quantity"] = quantity;
        this.cart_item_list[bool["index"]] = cart_item;
      } else {
        let cart_item = new Cart_Model();
        cart_item["product"] = item;
        cart_item["quantity"] = quantity;
        this.cart_item_list.push(cart_item);
      }
    } else {
      let cart_item = new Cart_Model();
      cart_item["product"] = item;
      cart_item["quantity"] = quantity;
      this.cart_item_list.push(cart_item);
    }
    /*
      if (item_add) {
        item_add["quantity"] = item_add["quantity"] + quantity;
      } else {
        let cart_item = new Cart_Model();
        cart_item["product"] = item;
        cart_item["quantity"] = quantity;
        this.cart_item_list.push(cart_item);
      }
      */

    if (flag != 1) {
      this.firebaseprovider.Order_Pre_Purchase(this.cart_item_list);
    }
  }

  Add_To_Cart_From_Main_Menu_PreOrder(item) {
    let cart_item = {
      product: item
    }
    this.cart_item_list_preorder.push(cart_item);
  }

  Add_To_Cart_PreOrder (item, chatroom,flag? : Number): void {
    if (flag != 1) {
        // Calculate Some shit
    }
    let cart_item = {
      product: item
    };

    this.cart_item_list_preorder.push(cart_item);

    if (flag != 1) {
      this.firebaseprovider.Set_PreOrder_Data(item, chatroom);
    }
  }

  private Find_Product_Id_Contain_In_Cart(item, flag?: number) {
    let result = [];
    let i = 0;
    if (flag == 1) {
      this.cart_item_list.forEach(value => {
        if (value["product"]["product_id"] == item["product"]["product_id"]) {
          result.push({ value: value, index: i });
          i++;
        }
      });
    } else {
      this.cart_item_list.forEach(value => {
        if (value["product"]["product_id"] == item["product_id"]) {
          result.push({ value: value, index: i });
          i++;
        }
      });
    }
    return result;
  }

  private Find_Product_Id_Contain_In_Cart_PreOrder (item, flag?: number) {
    let result = [];
    let i = 0;
    if (flag == 1) {

    } else {

    }
    return result;
  }

  private Check_If_Product_Exist_In_Cart(item_from_order, item_from_cart) {
    let item_in_cart = [];
    let index_arr = [];
    item_from_cart.forEach(res => {
      item_in_cart.push(res["value"]);
      index_arr.push(res["index"]);
    });

    let bool_arr: Array<boolean> = new Array<boolean>();
    for (let i = 0; i < item_in_cart.length; i++) {
      bool_arr.push(true);
    }
 
    for (let i = 0; i < item_in_cart.length; i++) {
      for (let j = 0; j < item_in_cart[i]["product"]["options"].length; j++) {
        if (
          //item_from_order["options"][j]["id"] !=
          //item_in_cart[i]["product"]["options"][j]["id"]
          item_from_order["options"][j]["option_filter_id"] !=
          item_in_cart[i]["product"]["options"][j]["option_filter_id"]
        ) {
          bool_arr[i] = false;
        }
      }
    }
    let contain = bool_arr.find(res => res == true);

    let index = bool_arr.indexOf(true);
    if (contain) {
      return { bool: true, index: index_arr[index] };
    } else {
      return { bool: false, index: index_arr[index] };
    }
  }

  private Check_If_Product_Exist_In_Cart_PreOrder (item_from_order, item_from_cart) {
    let item_in_cart = [];
    let index_arr = [];
    item_from_cart.forEach(value => {
      item_in_cart.push(value["value"]);
      index_arr.push(value["index"]);
    });

    let bool_arr: Array<boolean> = new Array<boolean>();
    for (let i = 0; i < item_in_cart.length; i++) {
      bool_arr.push(true);
    }

    for (let i = 0; i < item_in_cart.length; i++) {
      
    }

    let contain = bool_arr.find(res => res == true);

    let index = bool_arr.indexOf(true);
    if (contain) {
      return {bool: true, index: index_arr[index]}
    } else {
      return {bool: false, index: index_arr[index]}
    }
  }

  Alter_Cart_Quantity(
    item: Product_Model,
    quantity: number,
    flag?: Number
  ): void {
    var item_add = this.cart_item_list.find(
      res => res["product"]["product_id"] == item["product_id"]
    );

    if (item_add) {
      item_add["quantity"] = quantity;
    }
    if (flag == 1) {
      this.firebaseprovider.Order_Pre_Purchase(this.cart_item_list);
    }
  }

  Remove_From_Cart(item: Product_Model, index: number, flag?: Number) {
    /*
    if (flag != 1) {
      let item_delete = this.cart_item_list.find(
        res =>
          res["product"]["product_id"] == item["product_id"] &&
          res["product"]["product_price"]["price"] ==
            item["product_price"]["price"] &&
          res["product"]["product_price"]["size"] ==
            item["product_price"]["size"]
      );
      let index_number = this.cart_item_list.indexOf(item_delete);
      if (index_number != -1) {
        this.cart_item_list.splice(index_number, 1);
      }
    } else {
      var item_delete = this.cart_item_list.find(
        t => t["product"]["product_id"] == item["product"]["product_id"]
      );
      var index_number = this.cart_item_list.indexOf(item_delete);
      if (index_number != -1) {
        this.cart_item_list.splice(index_number, 1);
      }
    }
    */
    /*
    var item_delete = this.Find_Product_Id_Contain_In_Cart(item, 1);
    let bool = this.Check_If_Product_Exist_In_Cart(item, item_delete);
    if (bool['bool'] == true) {

      this.cart_item_list.splice(bool['index'], 1);
      this.firebaseprovider.Order_Pre_Purchase(this.cart_item_list);
    }
    */
    this.cart_item_list.splice(index, 1);

    this.firebaseprovider.Order_Pre_Purchase(this.cart_item_list);
  }

  Remove_From_Cart_Preorder (index) {
    
    this.cart_item_list_preorder.splice(index, 1);
    this.firebaseprovider.Remove_Preorder(index);
  }

  public Calculate_Each_Product_Total(price) {
    var total: number = 0;
    total =
      Number(price["product_price"]) +
      Number(price["design_price"]) +
      Number(price["promotion_price"]);
    price["total_price"] = total;
    return price;
  }

  public Calculate_Total() {
    var total: any = 0;
    for (let item of this.cart_item_list) {
      total = total + item["product"]["price"]["total_price"];
      /*
      if (item["product"]["design_pattern"]['present'] == true) {
        total = total + item["product"]["design_pattern"]['price'];
      }
      */
    }
    return total;
  }

  public Add_Address(address) {
    this.Remove_Address();
    if (address["key"] != undefined) {
      delete address["key"];
    }
    this.address = address;
    this.additional_data["address"] = this.address;
  }

  public Add_Address_Preorder(address) {
    this.Remove_Address_Preorder();
    if (address["key"] != undefined) {
      delete address["key"];
    }
    this.address_preorder = address;
    this.additional_data_preorder["address"] = this.address;
  }

  public Remove_Address() {
    this.address = new Address_Model(); 
    this.additional_data["address"] = this.address;
  }

  public Remove_Address_Preorder () {
    this.address_preorder = new Address_Model();
    this.additional_data_preorder["address"] = this.address_preorder;
  }

  public Check_Address_Select(): boolean {
    // Find better way to check if class has value.
    if (
      this.address["address"] == undefined ||
      this.address["fname"] == undefined
    ) {
      return false;
    }
    return true;
  }

  public Check_Address_Select_Preorder (): boolean {
    if (
      this.address_preorder["address"] == undefined ||
      this.address_preorder["fname"] == undefined
    ) {
      return false;
    }
    return true;
  }

  public Get_Address() {
    return this.address;
  }

  public Get_Address_Preorder () {
    return this.address_preorder;
  }

  public Add_Deliver_Type(data) {
    this.additional_data["delivery_type"] = data;
  }

  public Add_Deliver_Type_Preorder (data) {
    this.additional_data_preorder["delivery_type"] = data;
  }

  public Add_Bank(bank) {
    let payment: {
      method_type: string;
      detail: any;
    } = {
      method_type: "bank",
      detail: ""
    };
    payment.detail = {
      number : bank['number'],
      bank: bank['bank'],
      name: bank['name']
    }

    if (this.cart_state == this.CART_STATE.INSTOCK) {
      this.additional_data["payment"] = payment;
    }

    if (this.cart_state == this.CART_STATE.PREORDER) {
      this.additional_data_preorder["payment"] = payment;
    }
  }

  public Get_Additional_Data() {
    return this.additional_data;
  }

  public Get_Additional_Data_Preorder() {
    return this.additional_data_preorder;
  }

  public Add_Price(price) {
    /*
    * Flag
    *  0: product
    *  1: ship
    *  2: design
    *  3: promo
    */

    this.additional_data["price"]["product_price"] = Number(
      price["product_price"]
    );
    this.additional_data["price"]["shipping_price"] = Number(
      price["shipping_price"]
    );
    this.additional_data["price"]["promotion_price"] = Number(
      price["promotion_price"]
    );
    this.additional_data["price"]["total_price"] =
      this.additional_data["price"]["product_price"] +
      this.additional_data["price"]["shipping_price"] +
      this.additional_data["price"]["promotion_price"];
 
    //this.additional_data['sum_total'] = total;
  }

  public Add_Price_Preorder (price) {
    this.additional_data_preorder["price"]["product_price"] = Number(price["product_price"]);
    this.additional_data_preorder["price"]["shipping_price"] = Number(price["shipping_price"]);
  }

  public Add_Push_Key_For_Preorder (push_key) {
    this.additional_data_preorder["push_key"] = push_key;
  }

  public Check_If_User_Add_Design_Pattern(): boolean {
    let bool = false;
    this.cart_item_list.forEach(val => {
      if (val["product"]["design_pattern"]["present"] == true) {
        bool = true;
      }
    });
    return bool;
  }
}
