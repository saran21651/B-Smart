import { oneSignal } from "./../../class/Resource/Config";
import { AngularFireDatabase } from "angularfire2/database";
import { OneSignal } from "@ionic-native/onesignal";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { isCordovaAvailable } from "../../class/Util/CordovaCheck";
import { UserServiceProvider } from "../user-service/user-service";
import { MessageChat } from "../../class/Resource/StringList";

@Injectable()
export class OnesignalProvider {
  constructor(
    public http: HttpClient,
    public onesignal: OneSignal,
    public firebase: AngularFireDatabase,
    public userservice: UserServiceProvider
  ) {
    console.log("Hello OnesignalProvider Provider");
  }

  public get_player_id() {
   
    return new Promise<any>((resolve, reject) => {
      if (isCordovaAvailable()) {
        this.onesignal.getIds().then(
          id => {
        
            resolve(id);
          },
          err => {
            reject(err);
          }
        );
      } else {
        resolve({
          pushToken: "eel4tJEojMo:APA91bEXhEmWzXOfhXUNnQJLwMxSmlmpkF",
          userId: "d5784d2a-bc65-4a56-9f8a-0d9a8a810539"
        });
      }
    });
  }

  public set_player_id_to_user(user_key) {
    return new Promise<any>(async (resolve, reject) => {
      this.firebase.database.ref("user_information/" + user_key).update({
        onesignal_key: await this.get_player_id()
      }).then(() => {
        resolve();
      });
    });
  }

  public subscribe_chat_notification() {
    return new Promise<any>(async (resolve, reject) => {
      if (isCordovaAvailable()) {
        this.get_player_id().then(res => {

          this.onesignal.sendTag("chat_notification", res['userId']);
          resolve();
        });
      } else {
        resolve();
      }
    });
  }

  public unsubscribe_chat_notification() {
    if (isCordovaAvailable()) {
      this.onesignal.deleteTag("chat_notification");
    }
  }

  public subscribe_news_and_promotion_notification() {
    return new Promise<any>(async (resolve, reject) => {
      if (isCordovaAvailable()) {
        this.get_player_id().then(res => {
  
          this.onesignal.sendTag("news-promo_notification", res['userId']);
          resolve();
        });
      } else {
        resolve();
      }
    });
  }

  public unsubscribe_news_and_promotion_notification() {
    if (isCordovaAvailable()) {
      this.onesignal.deleteTag("news-promo_notification");
    }
  }

  public send_chat_notification(data) {
    /*
     *  data = {
     *    msg,
     *    content,
     *    onesignal_id,
     *    chatroom
     *  }
    */
    console.log(data);
    data["chatroom"]["chat_type"] = data["chat_type"];
    return new Promise<any>((resolve, reject) => {
      let header = {
        Authorization: oneSignal.REST_API_KEY,
        "Content-Type": "application/json"
      };
      let headers = new HttpHeaders(header);
      let item = {
        app_id: oneSignal.AppId,
        contents: {
          en: data["msg"]["name"] + ' : ' + data['content']
        },
        data: {
          data: {
            chatroom: data["chatroom"],
            signal_type: "chat"
          }
        },
        filters: [
          {
            field: "tag",
            key: "chat_notification",
            relation: "=",
            value: data["onesignal_id"]
          }
        ]
      };
      let url = "https://onesignal.com/api/v1/notifications";
      this.http.post(url, item, { headers }).subscribe(
        res => {
          resolve(res);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  public get_contact_player_id (uid, type) {
    let query = "";
    switch (type) {
      case MessageChat.ChatType.STAFF_TO_STAFF:
        query = "employee_information";
        break;
      case MessageChat.ChatType.CUSTOMER_TO_SALE:
        query = "employee_information";
        break;
      case MessageChat.ChatType.CUSTOMER_TO_SALE_PREORDER:
        query = "employee_information";
      default:
    }
    console.log(type);
    console.log(query);
    console.log(uid);
    return new Promise<any>((resolve, reject) => {
      if (query == "employee_information") {
        this.firebase
        .database
        .ref(query)
        .orderByChild('uid')
        .equalTo(uid)
        .once('value', (snap) => {
          if (snap.exists()) {
            snap.forEach ((child) => {
              resolve(child.child('onesignal_key/userId').val());
            });
          } else {
            //reject ("Not find any onesignal key") ;
            resolve('.....');
          }
        });
      } else {
        this.firebase
        .database
        .ref(query)
        .orderByChild('auth/uid')
        .equalTo(uid)
        .once('value', (snap) => {
          if (snap.exists()) {
            snap.forEach ((child) => {
              resolve(child.child('onesignal_key/userId').val());
            });
          } else {
            //reject ("Not find any onesignal key") ;
            this.firebase
              .database
              .ref("employee_information")
              .orderByChild("uid")
              .equalTo(uid)
              .once('value', (snap) => {
                if (snap.exists())
                {
                  snap.forEach ((child) => {
                    resolve(child.child('onesignal_key/userId').val());
                  });
                } else {
                  resolve('.....');
                }
              });
          }
        });
      }
    });
  }
}
