import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class OrderCarrierProvider {

  private data_order: any;

  constructor(public http: HttpClient) {
    console.log('Hello OrderCarrierProvider Provider');
  }

  public Set (data) {
    this.data_order = data;
  }

  public Get () {
    return this.data_order;
  }

  public Clear () {
    delete this.data_order;
  }

  public Exist (): boolean {
    if (this.data_order == undefined) {
      return false;
    } else {
      return true;
    }
  }

}
