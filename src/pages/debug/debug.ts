import { AngularFireDatabase } from 'angularfire2/database';
import { CartserviceProvider } from './../../providers/cartservice/cartservice';
import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { ModalProvider } from './../../providers/modal/modal';
import { Page } from './../../class/Resource/Page';
import { CallNumber } from '@ionic-native/call-number';
import { Api } from './../../class/Resource/API';
import { oneSignal } from './../../class/Resource/Config';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';

import { InAppBrowser } from "@ionic-native/in-app-browser";
import { SocialSharing } from '@ionic-native/social-sharing';
import { Storage } from '@ionic/storage';
import { User_Storage } from '../../class/Storage/UserStorage';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { OneSignal } from '../../../node_modules/@ionic-native/onesignal';
import { Product_Model } from '../../class/Model/Order/Product';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Hash } from '@firebase/util/dist/src/hash';
import { Hashing } from '../../class/Util/Hashing';
import { ChatServiceProvider } from '../../providers/chat-service/chat-service';

export const myConst = {
  InstallApp: {
    ios: {
      appId: '',
    },
    android: {
      appId: ''
    }
  }
}

@IonicPage()
@Component({
  selector: 'page-debug',
  templateUrl: 'debug.html',
})
export class DebugPage {

  text: any;
  data: any;

  page_select: any;
  Page: any;

  private firemessage: firebase.messaging.Messaging;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public socialSharing: SocialSharing,
    public storage: Storage,
    public http: HttpClient,
    public alert: AlertController,
    public fireauth: AngularFireAuth,
    public toastCtrl: ToastController,
    public onesignal: OneSignal,
    public modal: ModalProvider,
    public call: CallNumber,
    public cartservice: CartserviceProvider,
    public firebase: AngularFireDatabase,
    public authservice: AuthServiceProvider,
    public social: SocialSharing,
    public firebaseprovider: FirebaseProvider,
    public chat: ChatServiceProvider
  ) {
    /*
      this.data = navParams.get('data');
      this.get_page();
      if (this.data != null) {
        this.toasty(this.data);
      }
      this.test_load();
      this.calc();
      this.page_list();

      let str = Hashing.prototype.MD5("admin");
      console.log(str);
      */
     this.firebaseprovider.Get_Order_Instock_Revamp(5, 5).then(res => {
       // console.log(res);
     });

     this.test_random_chat();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DebugPage');
    this.storage.set('Test', '123456');
    //this.login();
    this.test_api();
  }

  GOTO_MAINAPP () {
    this.navCtrl.push(Page.Authentication.Login.Page);
  }

  OPENAPP () {
    this.test_api ();

  }

  async TESTSTORAGEFROMCLASS () {
    /*
    this.storage.get('Test1').then((value) => {
      console.log('Test Value: ', value);
    });

    let store = new User_Storage(this.storage);

    console.log('TEST Return: ', await store.Test_Return());
    */
    //console.log(this.firemessage.getToken());
    this.call.callNumber('0988097116', true);
  }


  SEND_NOTIFICATION () {
    let header = {
      'Authorization': oneSignal.REST_API_KEY,
      'Content-Type': 'application/json'
    };
    let headers = new HttpHeaders(header);
    let item = {
      "app_id": oneSignal.AppId,
      "included_segments": ["All"],
      "contents": {
        "en": "test hybrid ภาษา",
      },
      "data" : {
        "data": "Noon KAKAKAKAKAKAKAKAKAK"
      }
    }
    let url = "https://onesignal.com/api/v1/notifications";
    this.http.post(url, item, { headers }).subscribe(val => {
      //this.ALERT(val);
      console.log(val);
    }, err => {
      //this.ALERT(err);
      console.log(err);
    });
  }

  ALERT (message) {
    this.alert.create({
      title: 'Alert',
      subTitle: message,
    }).present();
  }

  login() {
    this.fireauth
      .auth
      .signInWithEmailAndPassword('test@test.com', '123456')
      .then(data => {
        console.log(data);
        this.firemessage = firebase.messaging();
        console.log(this.firemessage.getToken());
      }, err => {
        console.log(err);
      });
  }

  toasty(data) {
    const toast = this.toastCtrl.create({
      duration: 5000,
      position: 'top',
      message: data
    });
    //this.text = data['additionalData']['data'];
    //console.log(data['additionalData']['data']);
  }

  Send_Tag () {
    this.onesignal.sendTag('subscribe_event', '1');
    this.toasty('Send tag success');
  }

  Delete_Tag () {
    this.onesignal.deleteTag('subscribe_event');
    this.toasty('Delete tag succes');
  }

  test_api () {
    console.log(Api.LoginApi);
    let headerStr = {
      "Access-Control-Allow-Origin": "*"
    }
    let headers = new HttpHeaders(headerStr);
    this.http.get(Api.LoginApi).subscribe(res => {
      console.log(res);
    }, err => {
      console.log(err);
    });
  }

  get_page () {
    this.Page = Object.keys(Page);
  }

  onSelectChange_Page (event) {
    console.log(typeof(event));
    if (typeof(event) == 'object') {
      return
    }
    this.page_select = Page[event];
    console.log(event);
    console.log(Page);
    console.log(this.page_select);
  }

  go_to_page () {

  }

  test_modal () {
    this.modal.Open_Modal(Page.Menu.OrderTracking.Comment.Page, 'inset-modal-order-modal', {});
  }

  test_cart () {

  }

  GO_TO_BANK () {
    this.navCtrl.push(Page.Menu.Purchase.Page);
  }

  PAYMENT_CLICK () {
    this.navCtrl.push(Page.Menu.Purchase.PaymentInform.Page);
  }

  CLEAR_CLICK () {
    this.storage.clear();
  }

  TEST_GET_FALSE_CLICK () {
    this.cartservice.Check_If_User_Add_Design_Pattern();
  }

  items : any = [];
  limit = 10;
  test_load () {
    this.firebase.database.ref("z_test").limitToFirst(this.limit).once("value", (snap) => {
      let arr = [];
      snap.forEach((child) => {
        arr.push({key:child.key});
        //this.items.push({key: child.key});
      });
      this.items = arr;
    });
  }
  doInfinite(refresh) {
    console.log(refresh);
    this.limit = this.limit + 3;
    setTimeout(() => {
      this.test_load();
      refresh.complete();
    }, 2000);
  }
  starClicked(value){
    this.value = value + 1;
		this.calc();
  }

  // stars: string[] = [];
  stars: string[] = [];
  value: number = 5;
  numStars: number = 5;
  calc(){
    setTimeout(() => {
      this.stars = [];
      let tmp = this.value;
      for(let i=0; i < this.numStars; i++, tmp--)
        if(tmp >= 1)
          this.stars.push("star");
        else if (tmp < 1 && tmp > 0)
          this.stars.push("star-half");
        else
          this.stars.push("star-outline");
    }, 0);
  }



  FACEBOOK_CLICK () {
    this.authservice.facebook_login().then(() => {
      
    });
  }

  LINE_CLICK () {
    let appId = "jp.naver.line.android";
    this.social.shareVia(appId,"1234").then((res) => {
      console.log("RES");
      console.log(res);
    }).catch((err) => {
      console.log("ERR");
      console.log(err);
    });
  }

  LINE_CLICK2 () {

  }

  Firebase_Click () {
    this.firebase.database.ref('bank').once("value", (snap) => {
      console.log(snap.val());
    });
    this.firebaseprovider.Get_Version().then((res) => {
      console.log(res);
    });
  }

  PAGE_CLICK (page: string) {
    this.navCtrl.push(page);
  }

  page_array = [];

  page_list () {
    /*
    let tmp = Object.keys(Page);
    console.log(tmp);
    for (let v = 0; v < tmp.length; v++) {
      console.log(Page[tmp[v]]);
      let tmp2 = Page[tmp[v]];
      for (var  w in tmp2) {
        console.log(tmp2.hasOwnProperty(w));
        if (tmp2.hasOwnProperty(w)) {
          console.log(tmp2);
        }
  
      }
    } */
    let page = [
      Page.Authentication.Login.Page,
      Page.Authentication.Register.Page,
      Page.Menu.Page,
      Page.Menu.Cart.Page,
      Page.Menu.Cart.Checkout.Page,
      Page.Menu.Check.Page,
      Page.Menu.News.Page,
      Page.Menu.News.Detail.Page,
      Page.Menu.Option.Page,
      Page.Menu.Order.Page,
      Page.Menu.Order.Detail.Page,
      Page.Menu.Order.Detail.Modal,
      Page.Menu.OrderTracking.Page,
      Page.Menu.OrderTracking.Accept.Page,
      Page.Menu.OrderTracking.Comment.Page,
      Page.Menu.OrderTracking.Detail.Page,
      Page.Menu.Profile.Page,
      Page.Menu.Profile.Address.Page,
      Page.Menu.Profile.Address.Add.Page,
      Page.Menu.Profile.Address.Edit.Page,
      Page.Menu.Profile.Address.Modal.Delete,
      Page.Menu.Profile.CreditCard.Page,
      Page.Menu.Profile.CreditCard.Add.Page,
      Page.Menu.Profile.CreditCard.Edit.Page,
      Page.Menu.Profile.Edit.Page,
      Page.Menu.Profile.Order.Page,
      Page.Menu.Profile.Order.Detail.Page,
      Page.Menu.Promotion.Page,
      Page.Menu.Promotion.Detail.Page,
      Page.Menu.Purchase.Page,
      Page.Menu.Purchase.Bank.Page,
      Page.Menu.Purchase.CreditCard.Page,
      Page.Menu.Purchase.PaymentInform.Page
    ];
    this.page_array = page;
  }

  test_random_chat () {
    console.log('test random chat');
    this.chat.Fetch_All_Contact().then(res => {
      console.log('random chat');
      console.log(res);
    });
  }

}
