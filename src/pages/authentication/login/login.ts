import { ControllerProvider } from "./../../../providers/controller/controller";
import { CartserviceProvider } from "./../../../providers/cartservice/cartservice";
import { FirebaseProvider } from "./../../../providers/firebase/firebase";
import { OneSignal } from "@ionic-native/onesignal";
import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController
} from "ionic-angular";
import { Storage } from "@ionic/storage";
import { Page } from "../../../class/Resource/Page";
// Firebase
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireAuth } from "angularfire2/auth";
import { User_Storage } from "../../../class/Storage/UserStorage";
import { AuthServiceProvider } from "../../../providers/auth-service/auth-service";
import { UserServiceProvider } from "../../../providers/user-service/user-service";

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  @ViewChild("email_input")
  _email_Input;
  @ViewChild("password_input")
  _password_Input;

  openMenu = false;
  loadingObj: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public onesignal: OneSignal,
    public fireauth: AngularFireAuth,
    public firebase: AngularFireDatabase,
    public loadCtrl: LoadingController,
    public cartservice: CartserviceProvider,
    public authservice: AuthServiceProvider,
    public controller: ControllerProvider,
    public firebaseprovider: FirebaseProvider,
    public userservice: UserServiceProvider,
  ) {
   
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad LoginPage");
  }

  LOGIN_CLICK() {
    this.login();
  }

  CREATEACCOUNT_CLICK() {
    this.navCtrl.push(Page.Authentication.Register.Page);
  }

  TOGGLEPOPUPMENU_CLICK(): boolean {
    return (this.openMenu = !this.openMenu);
  }

  FACEBOOK_CLICK() {
    this.login_facebook();
  }

  showLoading() {
    /*
    this.loadingObj = this.loadCtrl.create({
      spinner: "hide",
      content: `
      <div class="cssload-loader-inner">
      <div class="cssload-cssload-loader-line-wrap-wrap">
        <div class="cssload-loader-line-wrap"></div>
      </div>
      <div class="cssload-cssload-loader-line-wrap-wrap">
        <div class="cssload-loader-line-wrap"></div>
      </div>
      <div class="cssload-cssload-loader-line-wrap-wrap">
        <div class="cssload-loader-line-wrap"></div>
      </div>
      <div class="cssload-cssload-loader-line-wrap-wrap">
        <div class="cssload-loader-line-wrap"></div>
      </div>
      <div class="cssload-cssload-loader-line-wrap-wrap">
        <div class="cssload-loader-line-wrap"></div>
      </div>
    </div>
    `,
      cssClass: "transparent"
    });
    this.loadingObj.present();
    */
  }

  hideLoading() {
    this.loadingObj.dismiss();
  }

  private login() {
    // this.showLoading();
    this.controller.Show_Loading();
    this.authservice
      .login(this._email_Input.value, this._password_Input.value)
      .then(
        res => {
          this.userservice.get_user_id().then(res => {
    
            if (res != undefined) {
              this.authservice.check_firsttime_login(res).then(res => {
                // true // false
                this.controller.Hide_Loading();
                if (res == true) {
                  this.navCtrl.setRoot(
                    Page.Menu.Profile.Edit.Page, {index: 1}, {
                      animate: true,
                      direction: "forward"
                    }
                  );
                } else if (res == false) {
                  this.navCtrl.setRoot(
                    Page.Menu.Page,{}, {
                      animate: true,
                      direction: "forward"
                    }
                  );
                } else {
                  // If error
                }
              });
            }
          });
        },
        err => {
          this.controller
            .Show_Alert_Success(1, "ล้มเหลว", err["message"])
            .then(() => {
              this.controller.Hide_Loading();
            });
        }
      );
  }

  private login_facebook() {
    this.authservice.facebook_login();
  }

  
}
