import { Page } from './../../../class/Resource/Page';
import { ControllerProvider } from './../../../providers/controller/controller';
import { AuthServiceProvider } from './../../../providers/auth-service/auth-service';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  @ViewChild("email_input") _email_Input;
  @ViewChild("password_input") _pass_input;
  @ViewChild("confirmpass_input") _conpass_input;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fireauth: AngularFireAuth,
    public firebase: AngularFireDatabase,
    public authservice: AuthServiceProvider,
    public controller: ControllerProvider
  ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  REGISTER_CLICK () {
    this.Register();
  }

  FACEBOOK_CLICK () {
    this.FacebookLogin();
  }

  Register () {
    this.controller.Show_Loading();
    this.authservice.register(this._email_Input.value, this._pass_input.value).then(res => {
      this.controller.Show_Alert_Success(1, "ลงทะเบียนสำเร็จ", "").then(() => {
        this.controller.Hide_Loading();
        this.navCtrl.push(Page.Authentication.Login.Page);
      });
    }, err => {
      this.controller.Show_Alert_Success(1, "ล้มเหลว", err['message']).then(() => {
        this.controller.Hide_Loading();
      });
    });
  }

  FacebookLogin() {
    let provider = new firebase.auth.FacebookAuthProvider();
    this.fireauth
      .auth
      .signInWithPopup(provider)
      .then(res => {
        
      }), err => {
       
      };
  }

  GoogleLogin() {

  }

}
