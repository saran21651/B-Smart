import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Page } from '../../../../class/Resource/Page';
import { FirebaseProvider } from '../../../../providers/firebase/firebase';


@IonicPage()
@Component({
  selector: 'page-main-menu-promotion-detail',
  templateUrl: 'main-menu-promotion-detail.html',
})
export class MainMenuPromotionDetailPage {

  item: any;
  imgc = [];

  product_info: any; // For check if data exists

  product_exist = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public firebaseprovider: FirebaseProvider
    ) {
      this.item = navParams.get("data");
      console.log(this.item);
      this.Init(this.item['image']);  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuNewsDetailPage');
  }

  private Init (data) {
    this.imgc = [];
    data.forEach ((img) => {
      this.imgc.push(img);
    });
    this.firebaseprovider.Get_Product_By_Order_Id(this.item["promotion"]).then(res => {
      console.log(res);
     this.product_info = res;
     if (res != undefined) {
      this.product_exist = true;
     }
    });
  }

  onSlideChanged() {

  }

  BACK_CLICK () {
    this.back_button();
  }

  PRODUCT_CLICK () {
    this.product_button();
  }

  private product_button () {
    console.log(this.item);
    /*
    this.firebaseprovider.Get_Product_By_Order_Id(this.item["promotion"]).then(res => {
      console.log(res);
      this.navCtrl.push(Page.Menu.Order.Detail.Page, {data: res});
    });
    */
   this.navCtrl.push(Page.Menu.Order.Detail.Page, {data: this.product_info});
  }

  private back_button() {
    if (this.navCtrl.length() == 1) {
      this.navCtrl.setRoot(
        Page.Menu.Promotion.Page,
        {},
        {
          animate: false,
          direction: "backward"
        }
      );
    } else {
      this.navCtrl.pop();
    }
  }

}
