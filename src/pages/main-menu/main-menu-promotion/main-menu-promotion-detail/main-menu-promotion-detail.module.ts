import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuPromotionDetailPage } from './main-menu-promotion-detail';

@NgModule({
  declarations: [
    MainMenuPromotionDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuPromotionDetailPage),
  ],
})
export class MainMenuPromotionDetailPageModule {}
