import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuPromotionPage } from './main-menu-promotion';

@NgModule({
  declarations: [
    MainMenuPromotionPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuPromotionPage),
  ],
})
export class MainMenuPromotionPageModule {}
