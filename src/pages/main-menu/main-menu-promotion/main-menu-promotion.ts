import { FirebaseProvider } from './../../../providers/firebase/firebase';
import { GlobalString } from '../../../class/Resource/StringList';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { TempData } from '../../../class/_Temp_data/tempdata';
import { Page } from '../../../class/Resource/Page';

@IonicPage()
@Component({
  selector: 'page-main-menu-promotion',
  templateUrl: 'main-menu-promotion.html',
})
export class MainMenuPromotionPage {

  // items = TempData.prototype.promotion_data();
  items: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public firebaseprovider: FirebaseProvider
  ) {
    this.Init();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuPromotionPage');
  }

  ionViewWillEnter() {

  }

  ionViewWillLeave() {

  }

  Init () {
    console.log("Init");
    this.firebaseprovider.Get_News_Or_Promotion(1).then((res) => {
      let tmp = res;
      for (let i = 0; i < tmp.length; i++) {
        tmp[i]['detail_short'] = this.limit_char(res[i]['detail']);
      }
      this.items = tmp;
    });
  }

  DETAIL_CLICK (item) {
    this.navCtrl.push(Page.Menu.Promotion.Detail.Page, {data: item});
    // this.navCtrl.push(Page.Menu.Order.Detail.Page, {data:item['promotion']});
  }

  BACK_CLICK () {
    this.back_button();
  }

  private limit_char (char: string) {
    let limit = 200;
    let new_string = "";
    for (let i = 0; i < limit; i++) {
      new_string = new_string + char[i];
    }
    new_string = new_string + "...";
    return new_string;
  }

  private back_button() {
    if (this.navCtrl.length() == 1) {
      this.navCtrl.setRoot(
        Page.Menu.Page,
        {},
        {
          animate: false,
          direction: "backward"
        }
      );
    } else {
      this.navCtrl.pop();
    }
  }
}
