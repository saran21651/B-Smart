import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuCartCheckoutPreorderPage } from './main-menu-cart-checkout-preorder';

@NgModule({
  declarations: [
    MainMenuCartCheckoutPreorderPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuCartCheckoutPreorderPage),
  ],
})
export class MainMenuCartCheckoutPreorderPageModule {}
