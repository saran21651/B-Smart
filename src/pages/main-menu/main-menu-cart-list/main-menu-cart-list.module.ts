import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuCartListPage } from './main-menu-cart-list';
import { SwipeSegmentDirective } from '../../../directives/swipe-segment.directive';

@NgModule({
  declarations: [
    MainMenuCartListPage,
    SwipeSegmentDirective
  ],
  imports: [
    IonicPageModule.forChild(MainMenuCartListPage),
  ],
})
export class MainMenuCartListPageModule {}
