import { Page } from './../../../class/Resource/Page';
import { CartserviceProvider } from './../../../providers/cartservice/cartservice';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-main-menu-cart-list',
  templateUrl: 'main-menu-cart-list.html',
})
export class MainMenuCartListPage {

  data = this.cartservice.List();
  data_preorder = this.cartservice.List_Preorder();
  total: number = this.cartservice.Calculate_Total();

  // Workaround
  is_exist_from_continue_button = false;

  // Tabs
  category: string = "instock";
  categories = ['instock', 'preorder'];


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public cartservice: CartserviceProvider
  ) {
    console.log(this.data_preorder);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuCartListPage');
  }

  ionViewWillEnter () {
    this.is_exist_from_continue_button = false;
  }

  ionViewWillLeave() {
    if (this.is_exist_from_continue_button == false) {
      this.navCtrl.setRoot(Page.Menu.Page, {}, {
        animate: false,
        direction: 'backward'
      });
    }
  }

  INC_ORDER_TAP (item, i) {
    this.inc_value(item, i);
  }

  DEC_ORDER_TAP (item, i) {
   this.dec_value(item, i);
  }

  DELETE_CLICK (product, i) {
    this.cartservice.Remove_From_Cart(product, i);
    this.total = this.cartservice.Calculate_Total();
  }

  DELETE_PREORDER_CLICK (i) {
    this.cartservice.Remove_From_Cart_Preorder(i);
    this.total = this.cartservice.Calculate_Total();
  }

  PURCHASE_CLICK () {
    this.is_exist_from_continue_button = true;
    this.cartservice.Set_Cart_State(0);
    this.navCtrl.push(Page.Menu.Cart.Checkout.Page);
  }

  CONTINUE_CLICK () {
    this.is_exist_from_continue_button = true;
    this.navCtrl.push(Page.Menu.Order.Page);
  }

  BACK_CLICK () {
    this.navCtrl.setRoot(Page.Menu.Page, {}, {
      animate: false,
      direction: 'backward'
    });
  }

  IsEmptyCart () : boolean {
    if (this.cartservice.Cart_Count() == 0) {
      return true;
    }
    return false;
  }

  onTabChanged (event) {
    this.category = event.value;
  }

  inc_value(item, i) {
    item['quantity'] = item['quantity'] + 1;

    if (item['quantity'] > item['product']['product_stock']) {
      item['quantity'] = item['product']['product_stock'];
    }
    this.cartservice.Alter_Cart_Quantity(item['product'], item['quantity']);
    this.total = this.cartservice.Calculate_Total();
  }

  dec_value (item, i) {
    if (item['quantity'] <= 1) {
      item['quantity'] = 1;
    } else {
      item['quantity']--;
    }
    this.cartservice.Alter_Cart_Quantity(item['product'], item['quantity']);
    this.total = this.cartservice.Calculate_Total();
  }

}
