import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuCartCheckoutPage } from './main-menu-cart-checkout';

@NgModule({
  declarations: [
    MainMenuCartCheckoutPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuCartCheckoutPage),
  ],
})
export class MainMenuCartCheckoutPageModule {}
