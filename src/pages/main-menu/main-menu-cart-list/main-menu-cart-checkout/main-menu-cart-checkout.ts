import { Address_Model } from "./../../../../class/Model/Address/address";
import { Page } from "./../../../../class/Resource/Page";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { CartserviceProvider } from "../../../../providers/cartservice/cartservice";
import { FirebaseProvider } from "../../../../providers/firebase/firebase";
@IonicPage()
@Component({
  selector: "page-main-menu-cart-checkout",
  templateUrl: "main-menu-cart-checkout.html"
})
export class MainMenuCartCheckoutPage {
  /*
  * Page Flag
  * 0 = Normal Page
  * 1 = Back from select address
  */
  data = this.cartservice.List();
  total: number = this.cartservice.Calculate_Total();

  page_flag: number = 0;
  address: Address_Model = new Address_Model();
  //delivery_selected = "5";

  delivery_selected_item: any = 4; // Select Address
  delivery_selected : 1;
  delivery_cost = 0;
  purchase_data: any;
  shipping: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public cartservice: CartserviceProvider,
    public firebaseprovider: FirebaseProvider
  ) {
    
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MainMenuCartCheckoutPage");
    this.cartservice.Remove_Address();
  }

  ionViewDidEnter () {
 
  }

  ionViewWillEnter() {
    this.init();
  }

  ADDRESS_CLICK() {
    this.navCtrl.push(Page.Menu.Profile.Address.Page, { data: 1 });
  }

  PURCHASE_CLICK() {
    if (this.delivery_selected == undefined) {
      this.delivery_selected = this.shipping[this.delivery_selected_item];
    }

    this.cartservice.Add_Deliver_Type(this.delivery_selected["type"]);
 
    let total_price = {
      product_price: Number(this.total) - Number(this.delivery_cost),
      shipping_price: Number(this.delivery_cost),
      promotion_price: 0,
      total_price: 0
    };
    this.cartservice.Add_Price(total_price);
    this.navCtrl.push(Page.Menu.Purchase.Page);
  }

  CHANGE_ADDRESS_CLICK() {
    this.navCtrl.push(Page.Menu.Profile.Address.Page, { data: 1 });
  }

  private init() {
    if (this.cartservice.Check_Address_Select()) {
      this.page_flag = 1;
      this.address = this.cartservice.Get_Address();
    }
    this.firebaseprovider.Get_Shipping_Charge().then(data => {
      this.shipping = data;
    });
  }

  onChange(event) {
    if (event !== undefined  && this.shipping !== undefined) {

      this.delivery_selected = this.shipping[event];
      
      this.delivery_cost = this.shipping[event]['charge']
      this.total = this.cartservice.Calculate_Total() + Number(this.delivery_cost); 
    }

  }
}
