import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuPurchasePage } from './main-menu-purchase';

@NgModule({
  declarations: [
    MainMenuPurchasePage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuPurchasePage),
  ],
})
export class MainMenuPurchasePageModule {}
