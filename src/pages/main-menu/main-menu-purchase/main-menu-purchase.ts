import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Page } from '../../../class/Resource/Page';


@IonicPage()
@Component({
  selector: 'page-main-menu-purchase',
  templateUrl: 'main-menu-purchase.html',
})
export class MainMenuPurchasePage {

  radioSelected: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuPurchasePage');
  }

  BANK_CLICK () {
    this.navCtrl.push(Page.Menu.Purchase.Bank.Page);
  }

  CREDIT_CLICK () {
    this.navCtrl.push(Page.Menu.Purchase.CreditCard.Page);
  }

  NEXT_CLICK () {
    if (this.radioSelected == 0) {
      this.navCtrl.push(Page.Menu.Purchase.Bank.Page);
    }
    if (this.radioSelected == 1) {
      this.navCtrl.push(Page.Menu.Purchase.CreditCard.Page);
    }
  }

}
