import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuPurchasePaymentinformPage } from './main-menu-purchase-paymentinform';

@NgModule({
  declarations: [
    MainMenuPurchasePaymentinformPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuPurchasePaymentinformPage),
  ],
})
export class MainMenuPurchasePaymentinformPageModule {}
