import { Page } from './../../../../class/Resource/Page';
import { AngularFireStorage } from 'angularfire2/storage';
import { FirebaseProvider } from './../../../../providers/firebase/firebase';
import { Camera } from '@ionic-native/camera';
import { CameraOptions } from '@ionic-native/camera';
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { DatePicker } from "@ionic-native/date-picker";
import { ImageUtil } from '../../../../class/Util/ImageUtil';
import { UserServiceProvider } from '../../../../providers/user-service/user-service';
import { Datetime } from '../../../../class/Util/Datetime';
import { BankNameList } from '../../../../class/Resource/StringList';
@IonicPage()
@Component({
  selector: "page-main-menu-purchase-paymentinform",
  templateUrl: "main-menu-purchase-paymentinform.html"
})
export class MainMenuPurchasePaymentinformPage {
  // ngModel
  selectInput: {
    account: any;
    payment_date: any;
    transfer: any;
    amount: any;
    evident_pic: any;
    evident_pic_file_path: any;
  } = {
    account: "init",
    payment_date: "",
    transfer: "",
    amount: "",
    evident_pic: "",
    evident_pic_file_path: ""
  };

  bank_list = [];

  display_date: any = "";
  display_time: any = "";

  isimageloading = false;

  order_info : any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public datePicker: DatePicker,
    public camera: Camera,
    public userservice: UserServiceProvider,
    public firebaseprovider: FirebaseProvider,
    public firestorage: AngularFireStorage
  ) {
    this.order_info = navParams.get("data");
    this.display_date = Datetime.get_current_date(0);
    this.display_time = Datetime.get_current_date(1);
    this.selectInput.payment_date = Datetime.get_current_date(2);
    this.selectInput.transfer = Datetime.get_current_date(2);
    this.bank_list.push(BankNameList);
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MainMenuPurchasePaymentinformPage");
  }

  DATEPICKER_CLICK() {
    this.datePicker
      .show({
        date: new Date(),
        mode: "date",
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      })
      .then(
        date => {
        
          this.display_date = Datetime.get_partial_datetime_from_picker(date, 0);
          this.selectInput.payment_date = date;
          console.log(this.selectInput);
        },
        err => console.log("Error occurred while getting date: ", err)
      );
  }

  TIMEPICKER_CLICK() {
  
    this.datePicker
      .show({
        date: new Date(),
        mode: "time",
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
        is24Hour: true
      })
      .then(
        date => {
      
          this.display_time = Datetime.get_partial_datetime_from_picker(date, 1);
          this.selectInput.transfer = date;
        },
        err => console.log("Error occurred while getting date: ", err)
      );
  }

  IMAGECHOOSE_CLICK() {
    this.call_camera();
  }

  ACCEPT_CLICK () {
    // if (this.selectInput.payment_date == "" || null) this.selectInput.payment_date = this.display_date; 
    // if (this.selectInput.transfer == "" || null) this.selectInput.transfer = this.display_time;

    let timestamp = Datetime.get_timestamp_from_full_date(this.selectInput.payment_date, this.selectInput.transfer);
    let data = {
      timestamp: timestamp || "sd",
      account: this.selectInput.account,
      evident_pic: this.selectInput.evident_pic,
      order_id: this.order_info["order_id"]
    }
    this.firebaseprovider.Order_Payment_Inform(data).then((res) => {
      if (res) {
        // const startIndex = this.navCtrl.getActive().index - 1;
        /*
        this.navCtrl.remove(startIndex, 2).then(() => {
          this.navCtrl.push(Page.Menu.OrderTracking.Page);
        });
        */
        let currentIndex = this.navCtrl.getActive().index;
        this.navCtrl.remove(currentIndex - 1).then(() => {
          this.navCtrl.popTo(Page.Menu.OrderTracking.Page);
        });
      } else {
        console.log(res);
      }
    });

  }

  private call_camera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetHeight: 800,
      targetWidth: 600,
      sourceType: 1
    };
    this.load_image();
    this.camera.getPicture(options).then(
      res => {
        let base64image = "data:image/jpeg;base64," + res;
        let img = ImageUtil.prototype.datauri_to_blob(base64image);
        this.image_photo(img);
      },
      err => {
        console.log(err);
      }
    );
  }

  private async image_photo(event) {
    if (event == undefined) {
      return;
    }
 
    let img: any;
    let file_path: any;
    let uid = this.userservice.get_user_id();
    img = event;
    file_path = "image_user/" + uid + "/";
    // this.selectInput.evident_pic = img;
    this.selectInput.evident_pic_file_path = file_path;
    await this.firestorage.storage.ref(file_path).put(img);
    let fileUrl = await this.firestorage.storage
      .ref(file_path)
      .getDownloadURL();
    this.load_image();
    this.selectInput.evident_pic = fileUrl;
  }

  Check () {
    let bool = true;
    if (this.selectInput.account != "init" && this.selectInput.amount != ""
        && this.selectInput.evident_pic != ""
        && this.selectInput.payment_date != "" && this.selectInput.transfer != "") {
        bool = false;
    } else {
      bool = false;
    }
    return bool;
  }

  load_image () {
    this.isimageloading = !this.isimageloading;
  }
}
