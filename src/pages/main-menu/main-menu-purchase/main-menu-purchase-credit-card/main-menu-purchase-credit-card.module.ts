import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuPurchaseCreditCardPage } from './main-menu-purchase-credit-card';

@NgModule({
  declarations: [
    MainMenuPurchaseCreditCardPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuPurchaseCreditCardPage),
  ],
})
export class MainMenuPurchaseCreditCardPageModule {}
