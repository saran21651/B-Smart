import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-main-menu-purchase-credit-card',
  templateUrl: 'main-menu-purchase-credit-card.html',
})
export class MainMenuPurchaseCreditCardPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuPurchaseCreditCardPage');
  }

}
