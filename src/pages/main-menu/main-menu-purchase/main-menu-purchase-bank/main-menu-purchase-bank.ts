import { FirebaseProvider } from './../../../../providers/firebase/firebase';
import { ControllerProvider } from './../../../../providers/controller/controller';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CartserviceProvider } from '../../../../providers/cartservice/cartservice';
import { Page } from '../../../../class/Resource/Page';


@IonicPage()
@Component({
  selector: 'page-main-menu-purchase-bank',
  templateUrl: 'main-menu-purchase-bank.html',
})
export class MainMenuPurchaseBankPage {

  bank: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public controller: ControllerProvider,
    public cartservice: CartserviceProvider,
    public firebaseprovider: FirebaseProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuPurchaseBankPage');
  }

  ionViewWillEnter () {
    this.Init();
  }

  Init () {
    this.firebaseprovider.Get_Bank().then((data) => {
      this.bank = data;
    });
  }

  BANK_CLICK (data) {

    this.cartservice.Add_Bank(data);
    if (this.cartservice.Get_Cart_State() == this.cartservice.CART_STATE.INSTOCK) {
      let bool = this.cartservice.Check_If_User_Add_Design_Pattern();
      this.firebaseprovider.Order_Purchase(this.cartservice.Get_Additional_Data()).then(() => {
        this.cartservice.Clear();
        if (bool) {
          this.controller.Show_Alert_Success(1, "ทำการสั่งซื้อสำเร็จ", "").then(() => {
            this.navCtrl.setRoot(Page.Menu.Page, {}, {
              animate: true,
              direction: 'forward'
            });
          });

        } else {
          this.controller.Show_Alert_Success(1, "ทำการสั่งซื้อสำเร็จ", "กรุณาแจ้งรายละเอียดออกแบบไฟล์กับพนักงาน").then(() => {
            this.navCtrl.setRoot(Page.Menu.Chat.Contact.Page, {}, {
              animate: true,
              direction: 'forward'
            });
          });
        }
      });
    }
    if (this.cartservice.Get_Cart_State() == this.cartservice.CART_STATE.PREORDER) {
      this.firebaseprovider.Preorder_Purchase(this.cartservice.Get_Additional_Data_Preorder()).then(() => {
        this.controller.Show_Alert_Success(1, "ทำการสั่งซื้อสำเร็จ", "").then(() => {
          this.navCtrl.setRoot(Page.Menu.Page, {}, {
            animate: true,
            direction: 'forward'
          });
        });
      });
    }
    

    /*
    this.controller.Show_Toast('สั่งซื้อสำเร็จ', 'bottom');
    this.navCtrl.setRoot(Page.Menu.Page, {}, {
      animate: true,
      direction: 'forward'
    });
    */
  }

}
