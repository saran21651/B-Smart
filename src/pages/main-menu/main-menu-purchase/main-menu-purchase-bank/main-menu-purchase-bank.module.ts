import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuPurchaseBankPage } from './main-menu-purchase-bank';

@NgModule({
  declarations: [
    MainMenuPurchaseBankPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuPurchaseBankPage),
  ],
})
export class MainMenuPurchaseBankPageModule {}
