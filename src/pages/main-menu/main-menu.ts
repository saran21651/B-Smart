import { FirebaseProvider } from './../../providers/firebase/firebase';
import { CartserviceProvider } from './../../providers/cartservice/cartservice';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Page } from '../../class/Resource/Page';
import { OnesignalProvider } from '../../providers/onesignal/onesignal';


@IonicPage()
@Component({
  selector: 'page-main-menu',
  templateUrl: 'main-menu.html',
})
export class MainMenuPage {

  items_bestseller: any;
  item_promotion: any;

  item_news_banner: any;

  cart_count = 0;

  recommended_item_grid_row = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public firebase: AngularFireDatabase,
    public cartservice: CartserviceProvider,
    public firebaseprovider: FirebaseProvider,
    public onesignal: OnesignalProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuPage');
  }

  ionViewWillEnter () {
    this.cart_count = this.cartservice.Cart_Count();
    this.Init();
  }

  PROFILE_CLICK () {
    this.navCtrl.push(Page.Menu.Profile.Page);
  }

  HOME_CLICK () {

  }

  ORDERMENU_CLICK() {
    this.navCtrl.push(Page.Menu.Order.Page);
  }

  PROMOTIONMENU_CLICK() {
    this.navCtrl.push(Page.Menu.Promotion.Page);
  }

  CHATMENU_CLICK() {
    this.navCtrl.push(Page.Menu.Chat.Contact.Page);
  }

  CHECKPRICEMENU_CLICK() {
    this.navCtrl.push(Page.Menu.Check.Page);
  }

  OPTIONSMENU_CLICK() {
    this.navCtrl.push(Page.Menu.Option.Page);
  }

  ORDERTRACKING_CLICK() {
    this.navCtrl.push(Page.Menu.OrderTracking.Page); 
  }

  CART_CLICK () {
    this.navCtrl.push(Page.Menu.Cart.Page);
  }

  NEWS_CLICK () {
    this.navCtrl.push(Page.Menu.News.Page);
  }

  SELECT_ITEM_CLICK (item) {
    this.navCtrl.push(Page.Menu.Order.Detail.Page, {data: item});
  }

  private Init ()  {
    this.firebaseprovider.Get_Order_Instock_HomePage().then((res) => {
      console.log(res);
      // this.items_bestseller = this.grid_arrange_row(res);
      let recommend = [];
      let promotion = [];
      let maximum = 12;
      if (res != undefined) {
        /*
        for (let i = 0; i < res.length; i++) {
          if (res[i]["promotion"] != undefined) {
            if (res[i]["promotion"]["promotion"] === false) {
              promotion.push(res[i]);
            } else {
              recommend.push(res[i]);
            }
          } else {
            recommend.push(res[i]);
          }
          
        }
        */

        for (let i = 0; i < maximum; i++) {
          if (res[i]["promotion"] != undefined) {
            if (res[i]["promotion"]["promotion"] === false) {
              recommend.push(res[i]);
            }
          } else {
            recommend.push(res[i]);
          }
        }

        for (let i = 0; i < res.length; i++) {
          console.log(res[i]["promotion"]);
          if (res[i]["promotion"] != undefined) {

            if (res[i]["promotion"]["promotion"] === true) {
              promotion.push(res[i]);
            } 
          }
          if (promotion.length > maximum) {
            break;
          }
        }
      }
      this.items_bestseller = this.grid_arrange_row(recommend);
      this.item_promotion = this.grid_arrange_row(promotion);
    });
    /*
    this.firebaseprovider.Get_Promotion_HomePage().then((res) => {
      let tmp = res;
      for (let i = 0; i < tmp.length; i++) {
        tmp[i]['detail'] = this.limit_char(res[i]['detail']);
      }
      this.item_promotion = tmp;
    });
    this.firebaseprovider.Get_News_Or_Promotion(0).then((res) => {
      let tmp = res;
      let length = 0;

      tmp.reverse();
      if (tmp.length > 3) {
        length = 3;
      } else {
        length = tmp.length;
      }
      this.item_news_banner = [];
      for (let i = 0; i < length; i++) {
        this.item_news_banner.push(tmp[i]['image'][0]);
      }
    });
    */
    this.onesignal.subscribe_chat_notification();
    this.onesignal.subscribe_news_and_promotion_notification();
  }

  private limit_char (char: string) {
    let limit = 200;
    let new_string = "";
    for (let i = 0; i < limit; i++) {
      new_string = new_string + char[i];
    }
    new_string = new_string + "...";
    return new_string;
  }

  private grid_arrange_row (data) {
    let each_row_no = 3;
    let length : number = Math.ceil(data.length / each_row_no);
    let current_index = 0;
    let tmp = [];
    for (let i = 0; i < length; i++) {
      let tmp2 = [];
      for (let j = 0; j < each_row_no; j++) {
        if (data[current_index] != undefined) {
          tmp2.push(data[current_index]);
        }
        
        current_index++;
      }
      tmp.push(tmp2);
    }
    return tmp;
  }


}
