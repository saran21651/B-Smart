import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuProfileAddressBookPage } from './main-menu-profile-address-book';

@NgModule({
  declarations: [
    MainMenuProfileAddressBookPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuProfileAddressBookPage),
  ],
})
export class MainMenuProfileAddressBookPageModule {}
