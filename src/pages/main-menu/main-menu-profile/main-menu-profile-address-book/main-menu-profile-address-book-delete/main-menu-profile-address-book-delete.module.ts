import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuProfileAddressBookDeletePage } from './main-menu-profile-address-book-delete';

@NgModule({
  declarations: [
    MainMenuProfileAddressBookDeletePage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuProfileAddressBookDeletePage),
  ],
})
export class MainMenuProfileAddressBookDeletePageModule {}
