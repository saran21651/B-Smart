import { ControllerProvider } from './../../../../../providers/controller/controller';
import { FirebaseProvider } from './../../../../../providers/firebase/firebase';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-main-menu-profile-address-book-delete',
  templateUrl: 'main-menu-profile-address-book-delete.html',
})
export class MainMenuProfileAddressBookDeletePage {

  data: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public firebaseprovider: FirebaseProvider,
    public controller: ControllerProvider
  ) {
    this.data = navParams.get('data');
  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuProfileAddressBookDeletePage');
  }

  DEL_CLICK () {
    this.firebaseprovider.Delete_Address_Book(this.data['data']['key']);
    this.controller.Show_Toast('ทำการลบที่อยู่ สำเร็จ', 'bottom');
    this.viewCtrl.dismiss();
  }

  DISMISS_CLICK () {
    this.viewCtrl.dismiss();
  }

}
