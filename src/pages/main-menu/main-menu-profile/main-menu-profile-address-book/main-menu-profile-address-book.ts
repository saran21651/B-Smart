import { ControllerProvider } from './../../../../providers/controller/controller';
import { CartserviceProvider } from './../../../../providers/cartservice/cartservice';
import { ModalProvider } from './../../../../providers/modal/modal';
import { FirebaseProvider } from './../../../../providers/firebase/firebase';
import { Address_Model } from './../../../../class/Model/Address/address';
import { Page } from './../../../../class/Resource/Page';
import { TempData } from './../../../../class/_Temp_data/tempdata';
import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-main-menu-profile-address-book',
  templateUrl: 'main-menu-profile-address-book.html',
})
export class MainMenuProfileAddressBookPage {

  // address_info : any = TempData.prototype.address_book();
  address_info: any;
  /*
  * Flag
  * 0 : Access from profile page
  * 1 : Access from cart page
  * 2 : Access from preorder page
  */
  page_flag : number = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public zone: NgZone,
    // public modalCtrl: ModalController,
    public firebaseprovider: FirebaseProvider,
    public modal: ModalProvider,
    public cartservice: CartserviceProvider,
    public controller: ControllerProvider
  ) {
    this.zone = new NgZone({enableLongStackTrace: false});
    if (navParams.get('data') === 1) {
      this.page_flag = 1;
    }
    if (navParams.get('data') === 2) {
      this.page_flag = 2;
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuProfileAddressBookPage');
  }

  ionViewWillEnter () {
    console.log('ionViewWillEnter');
    this.Init();
  }

  ionViewDidEnter () {
    console.log('ionViewDidEnter');
    this.Init();
  }

  ADD_CLICK () {
    this.navCtrl.push(Page.Menu.Profile.Address.Add.Page);
  }

  EDIT_CLICK (item) {
    this.navCtrl.push(Page.Menu.Profile.Address.Edit.Page, {data: item});
  }

  DEL_CLICK (item) {
    // this.modal.Open_Modal(Page.Menu.Profile.Address.Modal.Delete, 'inset-modal-addr-del', {data: item});
    this.controller.Show_Alert_Confirm(1, '', item).then((res) => {
      this.Init();
    });
  }

  SELECT_CLICK (item) {
  
    if (this.page_flag == 1) {
      this.cartservice.Add_Address(item);
    }
    if (this.page_flag == 2) {
      this.cartservice.Add_Address_Preorder(item);
    }
    
    this.navCtrl.pop();
  }

  Init () {
  
    this.firebaseprovider.Get_Address_Book().then((res) => {

      this.zone.run(() => {
        this.address_info = res;
      });
    });
  }

  BACK_CLICK () {
    this.back_button();
  }

  private back_button() {
    if (this.navCtrl.length() == 1) {
      let page = "";
      switch(this.page_flag) {
        case 0:
          page = Page.Menu.Profile.Page;
          break;
        case 1:
          page = Page.Menu.Cart.Checkout.Page; 
          break;
        case 2:
          page = Page.Menu.Cart.Checkout_Preorder.Page;
          break;
      }
      this.navCtrl.setRoot(
        page,
        {},
        {
          animate: false,
          direction: "backward"
        }
      );
    } else {
      this.navCtrl.pop();
    }
  }
}
