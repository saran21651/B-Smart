import { ControllerProvider } from "./../../../../../providers/controller/controller";
import { UserServiceProvider } from "./../../../../../providers/user-service/user-service";
import { FirebaseProvider } from "./../../../../../providers/firebase/firebase";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { Address_Model } from "../../../../../class/Model/Address/address";

import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Page } from "../../../../../class/Resource/Page";
@IonicPage()
@Component({
  selector: "page-main-menu-profile-address-book-add",
  templateUrl: "main-menu-profile-address-book-add.html"
})
export class MainMenuProfileAddressBookAddPage {
  data_input: Address_Model = new Address_Model();
  credentialsForm: FormGroup;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public firebaseprovider: FirebaseProvider,
    public userservice: UserServiceProvider,
    public controller: ControllerProvider
  ) {
    this.credentialsForm = this.formBuilder.group({
      fname: ["", Validators.required],
      lname: ["", Validators.required],
      address: ["", Validators.required],
      city: ["", Validators.required],
      province: ["", Validators.required],
      zipcode: ["", Validators.required],
      telephone: ["", Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MainMenuProfileAddressBookAddPage");
  }

  ADD_CLICK() {
    let tmp = new Address_Model();
    tmp = this.credentialsForm.value;
    this.firebaseprovider.Add_Address_Book(tmp).then(() => {
      this.controller.Show_Toast("ทำการเพิ่มที่อยู่ สำเร็จ", "bottom");
      this.navCtrl.pop();
    });
  }

  BACK_CLICK () {
    this.back_button();
  }

  private back_button() {
    if (this.navCtrl.length() == 1) {
      this.navCtrl.setRoot(
        Page.Menu.Profile.Address.Page,
        {},
        {
          animate: false,
          direction: "backward"
        }
      );
    } else {
      this.navCtrl.pop();
    }
  }
}
