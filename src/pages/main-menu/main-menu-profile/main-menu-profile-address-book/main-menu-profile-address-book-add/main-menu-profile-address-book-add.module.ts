import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuProfileAddressBookAddPage } from './main-menu-profile-address-book-add';

@NgModule({
  declarations: [
    MainMenuProfileAddressBookAddPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuProfileAddressBookAddPage),
  ],
})
export class MainMenuProfileAddressBookAddPageModule {}
