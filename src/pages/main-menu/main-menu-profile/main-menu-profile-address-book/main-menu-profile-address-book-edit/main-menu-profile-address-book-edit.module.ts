import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuProfileAddressBookEditPage } from './main-menu-profile-address-book-edit';

@NgModule({
  declarations: [
    MainMenuProfileAddressBookEditPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuProfileAddressBookEditPage),
  ],
})
export class MainMenuProfileAddressBookEditPageModule {}
