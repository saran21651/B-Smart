import { ControllerProvider } from './../../../../../providers/controller/controller';
import { UserServiceProvider } from './../../../../../providers/user-service/user-service';
import { FirebaseProvider } from './../../../../../providers/firebase/firebase';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Address_Model } from '../../../../../class/Model/Address/address';

import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Page } from '../../../../../class/Resource/Page';
@IonicPage()
@Component({
  selector: 'page-main-menu-profile-address-book-edit',
  templateUrl: 'main-menu-profile-address-book-edit.html',
})
export class MainMenuProfileAddressBookEditPage {

  data_input : Address_Model = new Address_Model();
  credentialsForm: FormGroup;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public firebaseprovider: FirebaseProvider,
    public userservice: UserServiceProvider,
    public controller: ControllerProvider
  ) {
    this.data_input = navParams.get('data');
    this.credentialsForm = this.formBuilder.group({
      fname: [this.data_input.fname, Validators.required],
      lname: [this.data_input.lname, Validators.required],
      address: [this.data_input.address, Validators.required],
      city: [this.data_input.city, Validators.required],
      province: [this.data_input.province, Validators.required],
      zipcode: [this.data_input.zipcode, Validators.required],
      telephone: [this.data_input.telephone, Validators.required],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuProfileAddressBookEditPage');
  }

  UPDATE_CLICK () {
    let tmp = new Address_Model();
    tmp = this.credentialsForm.value;
    this.firebaseprovider.Edit_Address_Book(tmp).then(() => {
      this.controller.Show_Toast('ทำการเปลี่ยนแปลงที่อยู่ สำเร็จ', 'bottom');
      this.navCtrl.pop();
    });
  }

  BACK_CLICK () {
    this.back_button();
  }

  private back_button() {
    if (this.navCtrl.length() == 1) {
      this.navCtrl.setRoot(
        Page.Menu.Profile.Address.Page,
        {},
        {
          animate: false,
          direction: "backward"
        }
      );
    } else {
      this.navCtrl.pop();
    }
  }
}
