import { AuthServiceProvider } from './../../../providers/auth-service/auth-service';
import { Storage } from '@ionic/storage';
import { UserInformationModel } from './../../../class/Model/UserData/UserInformation';
import { AlertController } from 'ionic-angular';
import { Page } from './../../../class/Resource/Page';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TempData } from '../../../class/_Temp_data/tempdata';
import { AngularFireDatabase, snapshotChanges } from 'angularfire2/database';
import { User_Storage } from '../../../class/Storage/UserStorage';
import { UserServiceProvider } from '../../../providers/user-service/user-service';


@IonicPage()
@Component({
  selector: 'page-main-menu-profile',
  templateUrl: 'main-menu-profile.html',
})
export class MainMenuProfilePage {

  //info = TempData.prototype.user_data();
  info = UserInformationModel.prototype;

  // storage
  store: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public storage: Storage,
    public firebase: AngularFireDatabase,
    public userservice: UserServiceProvider,
    public authservice: AuthServiceProvider) {
      // this.GetUserInfo();
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuProfilePage');
  }

  ionViewWillEnter () {
    this.GetUserInfo();
  }

  AlertBox () {
    const alertBox = this.alertCtrl.create({
      title: 'ออกจากระบบ',
      message: 'คุณต้องการที่จะออกจากระบบใช่หรือไม่?',
      cssClass: '',
      buttons : [
        {
          text: 'ใช่',
          handler: () => {
            this.LOGOUTCONFIRM_CLICK();
          }
        },
        {
          text: 'ไม่ใช่',
          handler: () => {

          }
        }
      ]
    });
    alertBox.present();
  }

  LOGOUT_CLICK () {
    this.AlertBox();
  }

  LOGOUTCONFIRM_CLICK () {
    this.authservice.logout().then(res => {
      this.navCtrl.setRoot(Page.Authentication.Login.Page);
    }, err => {});
  }

  EDITPROFILE_CLICK () {
    this.navCtrl.push(Page.Menu.Profile.Edit.Page);
  }

  ADDRESS_CLICK () {
    this.navCtrl.push(Page.Menu.Profile.Address.Page);
  }

  CREDIT_CLICK () {
    this.navCtrl.push(Page.Menu.Profile.CreditCard.Page);
  }

  ORDER_CLICK () {
    this.navCtrl.push(Page.Menu.Profile.Order.Page);
  }

  GetUserInfo () {
    this.userservice.get_user_information().then((res) => {
      this.info = res;
    }, (err) => {});
  }

  BACK_CLICK () {
    this.back_button();
  }

  private back_button() {
    if (this.navCtrl.length() == 1) {
      this.navCtrl.setRoot(
        Page.Menu.Profile.Page,
        {},
        {
          animate: false,
          direction: "backward"
        }
      );
    } else {
      this.navCtrl.pop();
    }
  }
  /* Old code
  async GetUserInfo () {
    this.store = new User_Storage(this.storage);
    this.info = await this.store.Get_User_Information();
  }
  */

}
