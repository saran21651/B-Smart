import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Item } from 'ionic-angular';
import { Page } from '../../../../../class/Resource/Page';

@IonicPage()
@Component({
  selector: 'page-mmp-order-history-detail',
  templateUrl: 'mmp-order-history-detail.html',
})
export class MmpOrderHistoryDetailPage {

  items: any;
  type;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {
      this.items = navParams.get('data');
      this.type = navParams.get('type');
      console.log(this.items);
      this.Init();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MmpOrderHistoryDetailPage');
  }

  private Init () {
   
  }

  BACK_CLICK () {
    this.back_button();
  }

  private back_button() {
    if (this.navCtrl.length() == 1) {
      this.navCtrl.setRoot(
        Page.Menu.Profile.Order.Page,
        {},
        {
          animate: false,
          direction: "backward"
        }
      );
    } else {
      this.navCtrl.pop();
    }
  }
}
