import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MmpOrderHistoryDetailPage } from './mmp-order-history-detail';

@NgModule({
  declarations: [
    MmpOrderHistoryDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(MmpOrderHistoryDetailPage),
  ],
})
export class MmpOrderHistoryDetailPageModule {}
