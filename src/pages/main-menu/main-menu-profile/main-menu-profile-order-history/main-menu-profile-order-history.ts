import { FirebaseProvider } from './../../../../providers/firebase/firebase';
import { Page } from './../../../../class/Resource/Page';
import { TempData } from './../../../../class/_Temp_data/tempdata';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-main-menu-profile-order-history',
  templateUrl: 'main-menu-profile-order-history.html',
})
export class MainMenuProfileOrderHistoryPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public firebaseprovider: FirebaseProvider) {
  }

  // items = TempData.prototype.order_tracking_data();
  items;
  items_preorder;

  // Tabs
  category: string = "instock";
  categories = ['instock', 'preorder'];


  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuProfileOrderHistoryPage');
  }

  ionViewWillEnter () {
    this.Init();
  }

  ORDERDETAIL_CLICK (data, type) {
    console.log(type);
    this.navCtrl.push(Page.Menu.Profile.Order.Detail.Page, { data: data, type: type });
  }

  onTabChanged(event) {
    this.category = event.value;
  }

  private Init () {
    this.firebaseprovider.Get_Order_History().then((res) => {
      this.items = res;
    });
    this.firebaseprovider.Get_Preorder_History().then((res) => {
      console.log(res);
      this.items_preorder = res;
    })
  }

  BACK_CLICK () {
    this.back_button();
  }

  private back_button() {
    if (this.navCtrl.length() == 1) {
      this.navCtrl.setRoot(
        Page.Menu.Profile.Page,
        {},
        {
          animate: false,
          direction: "backward"
        }
      );
    } else {
      this.navCtrl.pop();
    }
  }

}
