import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuProfileOrderHistoryPage } from './main-menu-profile-order-history';
import { SwipeSegmentDirective2 } from '../../../../directives/swipe-segment2.directive';

@NgModule({
  declarations: [
    MainMenuProfileOrderHistoryPage,
    SwipeSegmentDirective2
  ],
  imports: [
    IonicPageModule.forChild(MainMenuProfileOrderHistoryPage),
  ],
})
export class MainMenuProfileOrderHistoryPageModule {}
