import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuProfileEditPage } from './main-menu-profile-edit';

@NgModule({
  declarations: [
    MainMenuProfileEditPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuProfileEditPage),
  ],
})
export class MainMenuProfileEditPageModule {}
