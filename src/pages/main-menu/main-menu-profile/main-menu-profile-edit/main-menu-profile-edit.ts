import { AngularFireStorage } from 'angularfire2/storage';
import { UserServiceProvider } from './../../../../providers/user-service/user-service';
import { FirebaseProvider } from './../../../../providers/firebase/firebase';

import { TempData } from './../../../../class/_Temp_data/tempdata';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { UserInformationModel } from '../../../../class/Model/UserData/UserInformation';
import { Page } from '../../../../class/Resource/Page';

@IonicPage()
@Component({
  selector: 'page-main-menu-profile-edit',
  templateUrl: 'main-menu-profile-edit.html',
})
export class MainMenuProfileEditPage {

  // info  = TempData.prototype.user_data();
  info: UserInformationModel = new UserInformationModel();
  img_url: string;

  flags = 0;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public firestorage: AngularFireStorage,
    public firebaseprovider: FirebaseProvider,
    public userservice: UserServiceProvider
  ) {
    // this.init();
    
    this.flags = navParams.get('index');
    userservice.get_user_information().then((res) => {
 
      this.info = res;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuProfileEditPage');
  }

  SAVE_CLICK () {
    this.update_click();
  }

  CHAGE_PROFILE_IMAGE(event) {
    this.change_profile_image(event);
  }

  private async init () {
    /*
    email
    fname
    image
    join_date
    lname
    pname
    uid
    */
    this.info = await this.userservice.get_user_information();
    
  }

  private async change_profile_image (event) {
    if (event == undefined) {
      return;
    }
    let img: any;
    let file_path: any;
    img = event.target.files[0];
    if (img["type"].substring(0, 5) != "image") {
      // this.alertBox("รูปแบบไฟล์ไม่ถูกต้อง");
      return;
    }
    file_path =
      "image_user/" + "/" + this.info['uid'] + "/" + "profile";
    await this.firestorage.storage.ref(file_path).put(img);
    let fileUrl = await this.firestorage.storage
      .ref(file_path)
      .getDownloadURL();
    this.info['image'] = fileUrl;
  }

  private update_click () {
    this.firebaseprovider.Update_Profile(this.info).then(() => {
      this.userservice.set_user_information(this.info);
      if (this.flags == 1) {
        this.navCtrl.setRoot(
          Page.Menu.Page,{}, {
            animate: true,
            direction: "forward"
          }
        );
      } else {
        this.navCtrl.pop();
      }
    });
  }

  IsEmpty () : boolean {
    let tmp1 = this.CheckNull(this.info['fname']);
    let tmp2 = this.CheckNull(this.info['lname']);
    let tmp3 = this.CheckNull(this.info['pname']);
    // let tmp4 = this.CheckNull(this.info['line']);

    if ((tmp1 || tmp2 || tmp3 /*|| tmp4*/) == true) {
      return true;
    } else {
      return false;
    }
  }

  CheckNull (data) {
    if (data == "" || data == null) {
      return true;
    } else {
      return false;
    }
  }

  BACK_CLICK () {
    this.back_button();
  }

  private back_button() {
    if (this.navCtrl.length() == 1) {
      this.navCtrl.setRoot(
        Page.Menu.Profile.Page,
        {},
        {
          animate: false,
          direction: "backward"
        }
      );
    } else {
      this.navCtrl.pop();
    }
  }
}
