import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuProfileCreditCardAddPage } from './main-menu-profile-credit-card-add';

@NgModule({
  declarations: [
    MainMenuProfileCreditCardAddPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuProfileCreditCardAddPage),
  ],
})
export class MainMenuProfileCreditCardAddPageModule {}
