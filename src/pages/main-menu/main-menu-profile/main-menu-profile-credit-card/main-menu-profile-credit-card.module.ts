import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuProfileCreditCardPage } from './main-menu-profile-credit-card';

@NgModule({
  declarations: [
    MainMenuProfileCreditCardPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuProfileCreditCardPage),
  ],
})
export class MainMenuProfileCreditCardPageModule {}
