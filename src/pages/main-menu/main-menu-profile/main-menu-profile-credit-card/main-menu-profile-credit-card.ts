import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { TempData } from '../../../../class/_Temp_data/tempdata';
import { Page } from '../../../../class/Resource/Page';

@IonicPage()
@Component({
  selector: 'page-main-menu-profile-credit-card',
  templateUrl: 'main-menu-profile-credit-card.html',
})
export class MainMenuProfileCreditCardPage {

  credit_info = TempData.prototype.address_credit_card();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuProfileCreditCardPage');
  }

  alertBox() {

  }

  ADD_CLICK (data) {
    this.navCtrl.push(Page.Menu.Profile.CreditCard.Add.Page, data);
  }

  EDIT_CLICK (data) {
    this.navCtrl.push(Page.Menu.Profile.CreditCard.Edit.Page, data);
  }

  DEL_CLICK () {

  }

}
