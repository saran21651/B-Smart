import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuProfileCreditCardEditPage } from './main-menu-profile-credit-card-edit';

@NgModule({
  declarations: [
    MainMenuProfileCreditCardEditPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuProfileCreditCardEditPage),
  ],
})
export class MainMenuProfileCreditCardEditPageModule {}
