import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuProfilePage } from './main-menu-profile';

@NgModule({
  declarations: [
    MainMenuProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuProfilePage),
  ],
})
export class MainMenuProfilePageModule {}
