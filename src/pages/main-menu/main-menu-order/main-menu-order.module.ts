import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuOrderPage } from './main-menu-order';

@NgModule({
  declarations: [
    MainMenuOrderPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuOrderPage),
  ],
})
export class MainMenuOrderPageModule {}
