import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuOrderTabsPage } from './main-menu-order-tabs';

@NgModule({
  declarations: [
    MainMenuOrderTabsPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuOrderTabsPage),
  ]
})
export class MainMenuOrderTabsPageModule {}
