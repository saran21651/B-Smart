import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuOrderPreorderPage } from './main-menu-order-preorder';

@NgModule({
  declarations: [
    MainMenuOrderPreorderPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuOrderPreorderPage),
  ],
})
export class MainMenuOrderPreorderPageModule {}
