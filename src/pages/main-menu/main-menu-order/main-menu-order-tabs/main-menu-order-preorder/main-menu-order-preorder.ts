import { DatacarrierProvider } from "./../../../../../providers/datacarrier/datacarrier";
import { Component, ApplicationRef } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { Page } from "../../../../../class/Resource/Page";
import { ChatServiceProvider } from "../../../../../providers/chat-service/chat-service";
import { MessageChat } from "../../../../../class/Resource/StringList";
import { App } from "ionic-angular/components/app/app";
import { FirebaseProvider } from "../../../../../providers/firebase/firebase";

@IonicPage()
@Component({
  selector: "page-main-menu-order-preorder",
  templateUrl: "main-menu-order-preorder.html"
})
export class MainMenuOrderPreorderPage {
  temps;
  items = [];

  initial_load = 10;
  current_load = 0;
  max_load = 0;

  category_select: any = "init";
  category_dropdown: any;

  chat_info: any;
  chat_type = MessageChat.ChatType.CUSTOMER_TO_SALE_PREORDER;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public dataservice: DatacarrierProvider,
    public chatservice: ChatServiceProvider,
    public firebaseprovider: FirebaseProvider,
    public app: App
  ) {
    //let debug_order = 'order-1524984546847946';
    //let debug_sale_id = 'UHF7tFUaUegquSDOcggMavc5jtN2';
    let item = {
      order_id: "", //debug_order,
      uid: "" //debug_sale_id
    };
    this.chat_info = item;

    this.chat_info["chat_type"] = this.chat_type;
    this.Init();
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MainMenuOrderPreorderPage");
  }

  DETAIL_CLICK(data) {
    /*
      this.dataservice.Set_Order_Tabs_Data(data);
      this.navCtrl.parent.select(2);
    */
    // this.call_chat(data);
    this.chat_select(data);
  }

  CHAT_CLICK(order_data) {
    this.chat_select(order_data);
  }

  private Init() {
    this.firebaseprovider.Get_Categories().then(res => {
      this.category_dropdown = res;
    });
    this.firebaseprovider
      .Get_Order_Preorder_Revamp(this.initial_load, this.initial_load)
      .then(val => {
        this.temps = val;
        this.Arrange_Grid_Layout();
        this.current_load = this.initial_load;
      });
  }

  Arrange_Grid_Layout() {
    this.items = [];
    for (let i = 0; i < this.temps.length; i++) {
      this.items.push({
        a1: this.temps[i],
        a2: this.temps[i + 1]
      });
    }
  }

  doRefresh(event) {
    setTimeout(() => {
      if (this.category_select == "init") {
        this.firebaseprovider
          .Get_Order_Preorder_Revamp(this.current_load, this.current_load + 2)
          .then(res => {
            res.forEach(element => {
              this.temps.push(element);
            });
            this.Arrange_Grid_Layout();
            this.current_load += 2;
          });
      } else {
        this.firebaseprovider
          .Get_Order_Preorder_Revamp(
            this.current_load,
            this.current_load + 2,
            this.category_select["category_id"]
          )
          .then(res => {
            res.forEach(element => {
              this.temps.push(element);
            });
            this.Arrange_Grid_Layout();
            this.current_load += 2;
          });
      }

      event.complete();
    }, 1000);
  }

  onChanged(event) {
    /*
      // category_id
      // category_name
    */
    this.current_load = this.initial_load;
    if (event == "init") {
      this.firebaseprovider
        .Get_Order_Preorder_Revamp(this.initial_load, this.initial_load)
        .then(res => {
          this.temps = res;
          this.Arrange_Grid_Layout();
          this.current_load = this.initial_load;
        });
    } else {
      this.firebaseprovider
        .Get_Order_Preorder_Revamp(
          this.initial_load,
          this.initial_load,
          event["category_id"]
        )
        .then(res => {
          this.temps = res;
          this.Arrange_Grid_Layout();
          this.current_load = this.initial_load;
        });
    }
  }

  private async chat_select(item) {
    console.log(item);
    this.app.getRootNav().setRoot(Page.Menu.Chat.Preorder_List.Page, {data: item}, {
      animate: true,
      direction: "forward"
    });
    // this.app.getRootNav().setRoot(Page.Menu.Chat.Preorder_List.Page);
    /*
    this.chat_info["uid"] = await this.chatservice.Fetch_All_Contact();
    this.chat_info["uid"] = "UHF7tFUaUegquSDOcggMavc5jtN2";

    let info = this.chat_info;
    console.log(item);
    this.chatservice.Create_ChatRoom_Preorder(info, item).then(res => {
      let data = res;

      this.app.getRootNav().setRoot(Page.Menu.Chat.Page, data, {
        animate: true,
        direction: "forward"
      });
    });
    */
  }

  private call_chat(data) {
    // let line = "https://line.me/ti/p/~" + String("sadfdsfsf");
    // window.open(line, "_system");
  }
}
