import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuOrderInstockPage } from './main-menu-order-instock';

@NgModule({
  declarations: [
    MainMenuOrderInstockPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuOrderInstockPage),
  ],
})
export class MainMenuOrderInstockPageModule {}
