import { Page } from './../../../../../class/Resource/Page';
import { ModalProvider } from './../../../../../providers/modal/modal';
import { FirebaseProvider } from './../../../../../providers/firebase/firebase';
import { DatacarrierProvider } from './../../../../../providers/datacarrier/datacarrier';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-main-menu-order-instock',
  templateUrl: 'main-menu-order-instock.html',
})
export class MainMenuOrderInstockPage {

  // temp = TempData.prototype.order_instock();
  temp: any;
  items: any = [];

  initial_load = 10;
  current_load = 0;
  max_load = 0;

  category_select: any = "init";
  category_dropdown: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public dataservice: DatacarrierProvider,
    public firebaseProvider: FirebaseProvider,
    public modal: ModalProvider
  ) {
    this.Init();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuOrderInstockPage');
  }

  Init () {
    this.firebaseProvider.Get_Categories().then((res) => {
      this.category_dropdown = res;
    });
   this.firebaseProvider.Get_Order_Instock_Revamp(this.initial_load, this.initial_load).then((res) => {
      this.temp = res;
      this.Arrange_Grid_Layout ();
      this.current_load = this.initial_load;
   });
  }

  Arrange_Grid_Layout () {
    this.items = [];
    for (let i = 0; i < this.temp.length; i++) {

      this.items.push(
        {
          a1: this.temp[i],
          a2: this.temp[i + 1]
        }
      );
    }
    console.log(this.items);
  }

  DETAIL_CLICK (data) {
    this.navCtrl.push(Page.Menu.Order.Detail.Page, {data:data});
  }

  doRefresh (event) {
    
    setTimeout(() => {
      if (this.category_select == "init") {
        this.firebaseProvider.Get_Order_Instock_Revamp(this.current_load, this.current_load + 2).then((res) => {
          res.forEach(element => {
            this.temp.push(element)
          });
          this.Arrange_Grid_Layout ();
          this.current_load += 2;
        });
      } else {
        this.firebaseProvider.Get_Order_Instock_Revamp(this.current_load, this.current_load + 2, this.category_select['category_id']).then((res) => {
          res.forEach(element => {
            this.temp.push(element)
          });
          this.Arrange_Grid_Layout ();
          this.current_load += 2;
        });
      }
      
      event.complete();
    }, 1000);
  }

  onChanged(event) {
    /*
      // category_id
      // category_name
    */
    this.current_load = this.initial_load;
    if (event == "init") {
      this.firebaseProvider.Get_Order_Instock_Revamp(this.initial_load, this.initial_load).then((res) => {
        this.temp = res;
        this.Arrange_Grid_Layout ();
        this.current_load = this.initial_load;
      });
    } else {
      this.firebaseProvider.Get_Order_Instock_Revamp(this.initial_load, this.initial_load, event['category_id']).then((res) => {
        this.temp = res;
        this.Arrange_Grid_Layout ();
        this.current_load = this.initial_load;
      });
    }
  }
}
