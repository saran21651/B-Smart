import { CartserviceProvider } from './../../../../providers/cartservice/cartservice';
import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Page } from '../../../../class/Resource/Page';

@IonicPage()
@Component({
  selector: 'page-main-menu-order-tabs',
  templateUrl: 'main-menu-order-tabs.html'
})
export class MainMenuOrderTabsPage {

  mainMenuOrderPreorderRoot = 'MainMenuOrderPreorderPage'
  mainMenuOrderInstockRoot = 'MainMenuOrderInstockPage'
  mainMenuOrderDetailRoot = 'MainMenuOrderDetailPage'

  cart_count;

  constructor(
    public navCtrl: NavController,
    public cart: CartserviceProvider
  ) {
  }

  CART_CLICK () {
    this.navCtrl.push(Page.Menu.Cart.Page);
  }

  ionViewWillEnter () {
    this.cart_count = this.cart.Cart_Count();
  }

}
