import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuOrderDetailPage } from './main-menu-order-detail';

@NgModule({
  declarations: [
    MainMenuOrderDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuOrderDetailPage),
  ],
})
export class MainMenuOrderDetailPageModule {}
