import { Page } from "./../../../../../class/Resource/Page";
import { ModalProvider } from "./../../../../../providers/modal/modal";
import { DatacarrierProvider } from "./../../../../../providers/datacarrier/datacarrier";
import { Component, ViewChild } from "@angular/core";
import { IonicPage, NavController, NavParams, Slides } from "ionic-angular";
import { ArrayChecker } from "../../../../../class/Util/ArrayChecker";
import { FirebaseProvider } from "../../../../../providers/firebase/firebase";
import { CartserviceProvider } from "../../../../../providers/cartservice/cartservice";
import { UserServiceProvider } from "../../../../../providers/user-service/user-service";
import { Datetime } from "../../../../../class/Util/Datetime";
import { AngularFireStorage } from "angularfire2/storage";
import { App } from "ionic-angular/components/app/app";
import { ControllerProvider } from "../../../../../providers/controller/controller";

@IonicPage()
@Component({
  selector: "page-main-menu-order-detail",
  templateUrl: "main-menu-order-detail.html"
})
export class MainMenuOrderDetailPage {
  @ViewChild("slider") slider: Slides;
  @ViewChild("")
  slideIndex = 0;
  data: any;
  imgc: any = [];

  category_options: any;
  category_option_select: any;

  option_id: any;
  option_id_select: any;

  option: any;
  option_select: any = [];

  option_obj: any;

  sum_total = 0;
  product_amount = 1;
  design_pattern;
  design_pattern_display;

  can_input_product_amount: boolean = false; // if product_unit_price == 1 (true)

  is_design_check: boolean = false;
  is_image_load: boolean = false;

  is_checkout_ready: boolean = false;

  // Hack!!! Workaround for load view
  IsAlreadyLoadPage: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public dataservice: DatacarrierProvider,
    public modal: ModalProvider,
    public firebaseprovider: FirebaseProvider,
    public cartservice: CartserviceProvider,
    public userservice: UserServiceProvider,
    public firestorage: AngularFireStorage,
    public app: App,
    public controller: ControllerProvider
  ) {
    // this.data = navParams.get('data');
    //this.init();
  
    this.init(this.navParams.get("data"));
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MainMenuOrderDetailPage");
  }

  ionViewWillEnter() {
    //
    console.log("ionViewWillEnter MainMenuOrderDetailPage");
  }

  ionViewDidEnter() {
    console.log("ionViewDidEnter MainMenuOrderDetailPage");
  }

  onSlideChanged() {
    this.slideIndex = this.slider.getActiveIndex();
  }

  ADD_TO_CART_CLICK() {
    this.controller.Show_Loading();
    this.add_to_cart();
  }

  DISMISS_CLICK() {
    this.navCtrl.pop();
  }

  async init(data) {
    this.data_change(data);

    this.firebaseprovider
      .Get_Category_Options(data["category_id"])
      .then(res => {
        console.log(res);
        this.category_options = res;
      });

    console.log(data);
    this.firebaseprovider.Get_Option_By_Option_ID (data["option_id"]).then(res => {
      console.log(res);
      // this.option_obj = res;
    });
    this.firebaseprovider.Get_Options_and_Categorie (data["category_id"], data["option_id"]).then(res => {
      console.log(res);
      this.option_obj = res;
    });

    this.option = data["options"];
    console.log(this.option);
  }

  data_change(data) {
    this.data = data;
    this.imgc = [];

    let arr_checker = ArrayChecker.prototype.IsArray(
      this.data["product_image"]
    );

    if (arr_checker) {
      this.data["product_image"].forEach(val => {
        this.imgc.push({ img_collection: val });
      });
    } else {
      this.imgc.push({ img_collection: this.data["product_image"] });
    }

    if (Number(data["product_unit_price"]) == 1) {
      this.can_input_product_amount = true;
    }

    this.calculate_total();
  }

  onChanged(event) {
    this.firebaseprovider
      .Get_Order_Instock_By_Option_Id(event["option_id"])
      .then(res => {
        console.log(res);
        this.option_id = res;
      });
  }

  onChanged2(event) {
    if (event != undefined) {
      this.option = event["options"];

      for (let i = 0; i < this.option.length; i++) {
        this.option[i]["check"] = false;
      }
      this.data_change(event);
    }
  }

  onChanged3(event) {
    this.calculate_total();
  }

  onChanged_amount(event) {
    this.calculate_total();
  }

  show_extra_option(): boolean {
    // TODO: Fixed bug
    return true;
    /*
    if (
      this.option_id_select == undefined ||
      this.category_option_select == undefined
    ) {
      return false;
    } else {
      return true;
    }
    */
  }

  calculate_total() {
    console.log("Calculate");
    console.log(this.can_input_product_amount);
    if (this.can_input_product_amount) {
      console.log(Number(this.product_amount));
      if (Number(this.product_amount) > 0) {
        this.calculate_option();
      } else {
        this.sum_total = 0;
      }
    } else {
      this.calculate_option();
    }
  }

  calculate_option() {
    if (this.option == undefined) {
      if (this.data["promotion"] != undefined)
      {
        console.log();
        this.sum_total = (this.data["promotion"]["promotion"] == true ? Number(this.data["promotion"]["product_price"]["price"]) : Number(this.data["product_price"]["price"])) * this.product_amount;
      }
      else 
      {
        this.sum_total = Number(this.data["product_price"]["price"]) * this.product_amount;
      }

      return;
    }
    let price = 0;

    for (let i = 0; i < this.option.length; i++) {
      if (this.option[i]["check"] == true) {
        price += Number(this.option[i]["option_filter_price"]);
      }
    }

    if (this.data["promotion"] != undefined) {
      let p_price = (this.data["promotion"]["promotion"] == true ? Number(this.data["promotion"]["product_price"]["price"]) : Number(this.data["product_price"]["price"]));
      if (this.can_input_product_amount) {
        this.sum_total = (price + p_price) * this.product_amount;
      } else {
        this.sum_total = price + p_price;
      }
    } else {
      let p_price = Number(this.data["product_price"]["price"]);
      if (this.can_input_product_amount) {
        this.sum_total = (price + p_price) * this.product_amount;
      } else {
        this.sum_total = price + p_price;
      }
    }
    

    if (!this.is_design_check) {
      // this.sum_total += this.get_design_pattern_price();
    }
  }

  async add_to_cart() {
    // Serious bug
    // this.data["category_id"] = this.category_option_select;
  
    let product = this.data;
    let option_select = [];

    if (this.option != undefined) {
      for (let i = 0; i < this.option.length; i++) {
        if (this.option[i]["check"] == true) {
          option_select.push(this.option[i]);
        }
      }
    } else {
      option_select.push([]);
    }
    product["options"] = option_select;
    product["price"] = {
      product_price: this.sum_total,
      design_price: this.get_design_pattern_price(),
      promotion_price: 0,
      total_price: 0
    };

    product["design_pattern"] = {
      present: this.is_design_check,
      file: (await this.upload_image_to_firebase()) || null
    };
    this.cartservice.Add_To_Cart(product, this.product_amount, 2);
      
    
    this.app.getRootNav().setRoot(Page.Menu.Cart.Page, {}, {
      animate: true,
      direction: 'forward'
    }).then(() => {
      this.controller.Hide_Loading();
    });
    
  }

  uploadPhoto() {
    document.getElementById("file").click();
  }

  async change_pattern_image(event) {
    if (event == undefined) {
      return;
    }

    let reader = new FileReader();
    reader.onload = (event1: any) => {
      this.design_pattern_display = event1.target.result;
      this.design_pattern = event.target.files[0];
    };
    reader.readAsDataURL(event.target.files[0]);
  }

  is_desing_pattern_check() {
    return this.is_design_check;
  }

  calculate_design_pattern() {
    this.calculate_total();
  }

  get_design_pattern_price(): number {
    if (this.is_design_check == false) {
      return 100;
    } else {
      return 0;
    }
  }

  load_image() {
    this.is_image_load = !this.is_image_load;
  }

  async upload_image_to_firebase() {
    return new Promise<string>(async (resolve, reject) => {
      let uid = await this.userservice.get_user_id();
      let file_path =
        "design_pattern/" + uid + "/" + Datetime.getTimeInMilliseconds();
      if (this.design_pattern == undefined) {
        resolve(null)
      } else {
        await this.firestorage.storage.ref(file_path).put(this.design_pattern);
        let url = await this.firestorage.storage
          .ref(file_path)
          .getDownloadURL();
        resolve(url);
      }
    });
  }

  checkout_ready () {
    // TODO: Fix some but
    return true;
    /*
    if (this.category_option_select == undefined || this.option_id_select == undefined) {
      return false;
    } else {
      return true;
    }
    */
  }
}
