import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuOrderModalPage } from './main-menu-order-modal';

@NgModule({
  declarations: [
    MainMenuOrderModalPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuOrderModalPage),
  ],
})
export class MainMenuOrderModalPageModule {}
