import { AngularFireStorage } from 'angularfire2/storage';
import { Page } from "./../../../../../class/Resource/Page";
import { CartserviceProvider } from "./../../../../../providers/cartservice/cartservice";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import { Product_Model } from "../../../../../class/Model/Order/Product";
import { UserServiceProvider } from '../../../../../providers/user-service/user-service';
import { Datetime } from '../../../../../class/Util/Datetime';

@IonicPage()
@Component({
  selector: "page-main-menu-order-modal",
  templateUrl: "main-menu-order-modal.html"
})
export class MainMenuOrderModalPage {
  product_info: any;
  amount: number = 1;

  isarray = false;
  size_list: any = [];
  count_list: any = [];

  isimageloading = false; // Temp image for loading

  selected_1 = "init";
  selected_2 = "init";
  selected_size = "";
  selected_count = "";
  selected_info = {
    count: 0,
    size: "",
    price: 0,
    currency: ""
  };
  disable_dropdown = true;

  // Dynamic List
  product_info_list: Array<any>;
  array_list: Array<any>;
  array_list_price_fix : Array<boolean>;
  array_list_selected : Array<any>;
  array_list_id: Array<any>;
  array_list_option_name: Array<any>;
  sum_total = 0;

  // checkbox
  checkbox = false;
  design_pattern_img : any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public cartservice: CartserviceProvider,
    public firestorage: AngularFireStorage,
    public userservice: UserServiceProvider
  ) {
    this.product_info = this.navParams.get("data");
    // this.amount = 0;
    this.Init();
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MainMenuOrderModalPage");
  }

  DISMISS_CLICK() {
    this.viewCtrl.dismiss();
  }

  ADD_TO_CART_CLICK () {
    this.add_to_cart();
  }

  private Init() {
    let list = this.product_info["options"];
    this.array_list = new Array<any>();
    this.array_list_price_fix = new Array<boolean>();
    let length = list.length;
    for (let i = 0; i < length; i++) {
      let price_fix: Array<boolean> = new Array<boolean>();
      list[i]['list_option'].forEach((value) => {
        price_fix.push(value["price_fix"]);
      });
      let bool = price_fix.find(res => res == false);
      if (bool != undefined) {
        this.array_list.push("init");
        this.array_list.push("init");
        this.array_list_price_fix.push(false);
      } else {
        this.array_list.push("init");
        this.array_list_price_fix.push(true);
      }
    }

    this.product_info_list = new Array<any>();
    this.array_list_option_name = new Array<any>();
    for (let i = 0; i < this.array_list_price_fix.length; i++) {
      if (this.array_list_price_fix[i] == false) {
        this.product_info_list.push(this.product_info["options"][i]);
        this.array_list_option_name.push(this.product_info["options"][i]["option_name"]);
        let item = {
          option_name: "จำนวนหน้า",
          list_option : list[i]['list_option'][0]["rate"]
        }
        this.product_info_list.push(item);
        this.array_list_option_name.push(item["option_name"]);
      } else {
        this.product_info_list.push(list[i]);
        this.array_list_option_name.push(this.product_info["options"][i]["option_name"]);
      }
    }
    this.array_list_selected = new Array<any>();
    this.array_list_id = new Array<any>();
    for (let i = 0; i < this.product_info_list.length; i++) {
      this.array_list_selected.push("init");
      this.array_list_id.push("init");
    }
  }

  IsEmpty(): boolean {
    if (this.amount == 0) {
      return true;
    } else {
      let unselected = this.array_list.find(res => res === "init");
      if (unselected) {
        return true;
      } else {
        return false;
      }
    }
  }

  IsArray(data): boolean {
    return Object.prototype.toString.call(data) === "[object Array]";
  }

  onChanged(event, i) {
    console.log(this.array_list);
    this.calculate_total();
  }

  checkDesignPattern () {
    console.log(this.checkbox);
    this.calculate_total();
  }

  CHANGE_PATTERN_IMAGE (event) {
    this.change_pattern_image(event);
  }

  IsDesignPatternCheck () {
    return this.checkbox;
  }

  Return_Design_Pattern_Price () : number {
    if (this.checkbox == false) {
      return 100;
    } else {
      return 0;
    }
  }

  calculate_total() {
   
    // New Version
    if (Number(this.amount) > 0) {
      this.calculate_option();
    } else {
      this.sum_total = 0;
    }

  }

  calculate_type() {

    let result = [];
    let sum = 0;
    for (let i = 0; i < this.array_list_selected.length; i++) {
      if (this.array_list_selected[i] == "main" && this.array_list_selected[i+1] == "sub") {
        result.push(this.find_price_range(this.array_list[i+1]['price']));
        i++;
      }
    }
    for (let i = 0; i < result.length; i++) {
      sum = sum + result[i];
    }
    return sum;
  }

  find_price_range(data) {
  
    let price = 0;
    let find =false;
    data.forEach(child => {
      console.log(child);
      if (find == false) {
        let opt_start = Number(child["start"]);
        let opt_end = Number(child["end"]);
        if (isNaN(opt_end)) {
          price = Number(child["price"]);
        } else {
          if (opt_start <= Number(this.amount) && Number(this.amount) <= opt_end) {
            price = Number(child["price"]);
            find = true;
          }
        }
      }
    });
    return price;
  }

  calculate_option () {
    let result = 0;
    let price = [];
    for (let i = 0; i < this.array_list.length; i++) {
      if (this.array_list[i] != "init") {
        if (this.array_list[i]["price_fix"] == false ||
              this.array_list[i]["price_fix"] == undefined) {
          let value = "";
          if (this.array_list[i]["price_fix"] == false) {
            value = "main";
          } else if (this.array_list[i]["price_fix"] == undefined) {
            value = "sub";
          }
          this.array_list_selected[i] = value;
        } else {
          price.push(this.array_list[i]["rate"] * this.amount);
        }
        this.array_list_id[i] = this.array_list[i]["id"];
      } else {
        if (this.array_list[i]["price_fix"] == false ||
              this.array_list[i]["price_fix"] == undefined) {
          this.array_list_selected[i] = "init";
        }
        price.push(0);
      }
    }

    for (let i = 0; i < price.length; i++) {
      result = result + price[i];
    }

    this.sum_total = result + this.calculate_type() + this.Return_Design_Pattern_Price();
  }

  add_to_cart () {

    let product = this.product_info;
    let i = 0;
    console.log(this.product_info);
    this.array_list.forEach((item) => {
      this.array_list[i]['option_name'] = this.array_list_option_name[i]//this.product_info['options'][i]['option_name'];
      i++;
    });
    console.log(this.array_list);

    product['design_pattern'] = {
      present: this.checkbox,
      file: this.design_pattern_img || null
    };

    product['options'] = this.array_list;
    product['price'] = {
      product_price: this.sum_total,
      design_price: this.Return_Design_Pattern_Price(),
      promotion_price: 0,
      total_price: 0
    }
    product['product_code'] = this.get_product_code("BSC",this.array_list_id);
    console.log(this.get_product_code("BSC",this.array_list));
    this.cartservice.Add_To_Cart(product, this.amount, 2);

    this.navCtrl.setRoot(Page.Menu.Cart.Page, {}, {
      animate: true,
      direction: 'forward'
    });

  }

  get_product_code (code,arr) {
    let array = arr;
    let result = "";
    console.log(array);
    array.forEach((value) => {
      console.log(value);
      result = result + value;
    });
    console.log(result);
    return code + "-" + result;
  }

  async change_pattern_image(event) {
    // TODO : Insert LOading Here
    if (event == undefined) {
      return;
    }
    let img: any;
    let file_path: any;
    img = event.target.files[0];
    if (img['type'].substring(0, 5) != "image") {
      return;
    }
    this.load_image();
    let uid = await this.userservice.get_user_id();
    file_path =
      "design_pattern/" + uid + "/" + Datetime.getTimeInMilliseconds();
    await this.firestorage.storage.ref(file_path).put(img);
    let fileUrl = await this.firestorage.storage
      .ref(file_path)
      .getDownloadURL();
      this.load_image();
    this.design_pattern_img = fileUrl;
  }

  load_image () {
    this.isimageloading = !this.isimageloading;
  }
}
