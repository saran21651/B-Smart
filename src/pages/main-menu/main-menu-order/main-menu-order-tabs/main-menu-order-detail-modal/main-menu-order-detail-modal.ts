import { Page } from './../../../../../class/Resource/Page';
import { ModalProvider } from "./../../../../../providers/modal/modal";
import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  Slides
} from "ionic-angular";

@IonicPage()
@Component({
  selector: "page-main-menu-order-detail-modal",
  templateUrl: "main-menu-order-detail-modal.html"
})
export class MainMenuOrderDetailModalPage {

  @ViewChild("slider") slider: Slides;
  slideIndex = 0;
  data: any;
  imgc: any = [];


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modal: ModalProvider,
    public viewCtrl: ViewController
  ) {
    this.init(this.navParams.get('data'));
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MainMenuOrderDetailModalPage");
  }

  onSlideChanged() {
    this.slideIndex = this.slider.getActiveIndex();
  }

  ADD_TO_CART_CLICK() {
    this.modal.Open_Modal(
      Page.Menu.Order.Tabs.Modal,
      "inset-modal-order-modal",
      this.data
    );
    //this.navCtrl.push(Page.Menu.Cart.Page);
  }

  DISMISS_CLICK() {
    this.viewCtrl.dismiss();
  }

  async init(data) {
    this.data = data;
    this.imgc.push({ img_collection: this.data["product_image"] });
    this.imgc.push({ img_collection: "" });
    this.imgc.push({ img_collection: "" });

  }
}
