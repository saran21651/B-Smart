import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuOrderDetailModalPage } from './main-menu-order-detail-modal';

@NgModule({
  declarations: [
    MainMenuOrderDetailModalPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuOrderDetailModalPage),
  ],
})
export class MainMenuOrderDetailModalPageModule {}
