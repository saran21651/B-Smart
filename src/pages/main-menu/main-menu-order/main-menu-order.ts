import { GlobalString } from '../../../class/Resource/StringList';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { TempData } from '../../../class/_Temp_data/tempdata';
import { Page } from '../../../class/Resource/Page';

@IonicPage()
@Component({
  selector: 'page-main-menu-order',
  templateUrl: 'main-menu-order.html',
})
export class MainMenuOrderPage {

  items = TempData.prototype.order_data();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuOrderPage');
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true, GlobalString.SIDEMENU.MENU_AVATAR);
  }

  ionViewWillLeave() {
    this.menuCtrl.enable(false, GlobalString.SIDEMENU.MENU_AVATAR);
  }

  GOTOORDERSUBPAGE_CLICK (data) {
    //this.navCtrl.setRoot(Page.Menu.Order.Sub.Page, data);
  }

}
