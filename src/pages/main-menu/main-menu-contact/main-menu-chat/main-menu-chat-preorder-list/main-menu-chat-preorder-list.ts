import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { ChatServiceProvider } from '../../../../../providers/chat-service/chat-service';
import { MessageChat } from '../../../../../class/Resource/StringList';
import { Page } from '../../../../../class/Resource/Page';

@IonicPage()
@Component({
  selector: 'page-main-menu-chat-preorder-list',
  templateUrl: 'main-menu-chat-preorder-list.html',
})
export class MainMenuChatPreorderListPage {

  product_info: any;

  contact_list = [];

  chat_info: any;
  chat_type = MessageChat.ChatType.CUSTOMER_TO_SALE_PREORDER;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public chat: ChatServiceProvider,
    public zone: NgZone,
    public app: App
    ) {
      this.zone = new NgZone({ enableLongStackTrace: false});
      this.product_info = navParams.get("data");
      this.Init();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuChatPreorderListPage');
  }

  ionViewWillLeave () {
    this.chat.Unsubscribe_Event(this.chat.EVENT.CONTACT_LIST_PREORDER);
  }

  BACK_CLICK() {
    this.back_button();
  }

  CHAT_CLICK (item) {
    console.log(item);
    let chat_info = {
      uid: item['uid'],
      chat_type: this.chat_type,
      order_id: this.product_info['product_id']
    };
    this.chat.Create_ChatRoom_Preorder(chat_info, this.product_info).then(res => {
      let data = res;
      this.app.getRootNav().setRoot(Page.Menu.Chat.Page, data, {
        animate: true,
        direction: "forward"
      });
    });
  }

  private Init () {
    this.chat.Load_Contact_List_Preorder_By_Category_Id(this.product_info["category_id"]).subscribe(this.chat.EVENT.CONTACT_LIST_PREORDER, (res) => {
      this.zone.run(() => {
        this.contact_list = res;
      });
   
    });
  }

  private back_button () {
    console.log("Length: " + this.navCtrl.length());
    if (this.navCtrl.length() == 1) {
      this.navCtrl.setRoot(
        Page.Menu.Order.Page,
        {},
        {
          animate: false,
          direction: "backward"
        }
      );
    } else {
      this.navCtrl.pop();
    }
  }
}
