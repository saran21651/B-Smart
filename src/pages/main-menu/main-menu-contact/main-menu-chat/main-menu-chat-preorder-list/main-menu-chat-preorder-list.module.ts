import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuChatPreorderListPage } from './main-menu-chat-preorder-list';

@NgModule({
  declarations: [
    MainMenuChatPreorderListPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuChatPreorderListPage),
  ],
})
export class MainMenuChatPreorderListPageModule {}
