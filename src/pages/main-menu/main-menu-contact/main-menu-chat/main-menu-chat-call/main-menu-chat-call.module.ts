import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuChatCallPage } from './main-menu-chat-call';

@NgModule({
  declarations: [
    MainMenuChatCallPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuChatCallPage),
  ],
})
export class MainMenuChatCallPageModule {}
