import { CallNumber } from '@ionic-native/call-number';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FirebaseProvider } from '../../../../../providers/firebase/firebase';

@IonicPage()
@Component({
  selector: 'page-main-menu-chat-call',
  templateUrl: 'main-menu-chat-call.html',
})
export class MainMenuChatCallPage {

  call_number: any;
  call_number_display: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public call: CallNumber,
    public firebaseprovider: FirebaseProvider) {
      let data = this.navParams.get('data');
      this.Init(data);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuChatCallPage');
  }

  DISMISS_CLICK () {
    this.viewCtrl.dismiss();
  }

  DIAL_CLICK() {

    this.call.isCallSupported().then((res) => {
      this.call.callNumber(this.call_number, true)
    }, err => {
      console.log(err);
    });
  }

  Init (data) {
    this.firebaseprovider.Get_Contact_Info_Staff(data).then((value) => {
      let number = value['phone'];
      if(number.includes('-')) {
        number = number.replace('-', '');
      }
      this.call_number_display = value['phone'];
      this.call_number = number;
    });
  }

}
