import { PhotoViewer } from '@ionic-native/photo-viewer';
import { NgZone } from '@angular/core';
import { MessageChat } from './../../../../../class/Resource/StringList';
import { AlertController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';
import { AngularFireStorage } from 'angularfire2/storage';
@IonicPage()
@Component({
  selector: 'page-main-menu-chat-image',
  templateUrl: 'main-menu-chat-image.html',
})
export class MainMenuChatImagePage {

  @ViewChild('slide_pic') sld_slide: Slides;
  chatroom_info: {
    chatroom: string;
    user_id: string;
    contact_id: string;
    image: string;
    fname: string;
  };
  image_list = [];
  image_class = "img-slide-hidden";
  image: any;
  // image_click_data : any;
  init_value = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private firebase: AngularFireDatabase,
    private firestorage: AngularFireStorage,
    private iab: InAppBrowser,
    private zone: NgZone,
    private alertCtrl: AlertController,
    private photoviewer: PhotoViewer
  ) {
    this.chatroom_info = this.navParams.get('chatroom');
    this.init(this.navParams.get('data'));
    // this.image_click_data = this.navParams.get('data');
    // this.load_photo(this.navParams.get('data'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuChatImagePage');
  }

  ionViewDidEnter() {
    this.sld_slide.update();
    this.sld_slide.slideTo(this.init_value, 0);
    this.image_class = "img-slide-no-hidden"
  }

  DOWNLOAD_CLICK () {
    this.load();
  }

  PHOTO_CLICK () {
    this.photoViewer();
  }

  alertBox (message: string) {
    this.alertCtrl.create({
      subTitle: message,
      buttons: ['OK']
    }).present();
  }

  private async init (data) {
    let wait = await this.load_photo(data);
  }

  private load_photo (data) {
    return new Promise<any>((resolve, reject) => {
      this.firebase.database
      .ref("chatroom")
      .child(this.chatroom_info['chatroom'])
      .orderByChild('message_type')
      .equalTo(MessageChat.MessageType.IMAGE)
      .once('value', (snap) => {
        if (snap.exists()) {

          this.zone.run(() => {
            // Get initial value
            let i = 0;
            snap.forEach((child) => {
              let msg = child.val();
              this.image_list.push(msg);
    
              if (child.key === data['key']) {
                this.init_value = i;
              }
              i++;
            });
            resolve();
          });
        }
        else {
          reject();
        }
      });
    });

  }

  private load () {
    let active_index = this.sld_slide.getActiveIndex();
    const browser = this.iab.create(this.image_list[active_index]['message']);
    browser.show();
  }

  private photoViewer () {
    let active_index = this.sld_slide.getActiveIndex();
    this.photoviewer.show(this.image_list[active_index]['message'],'' ,{share: false});
  }
}
