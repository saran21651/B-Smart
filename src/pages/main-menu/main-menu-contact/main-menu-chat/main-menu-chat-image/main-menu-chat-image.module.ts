import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuChatImagePage } from './main-menu-chat-image';

@NgModule({
  declarations: [
    MainMenuChatImagePage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuChatImagePage),
  ],
})
export class MainMenuChatImagePageModule {}
