import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuChatPage } from './main-menu-chat';

@NgModule({
  declarations: [
    MainMenuChatPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuChatPage),
  ],
})
export class MainMenuChatPageModule {}
