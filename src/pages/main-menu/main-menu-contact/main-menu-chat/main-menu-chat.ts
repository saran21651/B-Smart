import { ModalProvider } from "./../../../../providers/modal/modal";
import { ControllerProvider } from "./../../../../providers/controller/controller";
import { MessageChat } from "./../../../../class/Resource/StringList";
import { AngularFireDatabase } from "angularfire2/database";
import { Component, ViewChild, NgZone, ElementRef } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Content,
  ModalController,
  AlertController,
  Events
} from "ionic-angular";
import { FormBuilder, FormControl } from "@angular/forms";
import { Page } from "../../../../class/Resource/Page";
import { AngularFireStorage } from "../../../../../node_modules/angularfire2/storage";
import { OnesignalProvider } from "../../../../providers/onesignal/onesignal";
import { ChatServiceProvider } from "../../../../providers/chat-service/chat-service";
import { ApplicationRef } from "@angular/core";
import { OrderCarrierProvider } from "../../../../providers/order-carrier/order-carrier";

@IonicPage()
@Component({
  selector: "page-main-menu-chat",
  templateUrl: "main-menu-chat.html"
})
export class MainMenuChatPage {
  @ViewChild(Content)
  content: Content;
  @ViewChild("fixedContainer")
  fixedContainer: ElementRef;

  messageForm: any;
  chatBox: any;
  messages = [];
  chat_type = "";//chat_type = MessageChat.ChatType.STAFF_TO_STAFF;
  chatroom_info: {
    chatroom: string;
    user_id: string;
    contact_id: string;
    contact_onesignal_id: string;
    image: string;
    fname: string;
  };

  debug_bool = false;

  // Order-Preorder
  order_info: any;
  is_preorder = false;
  is_order = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public modalCtrl: ModalController,
    public firebase: AngularFireDatabase,
    public firestorage: AngularFireStorage,
    public chatservice: ChatServiceProvider,
    public controller: ControllerProvider,
    public modal: ModalProvider,
    public onesignal: OnesignalProvider,
    public app: ApplicationRef,
    public orderprovider: OrderCarrierProvider
  ) {
    this.messageForm = formBuilder.group({
      message: new FormControl("")
    });
    this.chatBox = "";
    let data = this.navParams.get("data");
    this.Init(data);
    this.debug(data);
  }

  ionViewDidLoad () {
    if (this.chat_type === MessageChat.ChatType.CUSTOMER_TO_SALE) {
      if (this.order_info != undefined) {
          // this.fixedContainer_Setup();
          this.add_padding();
      }
    }
  }

  ionViewWillLeave() {
    this.chatservice.Unsubscribe_Event(this.chatservice.EVENT.MESSAGE_LIST);
    this.chatservice.Unsubscribe_Event(this.chatservice.EVENT.SET_SEEN);
    this.chatservice.Remove_Observe_Message();
    this.onesignal.subscribe_chat_notification();
  }

  ionViewDidEnter() {
    this.chatservice.Observe_Messages().then(() => {
      this.scrollToBottom(5);
    });
  }

  BACK_CLICK() {
    this.back_button();
  }

  SELLER_INFO_CLICK() {
    this.openModal(Page.Menu.Chat.Modal.Info, "inset-modal-info");
  }

  PREORDER_STATUS_CLICK () {
    this.modalCtrl
      .create(
        Page.Menu.Chat.Modal.PreOrder,
        { data: this.chatroom_info },
        { cssClass: "" }
      )
      .present();
  }

  CALL_CLICK() {
    this.openModal(Page.Menu.Chat.Modal.Call, "inset-modal");
  }

  CAMERA_CLICK() {
    this.controller.Show_Loading();
    this.chatservice.Call_Camera().then(() => {
      this.controller.Hide_Loading();
      this.scrollToBottom(5);
    });
  }

  IMAGE_CLICK(data) {
    this.chatservice.Call_Image_Page(data);
  }

  LINK_CLICK(data) {
    this.chatservice.Call_Browser(data);
  }

  SENDMESSAGE_CLICK(message) {
    this.chatservice.Send_Text(message).then(() => {
      this.chatBox = "";
      this.scrollToBottom(5);
    });
  }

  SENDIMAGE_CLICK(data) {
    this.controller.Show_Loading();
    this.chatservice.Send_Image_Storage(data).then(() => {
      this.controller.Hide_Loading();
      this.scrollToBottom(5);
    });
  }

  SENDFILE_CLICK(event) {
    this.controller.Show_Loading();
    this.chatservice.Send_File(event).then(() => {
      this.controller.Hide_Loading();
      this.scrollToBottom(5);
    });
  }

  ORDER_DETAIL_CLICK() {
    this.modalCtrl
      .create(
        Page.Menu.Chat.Modal.Order_Info,
        { data: this.order_info },
        { cssClass: "inset-modal-order-info" }
      )
      .present();
  }

  // send success data
  SUBMIT_ORDER_CLICK () {

  }

  doRefresh(event) {
    setTimeout(() => {
      this.chatservice.Load_Chat_Event_On_Refresh().then(async () => {

        this.messages = this.chatservice.Get_Message();
        event.complete();
      });
    }, 1000);
  }

  openModal(page, inset) {
    this.modalCtrl
      .create(
        page,
        { data: this.chatroom_info["contact_id"] },
        { cssClass: inset }
      )
      .present();
  }

  scrollToBottom(duration?: number) {
    let timeout_duration = duration || 100;
    setTimeout(() => {
      this.content.scrollToBottom();
    }, timeout_duration);
  }

  private async Init(data) {
    console.log(data);
    this.chat_type = data["chat_type"];
    
    if (this.chat_type == MessageChat.ChatType.CUSTOMER_TO_SALE) {
      /*this.order_info = await this.chatservice.Load_Order_Information();
      if (this.order_info != (undefined || null)) {
        // this.fixedContainer_Setup();
        this.is_design_pattern_request = true;
      }
      */
     /*
      if (data['order_info'] != undefined) {
        this.order_info = data['order_info'];
        this.is_order = true;
      }
      */
        
        let order_ = await this.chatservice.Get_Additional_Data(data["additional_data"]);
        console.log(order_);
        //if (data['order_info'] != undefined) {
        if (order_ != "") {
        // this.order_info = data['order_info'];
          this.order_info = order_;
          this.is_order = true;
      }
    }

    if (this.chat_type == MessageChat.ChatType.CUSTOMER_TO_SALE_PREORDER) {
      this.is_preorder = true;
    }
    
    this.chatservice.Setup_Chat(data, this.chat_type, 10).then(async () => {
      this.onesignal.unsubscribe_chat_notification();
      this.chatroom_info = this.chatservice.Get_Chatroom_Info();
      this.messages = this.chatservice.Get_Message();
      this.app.tick();
      this.scrollToBottom();
    });
  }

  private back_button() {
    console.log(this.navCtrl.length());
    if (this.navCtrl.length() == 1) {
      this.navCtrl.setRoot(
        Page.Menu.Page,
        {},
        {
          animate: false,
          direction: "backward"
        }
      );
    } else {
      this.navCtrl.pop();
    }
  }

  private fixedContainer_Setup() {

    let fixedContainer = this.fixedContainer.nativeElement;
    let itemHeight = fixedContainer.offsetHeight;
    let scroll = this.content.getScrollElement();
    let computeStyle = window.getComputedStyle(scroll);
    itemHeight =
      Number.parseFloat(scroll.style.marginTop.replace("px", "")) + itemHeight;
    scroll.style.marginTop = itemHeight + "px";
    
  }

  private add_padding () {
    let scroll = this.content.getScrollElement();
    scroll.classList.add("order-padding");
  }

  debug_text;
  debug (data) {
    this.debug_bool = true;
    this.debug_text = data;
  }
  /*
  @ViewChild(Content) content: Content;

  messageForm: any;
  chatBox: any;
  messages = [];

  contactInfo: any;
  limit_chat_load = 10;
  current_chat_load = 0;
  key: any;

  chatroom_info: {
    chatroom: string;
    user_id: string;
    contact_id: string;
    contact_onesignal_id: string;
    image: string;
    fname: string;chatroom
  };

  chat_type = MessageChat.ChatType.CUSTOMER_TO_SALE;

  store: any;
  user_info = UserInformationModel.prototype;

  // Firebase
  fb_chat: any;
  fb_set_seen: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public modalCtrl: ModalController,
    public firebase: AngularFireDatabase,
    public firestorage: AngularFireStorage,
    public storage: Storage,
    public iab: InAppBrowser,
    public alertCtrl: AlertController,
    public camera: Camera,
    public http: HttpClient,
    public zone: NgZone,
    public userservice: UserServiceProvider,
    public onesignalservice: OnesignalProvider,
    private photoviewer: PhotoViewer
  ) {
    this.messageForm = formBuilder.group({
      message: new FormControl("")
    });
    this.zone = new NgZone({ enableLongStackTrace: false });
    this.chatBox = "";
    let data = this.navParams.get("data");
    console.log(data);
    console.log(this.navParams.data);
    console.log(navCtrl);
    this.init(data);
  }

  async ionViewDidLoad() {
    console.log("ionViewDidLoad MainMenuChatPage");
  }

  ionViewDidLeave() {
    if (this.fb_chat != undefined) {
      this.fb_chat.off();
    }
    if (this.fb_set_seen != undefined) {
      this.fb_set_seen.off();
    }
  }

  openModal(page, inset) {
    this.modalCtrl
      .create(page, { data: this.contactInfo }, { cssClass: inset })
      .present();
  }

  BACK_CLICK () {
    this.navCtrl.pop();
  }

  SELLER_INFO_CLICK() {
    this.openModal(Page.Menu.Chat.Modal.Info, "inset-modal-info");
  }

  CALL_CLICK() {
    this.openModal(Page.Menu.Chat.Modal.Call, "inset-modal");
  }

  CAMERA_CLICK() {
    this.call_camera();
  }

  IMAGE_CLICK(data) {
    this.call_imagepage(data);
  }

  LINK_CLICK(data) {
    this.call_browser(data);
  }

  SENDMESSAGE_CLICK(message) {
    this.sendtext(message);
  }

  SENDIMAGE_CLICK(data) {
    this.sendimage_storage(data);
  }

  SENDFILE_CLICK(event) {
    this.sendfile(event);
  }

  doRefresh(event) {
    this.current_chat_load = this.messages.length + this.limit_chat_load;
    this.loadChat_on_refresh(this.current_chat_load);
    setTimeout(() => {
      event.complete();
    }, 1000);
  }

  scrollToBottom() {
    setTimeout(() => {
      this.content.scrollToBottom();
    }, 100);
  }

  alertBox(message: string) {
    this.alertCtrl
      .create({
        subTitle: message,
        buttons: ["OK"]
      })
      .present();
  }

  private async init(data) {
    console.log(data);
    this.user_info = await this.userservice.get_user_information().then();
    let contact_onesignal_id = await this.onesignalservice.get_contact_player_id(
      data["contact_id"],
      this.chat_type
    );
    this.chatroom_info = {
      chatroom: data["chatroom"],
      user_id: data["user_id"],
      contact_id: data["contact_id"],
      contact_onesignal_id: contact_onesignal_id,
      image: this.user_info["image"],
      fname: this.user_info["fname"]
    };

    this.messages = [];
    this.initial_chat();
  }

  private initial_chat() {
    this.firebase.database
      .ref("chatroom")
      .child(this.chatroom_info["chatroom"])
      .limitToLast(this.limit_chat_load)
      .once("value", snap => {
        if (snap.exists()) {
          snap.forEach(child => {
            let message = child.val();
            message["key"] = child.key;
            message["timeStamp"] = Datetime.get_chat_timestamp(
              child.child("timeStamp").val()
            );
            this.messages.push(message);

            if (
              message["read"] == MessageChat.Seen.NOT_READ &&
              this.chatroom_info["user_id"] != message["sender_id"]
            ) {
              this.set_seen_in_firebase(message["key"]);
            }

            this.key = child.key;
            return false;
          });
          this.scrollToBottom();
        }

        this.loadChat_event();
      });
  }

  private get_contact_data() {
    this.firebase.database
      .ref("user_information")
      .orderByChild("auth/uid")
      .equalTo(this.chatroom_info["contact_id"])
      .once("value", snap => {
        if (snap.exists()) {
          snap.forEach(child => {
            this.contactInfo = child.val();
          });
        }
      });
  }

  private loadChat_event() {
    this.fb_chat = this.firebase.database
      .ref("chatroom")
      .child(this.chatroom_info["chatroom"])
      .limitToLast(1);

    this.fb_chat.on("child_added", snap => {
      this.zone.run(() => {
        if (this.key != snap.key) {
          let message = snap.val();
          message["key"] = snap.key;
          message["timeStamp"] = Datetime.get_chat_timestamp(
            snap.child("timeStamp").val()
          );
          this.messages.push(message);
          if (
            message["read"] == MessageChat.Seen.NOT_READ &&
            this.chatroom_info["user_id"] != message["sender_id"]
          ) {
            this.set_seen_in_firebase(message["key"]);
          }
          this.key = snap.key;
        }
        this.scrollToBottom();
      });
    });

    this.fb_set_seen = this.firebase.database
      .ref("chatroom")
      .child(this.chatroom_info["chatroom"])
      .limitToLast(1);

    this.fb_set_seen.on("child_changed", snap => {
      if (snap.exists()) {
        // TODO: Check if working
        console.log(snap.val());
        this.zone.run(() => {
          this.messages[this.messages.length - 1]["read"] = "read";
        });
      }
    });
  }

  private loadChat_on_refresh(number: number) {
    this.firebase.database
      .ref("chatroom")
      .child(this.chatroom_info["chatroom"])
      .limitToLast(number)
      .once("value", async snap => {
        if (snap.exists()) {
          let number_chat = this.messages.length;
          let number_chat_load = snap.numChildren();

          if (number_chat_load > number_chat) {
            this.messages = [];
            // let count = number_chat_load - number_chat;

            snap.forEach(child => {
              let msg = child.val();
              msg["key"] = child.key;
              msg["timeStamp"] = Datetime.get_chat_timestamp(
                child.child("timeStamp").val()
              );
              this.messages.push(msg);

              if (
                msg["read"] == MessageChat.Seen.NOT_READ &&
                this.chatroom_info["user_id"] != msg["sender_id"]
              ) {
                this.set_seen_in_firebase(msg["key"]);
              }

              return false;
            });
          }
        }
      });
  }

  private set_seen_in_firebase(key: string) {
    console.log(key);
    this.firebase.database
      .ref("chatroom")
      .child(this.chatroom_info["chatroom"])
      .child(key)
      .child("read")
      .set(MessageChat.Seen.READ);
  }

  private sendtext(message) {
    if (message && message !== "") {
      let msg = MessageModel.prototype;
      msg = {
        message: message,
        sender_id: this.chatroom_info["user_id"],
        receiver_id: this.chatroom_info["contact_id"],
        image: this.chatroom_info["image"],
        timeStamp: Datetime.getTimeInMilliseconds(),
        read: MessageChat.Seen.NOT_READ,
        name: this.chatroom_info["fname"],
        file: "-",
        message_type: MessageChat.MessageType.TEXT,
        type: this.chat_type
      };
      this.firebase.database
        .ref("chatroom")
        .child(this.chatroom_info["chatroom"])
        .push()
        .set(msg)
        .then(res => {
          this.send_push_notification(msg);
          this.scrollToBottom();
        });

      // this.scrollToBottom();
    }
    this.chatBox = "";
  }

  private async sendfile(event) {
    console.log(event);
    let file = event.target.files[0];
    console.log(file);
    if (file == undefined) {
      return;
    }
    let file_path = "files/" + this.chatroom_info["chatroom"] + "/" + file.name;
    await this.firestorage.storage.ref(file_path).put(file);
    console.log(file_path);
    let fileUrl = await this.firestorage.storage
      .ref(file_path)
      .getDownloadURL();
    let msg = MessageModel.prototype;
    msg = {
      message: fileUrl,
      sender_id: this.chatroom_info["user_id"],
      receiver_id: this.chatroom_info["contact_id"],
      image: this.chatroom_info["image"],
      timeStamp: Datetime.getTimeInMilliseconds(),
      read: MessageChat.Seen.NOT_READ,
      name: this.chatroom_info["fname"],
      file: file.name,
      message_type: MessageChat.MessageType.FILE,
      type: this.chat_type
    };

    this.firebase.database
      .ref("chatroom")
      .child(this.chatroom_info["chatroom"])
      .push()
      .set(msg)
      .then(
        res => {
          this.send_push_notification(msg);
          this.alertBox("สำเร็จ");
          this.scrollToBottom();
        },
        err => {
          this.alertBox("ล้มเหลว");
        }
      );
  }

  private async sendimage_storage(event) {
    console.log(event);
    if (event == undefined) {
      return;
    }
    let img: any;
    let file_path: any;
    img = event.target.files[0];
    if (img["type"].substring(0, 5) != "image") {
      this.alertBox("รูปแบบไฟล์ไม่ถูกต้อง");
      return;
    }
    file_path =
      "image/" +
      this.chatroom_info["chatroom"] +
      "/" +
      Datetime.getTimeInMilliseconds() +
      "/" +
      img.name;
    this.send_image(file_path, img);
  }

  private async sendimage_camera(event) {
    if (event == undefined) {
      return;
    }
    let img: any;
    let file_path: any;
    img = event;
    file_path =
      "image/" +
      this.chatroom_info["chatroom"] +
      "/" +
      Datetime.getTimeInMilliseconds() +
      ".jpg";
    this.send_image(file_path, img);
  }

  private async send_image(file_path, img) {
    await this.firestorage.storage.ref(file_path).put(img);
    let fileUrl = await this.firestorage.storage
      .ref(file_path)
      .getDownloadURL();
    let msg = MessageModel.prototype;
    msg = {
      message: fileUrl,
      sender_id: this.chatroom_info["user_id"],
      receiver_id: this.chatroom_info["contact_id"],
      image: this.chatroom_info["image"],
      timeStamp: Datetime.getTimeInMilliseconds(),
      read: MessageChat.Seen.NOT_READ,
      name: this.chatroom_info["fname"],
      file: "-",
      message_type: MessageChat.MessageType.IMAGE,
      type: this.chat_type
    };
    this.firebase.database
      .ref("chatroom")
      .child(this.chatroom_info["chatroom"])
      .push()
      .set(msg)
      .then(
        res => {
          this.send_push_notification(msg);
          this.alertBox("สำเร็จ");
          this.scrollToBottom();
        },
        err => {
          this.alertBox("ล้มเหลว");
        }
      );
  }

  private send_push_notification(msg: MessageModel) {
    let push_content: any;
    switch (msg["message_type"]) {
      case MessageChat.MessageType.TEXT:
        push_content = msg["message"];
        break;
      case MessageChat.MessageType.IMAGE:
        push_content = "Image";
        break;
      case MessageChat.MessageType.FILE:
        push_content = "Attach";
        break;
    }
    this.onesignalservice.send_chat_notification({
      msg: msg,
      content: push_content,
      onesignal_id: this.chatroom_info["contact_onesignal_id"],
      chatroom: this.chatroom_info
    });
  }

  private call_camera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetHeight: 800,
      targetWidth: 600,
      sourceType: 1
    };
    this.camera.getPicture(options).then(
      res => {
        let base64image = "data:image/jpeg;base64," + res;
        let img = ImageUtil.prototype.datauri_to_blob(base64image);
        this.sendimage_camera(img);
      },
      err => {
        console.log(err);
      }
    );
  }

  private call_browser(data) {
    window.open(data, "_system");
  }

  private call_imagepage(data) {

   this.photoviewer.show(data['message'],'' ,{share: false});
  }
  */
}

/*
::::    :::  ::::::::   ::::::::  ::::    :::      :::    :::     :::     :::    :::
:+:+:   :+: :+:    :+: :+:    :+: :+:+:   :+:      :+:   :+:    :+: :+:   :+:   :+:
:+:+:+  +:+ +:+    +:+ +:+    +:+ :+:+:+  +:+      +:+  +:+    +:+   +:+  +:+  +:+
+#+ +:+ +#+ +#+    +:+ +#+    +:+ +#+ +:+ +#+      +#++:++    +#++:++#++: +#++:++
+#+  +#+#+# +#+    +#+ +#+    +#+ +#+  +#+#+#      +#+  +#+   +#+     +#+ +#+  +#+
#+#   #+#+# #+#    #+# #+#    #+# #+#   #+#+#      #+#   #+#  #+#     #+# #+#   #+#
###    ####  ########   ########  ###    ####      ###    ### ###     ### ###    ###
*/
