import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-main-menu-chat-order-info',
  templateUrl: 'main-menu-chat-order-info.html',
})
export class MainMenuChatOrderInfoPage {

  order_info: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController) {
      this.order_info = navParams.get("data");
  }

  DISMISS_CLICK () {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuChatOrderInfoPage');
  }

}
