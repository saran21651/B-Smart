import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuChatOrderInfoPage } from './main-menu-chat-order-info';

@NgModule({
  declarations: [
    MainMenuChatOrderInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuChatOrderInfoPage),
  ],
})
export class MainMenuChatOrderInfoPageModule {}
