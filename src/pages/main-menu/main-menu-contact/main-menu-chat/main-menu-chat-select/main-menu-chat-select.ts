import { ChatServiceProvider } from './../../../../../providers/chat-service/chat-service';
import { Page } from './../../../../../class/Resource/Page';
import { CallNumber } from '@ionic-native/call-number';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { MessageChat } from '../../../../../class/Resource/StringList';
import { OrderCarrierProvider } from '../../../../../providers/order-carrier/order-carrier';

@IonicPage()
@Component({
  selector: 'page-main-menu-chat-select',
  templateUrl: 'main-menu-chat-select.html',
})
export class MainMenuChatSelectPage {

  chat_info: any;
  chat_type = MessageChat.ChatType.CUSTOMER_TO_SALE;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public chatservice: ChatServiceProvider,
    public call: CallNumber,
    public viewCtrl: ViewController,
    public orderprovider: OrderCarrierProvider
  ) {
      this.chat_info = navParams.get('data');
      this.chat_info['chat_type'] = this.chat_type;

      console.log(this.chat_info);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuChatSelectPage');
  }

  GO_TO_CHAT_CLICK () {
    let order = this.orderprovider.Get();
    console.log(order);
    
    if (this.orderprovider.Exist()) {
      this.chatservice.Create_Chatroom(this.chat_info, order['order_id']).then(res => {

        let data = res;
        data['data']['order_info'] = this.orderprovider.Get();
        this.navCtrl.push(Page.Menu.Chat.Page, data);
        setTimeout(() => {
          this.DISMISS_CLICK();
        }, 100);
      });

    } else {
      this.chatservice.Create_Chatroom(this.chat_info).then(res => {

        let data = res;
        // data['data']['order_info'] = this.debug(); // Debug Purpose
        this.navCtrl.push(Page.Menu.Chat.Page, data);
        setTimeout(() => {
          this.DISMISS_CLICK();
        }, 100);
      });
    }
    
    
    /*
    let line = "https://line.me/ti/p/~" + String(this.chat_info['line']);
    window.open(line, "_system");
    */
  }

  GO_TO_CALL_CLICK () {
    let number: string = this.chat_info['phone'];
    if (number != undefined) {
      if(number.includes('-')) {
        number = number.replace('-', '');
      }
      this.call.callNumber(number, true);
    }
  }

  DISMISS_CLICK () {
    this.viewCtrl.dismiss();
  }

  debug () {
    // Test Preorder 
    let debug_order = {
      "delivery_type" : "2",
      "destination" : {
        "address" : "99/9 หมู่ 9 ต.ท่าโพธ์",
        "city" : "อ.เมือง",
        "fname" : "นายใจดี",
        "lname" : "พิมพ์ใจ",
        "province" : "จ.พิษณุโลก",
        "telephone" : "0987776666",
        "zipcode" : "65000"
      },
      "order_id" : "ord-1113002018091982728",
      "order_list" : [ {
        "amount" : 1,
        "category_id" : "c-001",
        "design_pattern" : {
          "present" : false
        },
        "options" : [ {
          "id" : "2",
          "option" : "นามบัตรพิมพ์สี",
          "option_name" : "ชนิด",
          "price_fix" : false,
          "rate" : [ {
            "id" : "1",
            "option" : "1 หน้า",
            "price" : [ {
              "end" : "2",
              "price" : 250,
              "start" : "1"
            }, {
              "end" : "4",
              "price" : 200,
              "start" : "3"
            }, {
              "end" : "9",
              "price" : 180,
              "start" : "5"
            }, {
              "end" : "19",
              "price" : 150,
              "start" : "10"
            }, {
              "end" : "n",
              "price" : 100,
              "start" : "20"
            } ]
          }, {
            "id" : "2",
            "option" : "2 หน้า",
            "price" : [ {
              "end" : "2",
              "price" : 350,
              "start" : "1"
            }, {
              "end" : "4",
              "price" : 300,
              "start" : "3"
            }, {
              "end" : "9",
              "price" : 280,
              "start" : "5"
            }, {
              "end" : "19",
              "price" : 250,
              "start" : "10"
            }, {
              "end" : "n",
              "price" : 200,
              "start" : "20"
            } ]
          } ]
        }, {
          "id" : "1",
          "option" : "1 หน้า",
          "option_name" : "จำนวนหน้า",
          "price" : [ {
            "end" : "4",
            "price" : 150,
            "start" : "1"
          }, {
            "end" : "9",
            "price" : 100,
            "start" : "5"
          }, {
            "end" : "n",
            "price" : 80,
            "start" : "10"
          } ]
        }, {
          "id" : "4",
          "option" : "กระดาษอาร์ตมัน 350 แกรม",
          "option_name" : "กระดาษ",
          "price_fix" : true,
          "rate" : 50
        }, {
          "id" : "4",
          "option" : "เคลือบ 250 ไมครอน",
          "option_name" : "เคลือบ",
          "price_fix" : true,
          "rate" : 300
        }, {
          "id" : "0",
          "option" : "ไม่ไดคัท",
          "option_name" : "ไดคัท",
          "price_fix" : true,
          "rate" : 0
        }, {
          "id" : "1",
          "option" : "ปั้มมุม",
          "option_name" : "ปั้มมุม",
          "price_fix" : true,
          "rate" : 50
        } ],
        "price" : {
          "design_price" : 100,
          "product_price" : 650,
          "promotion_price" : 0,
          "total_price" : 750
        },
        "product_code" : "BSC-214401",
        "product_description" : "บัตรสมาชิก พิมพ์ 4 สี \nพิมพ์หน้าหลัง \nเคลือบ 250 ไมครอน\n\nบัตรสมาชิก บัตรส่วนลด พิมพ์สี หน้า-หลัง หรือหน้าเดียว รันนัมเบอร์ หรือใส่บาร์โค๊ตได้ พร้อมเคลือบแข็งป้องกันน้ำ มีความหนาให้เลือกทั้งแบบ 125 ไมครอน และเคลือบแข็งพิเศษ 250 ไมครอน\n\nบัตรสมาชิก พร้อมรันนัมเบอร์ ",
        "product_id" : "prd-1610232018091362320",
        "product_image" : [ "https://firebasestorage.googleapis.com/v0/b/b-smart-3a342.appspot.com/o/product_image%2F1536206267832_m05.jpg?alt=media&token=839c7ecc-b816-4d47-be5a-479b4cc4266b" ],
        "product_name" : "บัตรสมาชิก พิมพ์ 4 สี หน้าหลัง เคลือบ 250 ไมครอน",
        "product_type" : "1",
        "product_unit" : "ใบ"
      } ],
      "order_status" : 6,
      "order_status_text" : "คำสั่งซื้อเสร็จสมบูรณ์",
      "payment" : {
        "detail" : {
          "bank" : "SCB",
          "name" : "B-smart Printing",
          "number" : "XXX-XXX-XXX"
        },
        "inform" : {
          "account" : "init",
          "datetime_transfer" : "sd",
          "evident_pic" : ""
        },
        "method_type" : "bank"
      },
      "price" : {
        "product_price" : 750,
        "promotion_price" : 0,
        "shipping_price" : 10,
        "total_price" : 760
      },
      "search" : {
        "order_status" : "G1YbG1DjowNq6HSO6L2Z96nIlhJ36"
      },
      "timestamp" : {
        "confirm_order_time" : "1537842191611",
        "delivered_time" : "1537842246201",
        "delivering_time" : "1537842227410",
        "order_time" : "1537842135000",
        "purchase_time" : "1537842173000"
      },
      "uid" : "G1YbG1DjowNq6HSO6L2Z96nIlhJ3",
      "user_information" : {
        "email" : "",
        "fname" : "",
        "image" : "https://firebasestorage.googleapis.com/v0/b/b-smart-3a342.appspot.com/o/image_user%2FG1YbG1DjowNq6HSO6L2Z96nIlhJ3%2Fprofile?alt=media&token=0253490b-dca5-4b67-b33b-2cc3979774ff",
        "join_date" : "Fri Aug 10 2018 15:59:07 GMT+0700 (Indochina Time)",
        "lname" : "",
        "pname" : "",
        "uid" : "G1YbG1DjowNq6HSO6L2Z96nIlhJ3"
      }
    }

    return debug_order;
  }

}
