import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuChatSelectPage } from './main-menu-chat-select';

@NgModule({
  declarations: [
    MainMenuChatSelectPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuChatSelectPage),
  ],
})
export class MainMenuChatSelectPageModule {}
