import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuChatInfoPage } from './main-menu-chat-info';

@NgModule({
  declarations: [
    MainMenuChatInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuChatInfoPage),
  ],
  exports: [
    MainMenuChatInfoPage
  ]
})
export class MainMenuChatInfoPageModule {}
