import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FirebaseProvider } from '../../../../../providers/firebase/firebase';


@IonicPage()
@Component({
  selector: 'page-main-menu-chat-info',
  templateUrl: 'main-menu-chat-info.html',
})
export class MainMenuChatInfoPage {

  info : any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public firebaseprovider: FirebaseProvider
  ) {
    this.info = this.navParams.get('data');
    this.Init(this.info);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuChatInfoPage');
  }

  DISMISS_CLICK () {
    this.viewCtrl.dismiss();
  }

  Init (data) {
    this.firebaseprovider.Get_Contact_Info_Staff(data).then((value) => {
      this.info = value;
    });
  }

}
