import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuChatPreorderPage } from './main-menu-chat-preorder';

@NgModule({
  declarations: [
    MainMenuChatPreorderPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuChatPreorderPage),
  ],
})
export class MainMenuChatPreorderPageModule {}
