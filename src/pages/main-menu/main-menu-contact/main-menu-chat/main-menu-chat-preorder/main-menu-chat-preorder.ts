import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChatServiceProvider } from '../../../../../providers/chat-service/chat-service';
import { PreOrder_Model } from '../../../../../class/Model/PreOrder/PreOrder';
import { FirebaseProvider } from '../../../../../providers/firebase/firebase';
import { Page } from '../../../../../class/Resource/Page';
import { CartserviceProvider } from '../../../../../providers/cartservice/cartservice';
import { ControllerProvider } from '../../../../../providers/controller/controller';

@IonicPage()
@Component({
  selector: 'page-main-menu-chat-preorder',
  templateUrl: 'main-menu-chat-preorder.html',
})
export class MainMenuChatPreorderPage {

  order_info: PreOrder_Model = {
    Product_Category : "",
    Product_Name: "",
    Product_Description: "",
    Product_Image: "",
    Amount: "",
    Product_Price: "",
    Employee_ID: "",
    Customer_ID: "",
    Product_Id: ""
  };
  chat_info: any;

  display_image: any;

  is_disabled: boolean = true; // True if "Customer" === False if "Sale"

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public chatservice: ChatServiceProvider,
    public zone: NgZone,
    public firebaseprovider: FirebaseProvider,
    public cartservice: CartserviceProvider,
    public controller: ControllerProvider,
    ) {
      this.zone = new NgZone({enableLongStackTrace: false});
      this.chat_info = navParams.get('data');
      this.init();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuChatPreorderPage');
  }

  ionViewWillLeave() {
    this.chatservice.Unsubscribe_Event(this.chatservice.EVENT.PREORDER);
  }

  SUBMIT_CLICK () {
    this.chatservice.Set_PreOrder_Chat(this.order_info, this.chat_info['chatroom']).then((res) => {
      this.navCtrl.pop();
    });
  }

  DISMISS_CLICK() {
    this.navCtrl.pop();
  }

  CUSTOMER_SUBMIT_CLICK () {
    this.customer_submit();
  }

  CUSTOMER_CANCEL_CLICK () {
    this.navCtrl.pop();
  }

  CHAGE_PROFILE_IMAGE(event) {
    this.change_image_pattern(event);
  }

  private customer_submit() {
    let tmp = {
      order_list : {
        amount: this.order_info.Amount,
        product_category: this.order_info.Product_Category,
        product_description: this.order_info.Product_Description,
        product_image: this.order_info.Product_Image,
        product_name: this.order_info.Product_Name,
        product_price: this.order_info.Product_Price
      },
      employee_information : this.chat_info['contact_id'],
      order_id: "",
      order_status: 1,
      order_status_text: "",
      cancel_status: {
        user_cancel: false,
        store_cancel: false
      },
      price: {
        product_price: this.order_info.Product_Price,
        shipping_price: 0,
        total_price: this.order_info.Product_Price
      }
    }
    this.controller.Show_Loading();
    this.firebaseprovider.Set_PreOrder_Data(tmp, this.chat_info['chatroom']).then((key) => {
      this.firebaseprovider.Find_Preorder_By_Key(key).then((res) => {
        // res['push_key'] = key;
        this.cartservice.Set_Cart_State(1);
        this.cartservice.Add_Push_Key_For_Preorder(key);
        this.navCtrl.push(Page.Menu.Cart.Checkout_Preorder.Page, { data : res });
        this.controller.Hide_Loading();
      });
    });

    // this.navCtrl.push(Page.Menu.Cart.Checkout_Preorder.Page, { data : this.order_info.Product_Id });
    /*
    this.navCtrl.setRoot(Page.Menu.Cart.Checkout_Preorder.Page, {}, {
      animate: true,
      direction: "forward"
    });
    */
  }
  
  private init () {
    this.controller.Show_Loading();
    this.chatservice.Get_PreOrder_Chat(this.chat_info['chatroom']).then((value) => {
      if (value[0]['additional_data'] != undefined) {
        this.order_info = {
          Product_Category : value[0]['additional_data']['Product_Category'],
          Product_Name: value[0]['additional_data']['Product_Name'],
          Product_Description: value[0]['additional_data']['Product_Description'],
          Product_Image: value[0]['additional_data']['Product_Image'],
          Amount: value[0]['additional_data']['Amount'],
          Product_Price: value[0]['additional_data']['Product_Price'],
          Employee_ID: value[0]['additional_data']['Employee_ID'],
          Customer_ID: value[0]['additional_data']['Customer_ID'],
          Product_Id: value[0]['additional_data']['Product_Id']
        }
        this.display_image = value[0]['additional_data']['Product_Image'];
      }
      this.controller.Hide_Loading();
    });
  
    this.chatservice.Get_PreOrder_Chat_OnEvent(this.chat_info['chatroom']).subscribe(this.chatservice.EVENT.PREORDER, (value) => {
      this.order_info = {
        Product_Category : value['Product_Category'],
        Product_Name: value['Product_Name'],
        Product_Description: value['Product_Description'],
        Product_Image: value['Product_Image'],
        Amount: value['Amount'],
        Product_Price: value['Product_Price'],
        Employee_ID: value['Employee_ID'],
        Customer_ID: value['Customer_ID'],
        Product_Id: value['Product_Id']
      }
      this.display_image = value['Product_Image'];
    });
  }

  async change_image_pattern (event) {
    if (event == undefined) {
      return;
    }

    let reader = new FileReader();
    reader.onload = (event1: any) => {
      this.display_image = event1.target.result;
      this.order_info['Product_Image'] = event.target.files[0];
    };
    reader.readAsDataURL(event.target.files[0]);
  }

  check_input_not_empty () {
    if (
      this.order_info["Product_Name"] != "" &&
      this.order_info["Product_Price"] != "" &&
      this.order_info["Amount"] != ""
    )
    { 
      return true;
    } else {
      return false;
    }
  }
}
