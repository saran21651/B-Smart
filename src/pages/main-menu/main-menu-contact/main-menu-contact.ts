import { ChatServiceProvider } from './../../../providers/chat-service/chat-service';
import { ModalProvider } from './../../../providers/modal/modal';
import { User_Storage } from "./../../../class/Storage/UserStorage";
import { AngularFireDatabase } from "angularfire2/database";
import { Component, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  MenuController,
  Events
} from "ionic-angular";
import { Page } from "../../../class/Resource/Page";
import { Storage } from "@ionic/storage";
import { OrderCarrierProvider } from '../../../providers/order-carrier/order-carrier';

@IonicPage()
@Component({
  selector: "page-main-menu-contact",
  templateUrl: "main-menu-contact.html"
})
export class MainMenuContactPage {
  //contacts_online = TempData.prototype.contact_data_online();
  //contacts_offline = TempData.prototype.contact_data_offline();
  contacts_online: any = [];
  contacts_offline: any = [];
  //
  contacts_debug : any = [];
  contacts_debug2 = [];
  contacts_debug3 = [];
  contacts_list: any = [];
  // storage
  store: any;

  // Firebase Event
  fb_contact: any;

  constructor(
    public navCtrl: NavController,
    public modal: ModalProvider,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public firebase: AngularFireDatabase,
    public storage: Storage,
    public chat: ChatServiceProvider,
    public zone: NgZone,
    public events: Events,
    public orderprovider: OrderCarrierProvider
  ) {
    this.zone = new NgZone({ enableLongStackTrace: false });
    this.load_contact();
    this.store = new User_Storage(this.storage);
    this.load_userdata();
    this.orderprovider.Exist();
    // this.debug();
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MainMenuChatPage");
  }

  ionViewWillEnter() {

  }

  ionViewWillLeave() {
    this.chat.Unsubscribe_Event(this.chat.EVENT.CONTACT_LIST_DEPARTMENT);
  }

  BACK_CLICK () {
    this.back_button();
  }

  GOTOCHATPAGE_CLICK(contact) {
    this.call_modal(contact);
  }

  async load_userdata() {
    return await this.store.Get_User_Information();
  }

  async load_contact() {
    /*
    this.chat.Load_Contact_List().subscribe(this.chat.EVENT.CONTACT_LIST, (res) => {
  
      this.contacts_online = [];
      this.contacts_offline = [];

      this.contacts_online = res['online'];
      this.contacts_offline = res['offline'];
    });
    */
    this.chat.Load_Contact_List_Department().subscribe(this.chat.EVENT.CONTACT_LIST_DEPARTMENT, (res) => {
      this.contacts_list = res;
      let i = 0;
      this.contacts_list.forEach ((val) => {
        this.contacts_list[i]['show'] = false;
        i++;
      });
    
    });
  }

  toggleGroup (i) {
    this.contacts_list[i]['show'] = !this.contacts_list[i]['show'];
  }

  private call_modal (data) {
    this.modal.Open_Modal(Page.Menu.Chat.Modal.Select, 'inset-modal-select', data);
  }

  private back_button () {
    if (this.orderprovider.Exist()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(Page.Menu.Page, {}, 
        {
          animate: true,
          direction: "backward"
        }
      );
    }
    
  }

  private debug () {
    this.chat.Load_Contact_List().subscribe(this.chat.EVENT.CONTACT_LIST, (res) => {
     
      this.contacts_debug  = res['online'];
    });
    this.chat.Load_Contact_List_Customer().subscribe(this.chat.EVENT.CONTACT_LIST_CUSTOMER, (res) => {
      console.log(res);
    });
    this.chat.Load_Contact_List_Customer_Preorder().subscribe(this.chat.EVENT.CONTACT_LIST_CUSTOMER_PREORDER, (res) => {
      console.log(res);
    });
  }


}
