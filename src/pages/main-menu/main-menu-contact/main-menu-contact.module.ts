import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuContactPage } from './main-menu-contact';

@NgModule({
  declarations: [
    MainMenuContactPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuContactPage),
  ],
})
export class MainMenuContactPageModule {}
