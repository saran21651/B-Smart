import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuOptionPage } from './main-menu-option';

@NgModule({
  declarations: [
    MainMenuOptionPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuOptionPage),
  ],
})
export class MainMenuOptionPageModule {}
