import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuCheckPricePage } from './main-menu-check-price';

@NgModule({
  declarations: [
    MainMenuCheckPricePage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuCheckPricePage),
  ],
})
export class MainMenuCheckPricePageModule {}
