
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { GlobalString } from '../../../class/Resource/StringList';

@IonicPage()
@Component({
  selector: 'page-main-menu-check-price',
  templateUrl: 'main-menu-check-price.html',
})
export class MainMenuCheckPricePage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuCheckPricePage');
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true, GlobalString.SIDEMENU.MENU_AVATAR);
  }

  ionViewWillLeave() {
    this.menuCtrl.enable(false, GlobalString.SIDEMENU.MENU_AVATAR);
  }

}
