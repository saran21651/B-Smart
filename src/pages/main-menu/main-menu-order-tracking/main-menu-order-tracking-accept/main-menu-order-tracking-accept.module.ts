import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuOrderTrackingAcceptPage } from './main-menu-order-tracking-accept';

@NgModule({
  declarations: [
    MainMenuOrderTrackingAcceptPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuOrderTrackingAcceptPage),
  ],
})
export class MainMenuOrderTrackingAcceptPageModule {}
