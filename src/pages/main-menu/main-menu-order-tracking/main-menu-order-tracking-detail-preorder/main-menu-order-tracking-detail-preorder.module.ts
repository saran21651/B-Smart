import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuOrderTrackingDetailPreorderPage } from './main-menu-order-tracking-detail-preorder';

@NgModule({
  declarations: [
    MainMenuOrderTrackingDetailPreorderPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuOrderTrackingDetailPreorderPage),
  ],
})
export class MainMenuOrderTrackingDetailPreorderPageModule {}
