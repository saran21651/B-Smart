import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { FirebaseProvider } from '../../../../providers/firebase/firebase';
import { ControllerProvider } from '../../../../providers/controller/controller';
import { Page } from '../../../../class/Resource/Page';
import { ModalProvider } from '../../../../providers/modal/modal';

@IonicPage()
@Component({
  selector: 'page-main-menu-order-tracking-detail-preorder',
  templateUrl: 'main-menu-order-tracking-detail-preorder.html',
})
export class MainMenuOrderTrackingDetailPreorderPage {

  items : any;
  order_tracking_array : any;
  load_success = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public firebaseprovider: FirebaseProvider,
    public controller: ControllerProvider,
    public modalcontroller: ModalProvider
    ) {
      this.items = navParams.get('data');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuOrderTrackingDetailPreorderPage');
  }

  ionViewWillEnter () {
    this.Init();
  }

  IsOrderType () {
    if (Number(this.items['order_status']) == 2)  {
      return true;
    } else {
      return false;
    }
  }

  PAYMENTINFORM_CLICK () {
    this.navCtrl.push(Page.Menu.Purchase.PaymentInform.Page, {data: this.items});
  }

  CONFIRM_CLICK () {
    this.modalcontroller.Open_Modal(Page.Menu.OrderTracking.Comment.Page, "inset-modal-comment", this.items);
  }

  CANCEL_CLICK () {
    console.log(this.items);
    
    this.controller.Show_Alert_Confirm(3, '', this.items).then((res) => {
      if (res) {
        this.navCtrl.pop();
      }
    });
    
  }

  private Init () {
    this.load_success = false;
    this.CheckStatus();
  }

  CheckStatus () {
    let value = Number(this.items['order_status']);
    this.order_tracking_array = [];
    this.Add_Status(value);
    this.load_success = true;
  }

  Add_Status (type: number) {
    for (let i = 1; i <= 6; i++) {
      if (i <= type) {
        this.order_tracking_array.push('success');
      } else {
        this.order_tracking_array.push('not_success');
      }
    }
  }

  MarkStatus (value) {
    if (this.load_success == true) {
      let val = Number(value) - 1;

      if (this.order_tracking_array[val] == "success") {
        return true;
      } else {
        return false;
      }
    }
  }

  CheckLevelForComment () {
    let value = Number(this.items['order_status']);
    if (value == 5) {
      return false;
    } else {
      return true;
    }
  }

  CheckOrderTrackingExist () : boolean {
    if (this.items['order_tracking'] != undefined) {
      return true;
    } else {
      return false;
    }
  }
}
