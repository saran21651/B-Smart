import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuOrderTrackingPage } from './main-menu-order-tracking';
import { SwipeSegmentDirective1 } from '../../../directives/swipe-segment1.directive';

@NgModule({
  declarations: [
    MainMenuOrderTrackingPage,
    SwipeSegmentDirective1
  ],
  imports: [
    IonicPageModule.forChild(MainMenuOrderTrackingPage),
  ],
})
export class MainMenuOrderTrackingPageModule {}
