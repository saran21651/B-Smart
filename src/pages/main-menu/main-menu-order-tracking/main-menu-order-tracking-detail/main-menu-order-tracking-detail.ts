import { ModalProvider } from './../../../../providers/modal/modal';
import { ControllerProvider } from './../../../../providers/controller/controller';
import { FirebaseProvider } from './../../../../providers/firebase/firebase';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Page } from '../../../../class/Resource/Page';
import { OrderCarrierProvider } from '../../../../providers/order-carrier/order-carrier';

@IonicPage()
@Component({
  selector: 'page-main-menu-order-tracking-detail',
  templateUrl: 'main-menu-order-tracking-detail.html',
})
export class MainMenuOrderTrackingDetailPage {

  items : any;
  order_tracking_array : any;
  load_success = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public firebaseprovider: FirebaseProvider,
    public controller: ControllerProvider,
    public modalcontroller: ModalProvider,
    public ordercarrierprovider: OrderCarrierProvider
  ) {
    this.items = navParams.get('data');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuOrderTrackingDetailPage');
  }

  ionViewWillEnter () {
    this.Init();
  }

  PURCHASE_CLICK() {
    this.navCtrl.push(Page.Menu.Purchase.Page);
  }

  PAYMENTINFORM_CLICK () {
    this.navCtrl.push(Page.Menu.Purchase.PaymentInform.Page, {data: this.items});
  }

  CONFIRM_CLICK () {
    this.modalcontroller.Open_Modal(Page.Menu.OrderTracking.Comment.Page, "inset-modal-comment", this.items);
  }

  CANCEL_CLICK () {
    this.controller.Show_Alert_Confirm(2, '', this.items).then((res) => {
      if (res) {
        this.navCtrl.pop();
      }
    });
  }

  CONTACT_SELLER_CLICK () {
    this.ordercarrierprovider.Set(this.items);
    this.navCtrl.push(Page.Menu.Chat.Contact.Page);
  }

  Init () {
    // this.IsOrderType();
    this.load_success = false;
    this.CheckStatus();
  }

  IsOrderType () {
    if (Number(this.items['order_status']) == 2)  {
      return true;
    } else {
      return false;
    }
  }

  CheckStatus () {
    let value = Number(this.items['order_status']);
    this.order_tracking_array = [];
   
    /*
    switch (value) {
      case 1:
      case 2:
        this.order_tracking_array.push('success');
        this.order_tracking_array.push('success');
        this.order_tracking_array.push('not_success');
        this.order_tracking_array.push('not_success');
        this.order_tracking_array.push('not_success');
        this.order_tracking_array.push('not_success');
        break;
      case 3:
      case 4:
        this.order_tracking_array.push('success');
        this.order_tracking_array.push('success');
        this.order_tracking_array.push('success');
        this.order_tracking_array.push('success');
        this.order_tracking_array.push('not_success');
        this.order_tracking_array.push('not_success');
        break;
      case 5:
        this.order_tracking_array.push('success');
        this.order_tracking_array.push('success');
        this.order_tracking_array.push('success');
        this.order_tracking_array.push('success');
        this.order_tracking_array.push('success');
        this.order_tracking_array.push('not_success');
        break;
    }
    */
    this.Add_Status(value);
    this.load_success = true;
  }

  Add_Status (type: number) {
    for (let i = 1; i <= 6; i++) {
      if (i <= type) {
        this.order_tracking_array.push('success');
      } else {
        this.order_tracking_array.push('not_success');
      }
    }
  }

  MarkStatus (value) {
    if (this.load_success == true) {
      let val = Number(value) - 1;

      if (this.order_tracking_array[val] == "success") {
        return true;
      } else {
        return false;
      }
    }
  }

  CheckLevelForComment () {
    let value = Number(this.items['order_status']);
    if (value == 5) {
      return false;
    } else {
      return true;
    }
  }

  CheckOrderTrackingExist () : boolean {
    if (this.items['order_tracking'] != undefined) {
      return true;
    } else {
      return false;
    }
  }
}
