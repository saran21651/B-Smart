import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuOrderTrackingDetailPage } from './main-menu-order-tracking-detail';

@NgModule({
  declarations: [
    MainMenuOrderTrackingDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuOrderTrackingDetailPage),
  ],
})
export class MainMenuOrderTrackingDetailPageModule {}
