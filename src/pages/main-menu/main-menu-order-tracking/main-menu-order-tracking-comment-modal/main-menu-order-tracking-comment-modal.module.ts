import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuOrderTrackingCommentModalPage } from './main-menu-order-tracking-comment-modal';

@NgModule({
  declarations: [
    MainMenuOrderTrackingCommentModalPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuOrderTrackingCommentModalPage),
  ],
})
export class MainMenuOrderTrackingCommentModalPageModule {}
