import { Page } from "./../../../../class/Resource/Page";
import { FirebaseProvider } from "./../../../../providers/firebase/firebase";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";

@IonicPage()
@Component({
  selector: "page-main-menu-order-tracking-comment-modal",
  templateUrl: "main-menu-order-tracking-comment-modal.html"
})
export class MainMenuOrderTrackingCommentModalPage {
  stars: string[] = [];
  value: number = 5;
  numStars: number = 5;

  comment_text: string = "";

  info: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public firebaseprovider: FirebaseProvider,
    public viewCtrl: ViewController
  ) {
    this.info = navParams.get("data");
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MainMenuOrderTrackingCommentModalPage");
  }

  ngAfterViewInit() {
    this.calc();
  }

  FINISH_CLICK() {
    let item = {
      star: this.value,
      comment: this.comment_text,
      order_id: this.info["order_id"]
    };
    this.firebaseprovider.Order_Wait(item).then(res => {
      if (res) {
        /*
        let currentIndex = this.navCtrl.getActive().index;
        this.navCtrl.push(Page.Menu.OrderTracking.Page).then(() => {
          this.navCtrl.remove(currentIndex - 2);
        });
        */
        /*
        this.viewCtrl.dismiss().then(() => {
          this.navCtrl.setRoot(Page.Menu.Page);
        });
        */
        this.viewCtrl.dismiss().then(() => {
          this.navCtrl.push(
            Page.Menu.Page,
            { index: 1 },
            {
              animate: false,
              direction: "backward"
            }
          );
        });
      } else {

      }
    });
  }

  STAR_CLICK(value) {
    this.value = value + 1;
    this.calc();
  }

  calc() {
    setTimeout(() => {
      this.stars = [];
      let tmp = this.value;
      for (let i = 0; i < this.numStars; i++, tmp--)
        if (tmp >= 1) this.stars.push("star");
        else if (tmp < 1 && tmp > 0) this.stars.push("star-half");
        else this.stars.push("star-outline");
    }, 0);
  }
}
