import { FirebaseProvider } from './../../../providers/firebase/firebase';
import { Component, ɵConsole } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TempData } from '../../../class/_Temp_data/tempdata';
import { Page } from '../../../class/Resource/Page';


@IonicPage()
@Component({
  selector: 'page-main-menu-order-tracking',
  templateUrl: 'main-menu-order-tracking.html',
})
export class MainMenuOrderTrackingPage {

  // items = TempData.prototype.order_tracking_data();
  items;
  items_preorder;

  // Tabs
  category: string = "instock";
  categories = ['instock', 'preorder'];

  back_from_comment : any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public firebaseprovider: FirebaseProvider
  ) {
    if (navParams.get('index') != undefined) {
      this.back_from_comment = navParams.get('index');
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuOrderTrackingPage');
  }

  ionViewWillEnter () {
    this.Init();
  }

  ionViewWillLeave () {
    if (this.back_from_comment != undefined) {
      this.navCtrl.setRoot(Page.Menu.Page, {}, {
        animate: false,
        direction: 'backward'
      });
    }
  }

  ORDERDETAIL_CLICK (data, type) {
    let dt = {
      data: data
    }
    let page = "";
    console.log(type);
    if (type == "instock") {
      page = Page.Menu.OrderTracking.Detail.Page;
    } else {
      page = Page.Menu.OrderTracking.DetailPreorder.Page;
    }
    this.navCtrl.push(page, dt);
  }

  BACK_CLICK () {
    this.navCtrl.setRoot(Page.Menu.Page, {}, {
      animate: false,
      direction: 'backward'
    });
  }

  onTabChanged(event) {
    this.category = event.value;
  }

  private Init () {
    this.firebaseprovider.Get_Order_Tracking().then((res) => {
      this.items = res;
    });
    this.firebaseprovider.Get_PreOrder_Tracking().then((res) => {
      console.log(res);
      this.items_preorder = res;
    });
  }

}
