import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuNewsPage } from './main-menu-news';

@NgModule({
  declarations: [
    MainMenuNewsPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuNewsPage),
  ],
})
export class MainMenuNewsPageModule {}
