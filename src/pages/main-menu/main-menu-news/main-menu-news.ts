import { FirebaseProvider } from './../../../providers/firebase/firebase';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Page } from '../../../class/Resource/Page';

@IonicPage()
@Component({
  selector: 'page-main-menu-news',
  templateUrl: 'main-menu-news.html',
})
export class MainMenuNewsPage {

  items : any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public firebaseprovider: FirebaseProvider
  ) {
    this.Init();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuNewsPage');
  }

  private Init () {
    this.firebaseprovider.Get_News_Or_Promotion(0).then((res) => {
      let tmp = res;
      for (let i = 0; i < tmp.length; i++) {
        tmp[i]['detail_short'] = this.limit_char(res[i]['detail']);
      }
      this.items = tmp;
    });
  }

  DETAIL_CLICK (item) {
    this.navCtrl.push(Page.Menu.News.Detail.Page, {data: item});
  }

  BACK_CLICK () {
    this.back_button();
  }

  private limit_char (char: string) {
    let limit = 200;
    let new_string = "";
    for (let i = 0; i < limit; i++) {
      new_string = new_string + char[i];
    }
    new_string = new_string + "...";
    return new_string;
  }

  private back_button() {
    if (this.navCtrl.length() == 1) {
      this.navCtrl.setRoot(
        Page.Menu.Page,
        {},
        {
          animate: false,
          direction: "backward"
        }
      );
    } else {
      this.navCtrl.pop();
    }
  }
}
