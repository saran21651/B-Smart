import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Page } from '../../../../class/Resource/Page';

@IonicPage()
@Component({
  selector: 'page-main-menu-news-detail',
  templateUrl: 'main-menu-news-detail.html',
})
export class MainMenuNewsDetailPage {

  item: any;
  imgc = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
    ) {
      this.item = navParams.get("data");
      this.Init(this.item['image']);  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainMenuNewsDetailPage');
  }

  private Init (data) {
    this.imgc = [];
    data.forEach ((img) => {
      this.imgc.push(img);
    });
  }

  onSlideChanged() {
    
  }

  BACK_CLICK () {
    this.back_button();
  }

  private back_button() {
    if (this.navCtrl.length() == 1) {
      this.navCtrl.setRoot(
        Page.Menu.News.Page,
        {},
        {
          animate: false,
          direction: "backward"
        }
      );
    } else {
      this.navCtrl.pop();
    }
  }

}
