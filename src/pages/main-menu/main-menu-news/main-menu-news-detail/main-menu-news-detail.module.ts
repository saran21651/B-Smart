import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuNewsDetailPage } from './main-menu-news-detail';

@NgModule({
  declarations: [
    MainMenuNewsDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuNewsDetailPage),
  ],
})
export class MainMenuNewsDetailPageModule {}
