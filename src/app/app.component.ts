import { UserServiceProvider } from "./../providers/user-service/user-service";
import { isCordovaAvailable } from "./../class/Util/CordovaCheck";
import { Component, ViewChild } from "@angular/core";
import { Platform, MenuController, Nav, ToastController } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { Http, Headers, RequestOptions } from "@angular/http";
import { Subject } from "rxjs";
import { Page } from "../class/Resource/Page";

// One Signal
import { OneSignal, OSNotificationPayload } from "@ionic-native/onesignal";
import { oneSignal } from "../class/Resource/Config";
import { AuthServiceProvider } from "../providers/auth-service/auth-service";
import { ControllerProvider } from "../providers/controller/controller";
import { FirebaseProvider } from "../providers/firebase/firebase";
import { OnesignalProvider } from "../providers/onesignal/onesignal";
import { CartserviceProvider } from "../providers/cartservice/cartservice";
import { AppMinimize } from "@ionic-native/app-minimize";


@Component({
  templateUrl: "app.html"
})
export class MyApp {

  @ViewChild(Nav)
  nav: Nav;

  //rootPage: any = Page.Authentication.Login.Page;
  rootPage: any;
  placeholder = "assets/imgs/default.jpg";

  activePage = new Subject();
  pages: Array<{
    title: string;
    component: any;
    active: boolean;
    icon: string;
  }>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public menuCtrl: MenuController,
    public oneSignal: OneSignal,
    public toastCtrl: ToastController,
    public userservice: UserServiceProvider,
    public http: Http,
    public authservice: AuthServiceProvider,
    public controller: ControllerProvider,
    public firebaseprovider: FirebaseProvider,
    public onesignal : OnesignalProvider,
    public cartservice: CartserviceProvider,
    public appMinimize: AppMinimize
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if (isCordovaAvailable()) {
        this.platform.registerBackButtonAction(() => {
          this.appMinimize.minimize();
        });
        this.Initialize_OneSignal();
      }

      this.check_login();
    });
  }

  SELECTPAGE_CLICK(page) {
    this.nav.setRoot(page.component);
    this.activePage.next(page);
  }

  Initialize_OneSignal() {
    this.oneSignal.startInit(oneSignal.AppId, oneSignal.Sender_Id);
    this.oneSignal.setSubscription(true);
    this.oneSignal.inFocusDisplaying(
      this.oneSignal.OSInFocusDisplayOption.Notification
    );
    this.oneSignal
      .handleNotificationReceived()
      .subscribe(data => this.onPushReceived(data.payload));
    this.oneSignal
      .handleNotificationOpened()
      .subscribe(data => this.onPushOpened(data.notification.payload));
    this.oneSignal.endInit();
  }

  private onPushReceived(payload: OSNotificationPayload) {}

  private onPushOpened(payload: OSNotificationPayload) {
    //this.nav.push(Page.Debug.Page, {data: payload})
    console.log(payload);
    if (payload["additionalData"]["data"] != null) {
      if (payload["additionalData"]["data"]["signal_type"] == "chat") {
        // Chat
        this.nav.setRoot(Page.Menu.Chat.Page, {
          data: {
            chatroom: payload["additionalData"]["data"]["chatroom"]["chatroom"],
            user_id: payload["additionalData"]["data"]["chatroom"]["contact_id"],
            contact_id: payload["additionalData"]["data"]["chatroom"]["user_id"],
            chat_type: payload["additionalData"]["data"]["chatroom"]["chat_type"],
            additional_data: payload["additionalData"]["data"]["chatroom"]["additional_data"] || "",
          }
        });
      }
      // News
      if (payload["additionalData"]["data"]["signal_type"] == "news") {
        let data = payload["additionalData"]["data"]["news"];
        this.nav.setRoot(Page.Menu.News.Detail.Page, {data: data});
      }
      // Promotion
      if (payload["additionalData"]["data"]["signal_type"] == "promotion") {
        let data = payload["additionalData"]["data"]["promotion"];
        this.nav.setRoot(Page.Menu.Promotion.Detail.Page, {data: data});
      }
    }
  }
  debug_flag = true;
  private check_login() {
    this.userservice.check_if_user_logged().then(bool => {

      // this.check_update().then(res => {
      this.firebaseprovider.Get_Version().then(res => {
        if (this.debug_flag == false) {
          if (res === true) {
            this.rootPage = Page.Blank.Page;
            this.controller.Show_Alert_Success(
              2,
              "กรุณาทำการอัพเดตแอพพลิเคชั่น",
              ""
            );
            
          } else {
            if (bool) {
              this.onesignal.subscribe_chat_notification();
              this.onesignal.subscribe_news_and_promotion_notification();
              // this.rootPage = Page.Debug.Page;
              this.rootPage = Page.Menu.Page;
            } else {
              // this.rootPage = Page.Debug.Page;
              this.rootPage = Page.Authentication.Login.Page;
            }
          }
        } else {
          this.cartservice.Initial_Cart().then(() => {
            if (bool) {
              this.onesignal.subscribe_chat_notification();
              this.onesignal.subscribe_news_and_promotion_notification();
              // this.rootPage = Page.Debug.Page;
              this.rootPage = Page.Menu.Page;
            } else {
              // this.rootPage = Page.Debug.Page;
              this.rootPage = Page.Authentication.Login.Page;
            }
          });
        }
        
      });
    });
  }

  private debug() {
    this.nav.setRoot(Page.Menu.Chat.Page, { data: { data: "some data" } });
  }
}
