import { InAppBrowser } from '@ionic-native/in-app-browser';

import { MainMenuPageModule } from './../pages/main-menu/main-menu.module';

// Page
import { MyApp } from './app.component';

// Module
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { SocialSharing } from '@ionic-native/social-sharing';
import { IonicStorageModule } from '@ionic/storage';
import { OneSignal } from "@ionic-native/onesignal";
import { HttpClientModule } from "@angular/common/http";
import { CallNumber } from "@ionic-native/call-number";
import { Camera } from "@ionic-native/camera";
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { DatePicker } from "@ionic-native/date-picker";
import { Market } from "@ionic-native/market";
//import { Facebook } from "@ionic-native/facebook";
// Firebase
//import { Firebase } from "@ionic-native/firebase";
import { AngularFireModule } from 'angularfire2';
//import { AngularFirestoreModule } from "angularfire2/firestore";
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireStorageModule } from 'angularfire2/storage';
// Page Module
import { MainMenuContactPageModule } from '../pages/main-menu/main-menu-contact/main-menu-contact.module';
import { MainMenuCheckPricePageModule } from '../pages/main-menu/main-menu-check-price/main-menu-check-price.module';
import { MainMenuOptionPageModule } from '../pages/main-menu/main-menu-option/main-menu-option.module';
import { MainMenuPromotionPageModule } from '../pages/main-menu/main-menu-promotion/main-menu-promotion.module';
import { MainMenuOrderPageModule } from '../pages/main-menu/main-menu-order/main-menu-order.module';
import { MainMenuChatPageModule } from '../pages/main-menu/main-menu-contact/main-menu-chat/main-menu-chat.module';
import { OnesignalProvider } from '../providers/onesignal/onesignal';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { UserServiceProvider } from '../providers/user-service/user-service';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { DatacarrierProvider } from '../providers/datacarrier/datacarrier';
import { CartserviceProvider } from '../providers/cartservice/cartservice';
import { ModalProvider } from '../providers/modal/modal';
import { ChatProvider } from '../providers/chat/chat';
import { FirebaseProvider } from '../providers/firebase/firebase';
import { ControllerProvider } from '../providers/controller/controller';
import { ChatServiceProvider } from '../providers/chat-service/chat-service';
import { IonRatingComponent } from '../components/ion-rating/ion-rating';
import { AppVersion } from '@ionic-native/app-version';
import { HttpModule } from '@angular/http';
import { LineProvider } from '../providers/line/line';
import { SwipeSegmentDirective } from '../directives/swipe-segment.directive';
import { OrderCarrierProvider } from '../providers/order-carrier/order-carrier';
import { AppMinimize } from '@ionic-native/app-minimize';

const firebaseAuth = {
  apiKey: "AIzaSyAZbz7UAb1GyLi3qCgrnInmsnNSeifXu28",
  authDomain: "b-smart-3a342.firebaseapp.com",
  databaseURL: "https://b-smart-3a342.firebaseio.com",
  projectId: "b-smart-3a342",
  storageBucket: "b-smart-3a342.appspot.com",
  messagingSenderId: "245884396570"
};
@NgModule({
  declarations: [

    MyApp,
    IonRatingComponent,
    // SwipeSegmentDirective
    /*
    HomePage,
    DebugPage,
    //LoginPage,
    RegisterPage,
    //MainMenuPage,
    //MainMenuChatPage,
    //MainMenuCheckPricePage,
    //MainMenuOptionPage,
    //MainMenuOrderPage,
    //MainMenuPromotionPage,

    */
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    AngularFireModule.initializeApp(firebaseAuth),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    MainMenuPageModule,
    MainMenuContactPageModule,
    MainMenuChatPageModule,
    MainMenuCheckPricePageModule,
    MainMenuOptionPageModule,
    MainMenuPromotionPageModule,
    MainMenuOrderPageModule,
    IonicStorageModule.forRoot(),
    HttpClientModule,
    IonicImageViewerModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [

    MyApp,
    /*
    HomePage,
    DebugPage,
    LoginPage,
    RegisterPage,
    MainMenuPage,
    MainMenuContactPage,
    MainMenuChatPage,
    MainMenuCheckPricePage,
    MainMenuOptionPage,
    MainMenuOrderPage,
    MainMenuPromotionPage,
    MainMenuOrderSubpagePage,
    MainMenuOrderDetailPage
    */
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SocialSharing,
    //Facebook,
    OneSignal,
    HttpClientModule,
    InAppBrowser,
    CallNumber,
    Camera,
    PhotoViewer,
    DatePicker,
    AppVersion,
    Market,
    OnesignalProvider,
    AuthServiceProvider,
    UserServiceProvider,
    DatacarrierProvider,
    CartserviceProvider,
    ModalProvider,
    ChatProvider,
    FirebaseProvider,
    ControllerProvider,
    ChatServiceProvider,
    LineProvider,
    OrderCarrierProvider,
    AppMinimize
    //Firebase,
    //FcmProvider,
  ]
})
export class AppModule {}
